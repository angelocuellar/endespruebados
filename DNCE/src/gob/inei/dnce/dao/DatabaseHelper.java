package gob.inei.dnce.dao;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
/*import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.google.code.microlog4android.config.PropertyConfigurator;*/

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


public abstract class DatabaseHelper extends SQLiteOpenHelper {

	/**********************************************/
	private static String TAG = "DatabaseHelper";
	private String databaseName;
	private int databaseVersion;
	//protected static final Logger logger = LoggerFactory.getLogger();
	
	protected static Context context;
	
	public DatabaseHelper(Context ctx, String databaseName, int databaseVersion) {
		super(ctx, databaseName, null, databaseVersion);
		this.databaseName = databaseName;
		this.databaseVersion = databaseVersion;
		context = ctx;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}

	public int getDatabaseVersion() {
		return databaseVersion;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//PropertyConfigurator.getConfigurator(context).configure();
		try {
			leerRAW(db);
			crearVistasCobertura(db);
		} catch (Throwable t) {
			Log.e(TAG, t.getMessage());
			Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
			//logger.error("[" + this.getClass().getName() + "] Error: " + t.getMessage(), t);
		}
	}
	
	public abstract void leerRAW(SQLiteDatabase db) throws Exception;
	
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		crearVistasCobertura(db);
	}
	
	protected void cargarXML(SQLiteDatabase db, int xml, String tag) {
		try {
			cargarXML(db, xml, tag, false);
		} catch (Exception t) {
			
		}
	}
		
	protected void cargarXMLConExcepcion(SQLiteDatabase db, int xml, String tag) throws Exception {
		cargarXML(db,xml,tag,true);
	}
	
	private void cargarXML(SQLiteDatabase db, int xml, String tag, boolean lanzarExcepcion) throws Exception {
		try {
			String sql;
			int i;
			DocumentBuilder dbuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputStream in = context.getResources().openRawResource(xml);			
			Document doc = dbuilder.parse(in, null);
			NodeList statements = doc.getElementsByTagName(tag);
			for (i=0; i<statements.getLength(); i++) {
				sql = statements.item(i).getChildNodes().item(0).getNodeValue();
				
				if(!sql.trim().isEmpty())//Ramon
					db.execSQL(sql);
			}
		}
		catch(Exception t) {
			String msj = "Error al leer RAW["+tag+"]"+t.getMessage();
			Log.e(this.getClass().toString(),msj);
			Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
			//logger.error("[" + this.getClass().getName() + "] Error: " + t.getMessage(), t);
			
			if(lanzarExcepcion) {
				db.endTransaction();//Ramon: har� rollback porque no se hizo setSusscefull antes
				throw new Exception(msj);//Ramon
			}
		}
	}
	
	public abstract void crearVistasCobertura(SQLiteDatabase db);
}
