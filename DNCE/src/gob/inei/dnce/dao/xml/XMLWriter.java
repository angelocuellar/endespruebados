package gob.inei.dnce.dao.xml;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Observable;

import org.xmlpull.v1.XmlSerializer;

import android.util.Base64;
import android.util.Log;
import android.util.Xml;

public class XMLWriter extends Observable {

	private int cantidadEscribir;
	private int cantidadEscrito;
	private boolean enOrdenColumnas;

	/******* Correcta implementacion del patron Singleton *******/
	
	private XMLWriter() {}
	
	public static XMLWriter getInstance() {
		return HolderXMLWriter.xmlWriter;
	}
	
	private static class HolderXMLWriter {
		private static XMLWriter xmlWriter = new XMLWriter();
	}
	/************************************************************/

	public void putBeans(File archivo, String projectName, XMLObject... xmlObjects) throws Exception {
		this.putBeans(false, archivo, projectName, null, xmlObjects);
	}
	
	public void putBeans(boolean enOrdenColumnas, File archivo, String projectName, XMLObject... xmlObjects) throws Exception {
		this.putBeans(enOrdenColumnas, archivo, projectName, null, xmlObjects);
	}

	public void putBeans(File archivo, String projectName, Atributo[] atributos, XMLObject... xmlObjects) throws Exception {
		this.putBeans(false, archivo, projectName, atributos, xmlObjects);
	}
	
	public void putBeans(boolean enOrdenColumnas, File archivo, String projectName, Atributo[] atributos, XMLObject... xmlObjects) throws Exception {
		if (xmlObjects == null || xmlObjects.length == 0) {
			throw new Exception("No indico objetos a leer.");
		}
		this.enOrdenColumnas = enOrdenColumnas;
		FileOutputStream fileos = null;
		try {			
			cantidadEscribir = 0;
			cantidadEscrito = 0;
			fileos = new FileOutputStream(archivo);
			XmlSerializer serializer = Xml.newSerializer();
			serializer.setOutput(fileos, "UTF-8");
			serializer.startDocument(null, Boolean.valueOf(true));
			serializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output", true);
			serializer.startTag(null, projectName);
			if(atributos!=null){
				for (int i = 0; i < atributos.length; i++) {
					serializer.attribute(null, atributos[i].key, atributos[i].value);
				}
			}			
			for (int i = 0; i < xmlObjects.length; i++) {
				if (xmlObjects[i].getBeans() == null) {
					continue;
				}
				cantidadEscribir += xmlObjects[i].getBeans().size();
			}
			setChanged();
			notifyObservers(new XMLObject.BeansProcesados(cantidadEscribir, cantidadEscrito));
			for (int i = 0; i < xmlObjects.length; i++) {
//				escribir(serializer, xmlObjects[i].getBeans(), xmlObjects[i].getClase().getSimpleName(), xmlObjects[i].getTableName());
				if (xmlObjects[i].getBeans() == null) {
					continue;
				}
				escribir(serializer, xmlObjects[i]);
			}
			serializer.endTag(null, projectName);
			serializer.endDocument();
			serializer.flush();         		
        } catch (FileNotFoundException e) {
        	Log.e(this.getClass().toString(),e.getMessage(), e);
        	throw new Exception("El archivo no pudo ser encontrado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
			throw new Exception("No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
		} finally {
            try {
				fileos.close();   
			} catch (IOException e) {
				Log.e(this.getClass().toString(),e.getMessage(), e);
			}
        }
	}
	
//	private void escribir(XmlSerializer serializer, List<? extends Entity> beans, String agrupacion, String item) throws IllegalArgumentException, IllegalAccessException, IllegalStateException, IOException {
	private void escribir(XmlSerializer serializer, XMLObject xmlObject) throws IllegalArgumentException, 
		IllegalAccessException, IllegalStateException, IOException {		
		if (xmlObject.getBeans().size() == 0) {
			return;
		}
		serializer.startTag(null, xmlObject.getClase().getSimpleName());
		serializer.attribute(null, "cantidad", String.valueOf(xmlObject.getBeans().size()));
		if (!this.enOrdenColumnas) {
			for (Entity b : xmlObject.getBeans()) {			
				escribirSinColumnas(serializer, b, xmlObject.getTableName());	
				cantidadEscrito += 1;
				setChanged();
				notifyObservers(new XMLObject.BeansProcesados(cantidadEscribir, cantidadEscrito));
			}
		} else {
			for (Entity b : xmlObject.getBeans()) {			
				escribirConColumnas(serializer, b, xmlObject.getTableName(), xmlObject.getColumNames());	
				cantidadEscrito += 1;
				setChanged();
				notifyObservers(new XMLObject.BeansProcesados(cantidadEscribir, cantidadEscrito));
			}
		}		
		serializer.endTag(null, xmlObject.getClase().getSimpleName());
	}
	
	private void escribirSinColumnas(XmlSerializer serializer, Entity bean, String item) throws IllegalArgumentException, IllegalAccessException, IllegalStateException, IOException {
		serializer.startTag(null, item);
		Field[] fields = bean.getClass().getFields();
		for (int i = 0; i < fields.length; i++) {
			if ("serialVersionUID".equals(fields[i].getName())) {
				continue;
			}
			FieldEntity annotation = fields[i].getAnnotation(FieldEntity.class);  
			if (annotation != null) {
				if (annotation.ignoreField()) {
					continue;
				} 	
			}
			String valor = "";
			if ("null".equals(String.valueOf(fields[i].get(bean)))
					|| "".equals(String.valueOf(fields[i].get(bean)))) {
//				valor = "";
				continue;
			} else {	
				try{
					if(fields[i].getType() == byte[].class){
						Object o = fields[i].get(bean);
						valor = Base64.encodeToString((byte[])fields[i].get(bean), Base64.DEFAULT);
					} else {
						valor = String.valueOf(fields[i].get(bean));							
					}
				} catch (IllegalArgumentException iae) {
					Log.e(getClass().getSimpleName(), iae.getMessage());
					valor = "";
				}
			}
			serializer.startTag(null, fields[i].getName().toUpperCase());
			serializer.text(valor);
			serializer.endTag(null, fields[i].getName().toUpperCase());
		}
		serializer.endTag(null, item);
	}
	
	private void escribirConColumnas(XmlSerializer serializer, Entity bean, String item, String[] columNames) throws IllegalAccessException, IllegalStateException, IOException {
		serializer.startTag(null, item);
		Field[] fields = bean.getClass().getFields();
		for (int j = 0; j < columNames.length; j++) {
			for (int i = 0; i < fields.length; i++) {
				String valor = "";
				if (!columNames[j].equalsIgnoreCase(fields[i].getName())) {
					continue;
				}
				if ("null".equals(String.valueOf(fields[i].get(bean)))
						|| "".equals(String.valueOf(fields[i].get(bean)))) {
					valor = "";
				} else {					
					try{
						if(fields[i].getType() == byte[].class){
							Object o = fields[i].get(bean);
							valor = Base64.encodeToString((byte[])fields[i].get(bean), Base64.DEFAULT);
						} else {
							valor = String.valueOf(fields[i].get(bean));							
						}
					} catch (IllegalArgumentException iae) {
						Log.e(getClass().getSimpleName(), iae.getMessage());
						valor = "";
					}
				}
				serializer.startTag(null, fields[i].getName().toUpperCase());
				serializer.text(valor);
				serializer.endTag(null, fields[i].getName().toUpperCase());
			}
		}
		serializer.endTag(null, item);
	}
	
	public void putMaps(File archivo, String projectName, Atributo[] atributos, XMLDataObject... xmlObjects) throws Exception {
		if (xmlObjects == null || xmlObjects.length == 0) {
			throw new Exception("No indico objetos a leer.");
		}
		FileOutputStream fileos = null;
		try {			
			cantidadEscribir = 0;
			cantidadEscrito = 0;
			fileos = new FileOutputStream(archivo);
			XmlSerializer serializer = Xml.newSerializer();
			serializer.setOutput(fileos, "ISO-8859-1");
			serializer.startDocument(null, Boolean.valueOf(true));
			serializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output", true);
			serializer.startTag(null, projectName);
			if(atributos!=null){
				for (int i = 0; i < atributos.length; i++) {
					serializer.attribute(null, atributos[i].key, atributos[i].value);
				}
			}
			for (int i = 0; i < xmlObjects.length; i++) {
				if (xmlObjects[i].getRegistros() == null) {
					continue;
				}
				cantidadEscribir += xmlObjects[i].getRegistros().size();
			}
			setChanged();
			notifyObservers(new XMLObject.BeansProcesados(cantidadEscribir, cantidadEscrito));
			for (int i = 0; i < xmlObjects.length; i++) {
//				escribir(serializer, xmlObjects[i].getBeans(), xmlObjects[i].getClase().getSimpleName(), xmlObjects[i].getTableName());
				if (xmlObjects[i].getRegistros() == null) {
					continue;
				}
				escribir(serializer, xmlObjects[i]);
			}
			serializer.endTag(null, projectName);
			serializer.endDocument();
			serializer.flush();         
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new Exception("No se pudo cargar los datos en la Base de Datos.");
		} finally {
        }
	}
	
	private void escribir(XmlSerializer serializer, XMLDataObject xmlObject) throws IllegalArgumentException, IllegalAccessException, IllegalStateException, IOException {	
		if (xmlObject.getRegistros().size() == 0) {
			return;
		}
		serializer.startTag(null, xmlObject.getBeanName());
		serializer.attribute(null, "cantidad", String.valueOf(xmlObject.getRegistros().size()));
		for (int i = 0; i < xmlObject.getRegistros().size(); i++) {
			if(xmlObject.getRegistros().get(i).isEmpty()) {
				continue;
			}			
			Map<String, Object> c1 = xmlObject.getRegistros().get(i);
			serializer.startTag(null, xmlObject.getTableName());
			for ( String key : c1.keySet() ) {
				if (c1.get(key) == null) {
					continue;
				}
				try {
					String type = null;
					String valor = "";
					if ("null".equals(String.valueOf(c1.get(key)))
							|| "".equals(String.valueOf(c1.get(key)))) {
						continue;
					} else {	
						try{
							if(c1.get(key) instanceof byte[]){
								type = "BLOB";
								valor = Base64.encodeToString((byte[])c1.get(key), Base64.DEFAULT);
							} else {
								valor = String.valueOf(c1.get(key));							
							}
						} catch (IllegalArgumentException iae) {
							Log.e(getClass().getSimpleName(), iae.getMessage());
							valor = "";
						}
					}
					serializer.startTag(null, key.toUpperCase());
					if (type != null) {
						serializer.attribute(null, "type", type);
					}
					serializer.text(valor);
					serializer.endTag(null, key.toUpperCase());
				} catch (Exception e) {
					//table.addContent(new Element(key.toUpperCase()).setText("ERROR"));
				}
			}
			serializer.endTag(null, xmlObject.getTableName());
			cantidadEscrito += 1;
			setChanged();
			notifyObservers(new XMLObject.BeansProcesados(cantidadEscribir, cantidadEscrito));
		}
		serializer.endTag(null, xmlObject.getBeanName());
	}	
	
	public static class Atributo {
		public String key;
		public String value;
		
		public Atributo(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}
		
	}
}
