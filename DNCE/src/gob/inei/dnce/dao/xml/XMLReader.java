package gob.inei.dnce.dao.xml;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Base64;
import android.util.Log;
import android.util.Xml;

public class XMLReader extends Observable {
	
	private static final String ns = null;
	private int cantidadLeer;
	private int cantidadLeido;
	private final String TAG = getClass().getSimpleName();
	private String name_gpx = null;
	/******* Correcta implementacion del patron Singleton *******/
	
	private XMLReader() {}
	
	public static XMLReader getInstance() {
		return HolderXMLReader.xmlReader;
	}
	
	private static class HolderXMLReader {
		private static XMLReader xmlReader = new XMLReader();
	}
	
	/************************************************************/
	
	public List<XMLObject> getBeans(File archivo, String projectName, XMLObject... tmps) throws Exception {
		if (tmps == null || tmps.length == 0) {
			throw new Exception("No indico objetos a leer.");
		}
		InputStream in = null;
		try {			
			cantidadLeer = 0;
			cantidadLeido = 0;
			in = new FileInputStream(archivo);
			XmlPullParser parserCount = Xml.newPullParser();
			parserCount.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parserCount.setInput(in, null);
			parserCount.nextTag();
            contarBeansXML(parserCount, projectName, tmps);
            cantidadLeer *= 2;
            setChanged();
            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
            in.close();
            in = null;
			in = new FileInputStream(archivo);
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
//            parser.setInput(in, "ISO-8859-1");
            parser.nextTag();
        	leerXML(parser, projectName, tmps);            		
        } catch (FileNotFoundException e) {
        	Log.e(this.getClass().toString(),e.getMessage(), e);
        	throw new Exception("El archivo no pudo ser encontrado.");
		} catch (XmlPullParserException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
			throw new Exception("El archivo no pudo ser cargado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
			throw new Exception("No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
		} finally {
            try {
				in.close();
			} catch (IOException e) {
				Log.e(this.getClass().toString(),e.getMessage(), e);
			}
        }
		return Arrays.asList(tmps);
	}
	
	private void leerXML(XmlPullParser parser, String projectName, XMLObject... tmps) throws XmlPullParserException, IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		parser.require(XmlPullParser.START_TAG, ns, projectName);

		int eventType = parser.getEventType();
//		while (eventType != XmlPullParser.END_DOCUMENT/* && parser.next() != XmlPullParser.END_TAG*/) {
		while(parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        if (projectName.equals(name)) {
				continue;
			}
	        boolean flag = true;
	        for (int i = 0; i < tmps.length; i++) {
	        	if (name.equals(tmps[i].getClase().getSimpleName())) {		             
	        		leer(parser, tmps[i].getClase(), tmps[i].getBeans(), name, tmps[i].getTableName());
	        		flag = false;
	        		break;
		        }
			}
	        if (flag) {
		        skip(parser);				
			}
	        //eventType = parser.next();
	    }  
	}
	
	private void contarBeansXML(XmlPullParser parser, String projectName, XMLObject... tmps) throws XmlPullParserException, IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
//		parser.require(XmlPullParser.START_TAG, ns, projectName);
//		int indiceCantidad = 0;
//		boolean encontroCantidad;
//	    while (parser.next() != XmlPullParser.END_TAG) {
//	        if (parser.getEventType() != XmlPullParser.START_TAG) {
//	            continue;
//	        }
//	        encontroCantidad = false;
//	        String name = parser.getName();
//	        for (int i = indiceCantidad; i < tmps.length; i++) {
//	        	if (name.equals(tmps[i].getClase().getSimpleName())) {
//	        		this.cantidadLeer += (Integer.parseInt(parser.getAttributeValue(0)));
//	        		indiceCantidad += 1;
//	        		encontroCantidad = true;
//	        		skip(parser);
//	        		break;
//		        }
//			}
//	        if (!encontroCantidad) {
//	        	//skip(parser);
//			}
//	    }  
		parser.require(XmlPullParser.START_TAG, ns, projectName);
		int indiceCantidad = 0;
		boolean encontroCantidad;
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        encontroCantidad = false;
	        String name = parser.getName();
	        for (int i = indiceCantidad; i < tmps.length; i++) {
	        	if (tmps[i].getClase().getSimpleName() == null || tmps[i].getClase().getSimpleName().isEmpty()) {
	        		Map<String, Object> map;
	        		String agrupacion = tmps[i].getTableName();
	    		    int eventType = parser.getEventType(); //parser.next();
	    		    while (parser.next() != XmlPullParser.END_TAG) {
	    			    if (parser.getEventType() != XmlPullParser.START_TAG) {
	    			        continue;
	    			    }
	    			    name = parser.getName();
	    			    if (name.equals(agrupacion)) {
	    			        if (parser.getAttributeCount() > 0) {
	    			        	for (int j = 0; j < parser.getAttributeCount(); j++) {
	    			        		if ("cantidad".equals(parser.getAttributeName(j))) {
	    								this.cantidadLeer += Util.getInt(Integer.parseInt(parser.getAttributeValue(0)));
	    				        		indiceCantidad += 1;
	    				        		encontroCantidad = true;
	    				        		skip(parser);
	    				        		break;	
	    							}
	    						}
	    			        } else {
	    			        	this.cantidadLeer += 1;
	    			        }
	    			    } else {
	    			        skip(parser);
	    			    }
	    		    }
				} else if (name.equals(tmps[i].getClase().getSimpleName())) {      
					if (parser.getAttributeCount() > 0) {
						for (int j = 0; j < parser.getAttributeCount(); j++) {
							if ("cantidad".equals(parser.getAttributeName(j))) {
								this.cantidadLeer += Util.getInt(Integer.parseInt(parser.getAttributeValue(0)));
				        		indiceCantidad += 1;
				        		encontroCantidad = true;
				        		skip(parser);
				        		break;	
							}
						}
					}
		        }
			}
	        if (!encontroCantidad) {
	        	skip(parser);
			}
	    }  
	
	}
	
	private void leer(XmlPullParser parser, Class<? extends Entity> clase, List beans, String agrupacion, String item) throws XmlPullParserException, 
		IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, 
		InvocationTargetException, NoSuchMethodException {		
		parser.require(XmlPullParser.START_TAG, ns, agrupacion);
		while (parser.next() != XmlPullParser.END_TAG) {
		    if (parser.getEventType() != XmlPullParser.START_TAG) {
		        continue;
		    }
		    String name = parser.getName();
		    Entity o = null;
		    // Starts by looking for the item tag
		    if (name.equals(item)) {
		    	o = (Entity) leer(parser, clase, o, item);
		    	if (o != null) {
		    		cantidadLeido += 1;
		            beans.add(o);
		            setChanged();
		            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
				}
		    } else {
		        skip(parser);
		    }
		}  
	}
	
	private Object leer(XmlPullParser parser, Class<? extends Entity> clase, Object bean, String item) throws XmlPullParserException, 
		IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, 
		NoSuchMethodException {
		parser.require(XmlPullParser.START_TAG, ns, item);
		bean = clase.getConstructor().newInstance();
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        Field[] propiedades = bean.getClass().getFields();
	        String name = parser.getName();
	        String val = null;
	        for (int i = 0; i < propiedades.length; i++) {
				if (propiedades[i].getName().compareToIgnoreCase(name) == 0) {
					val = readTag(parser, name);
					if ("".equals(val)) {
						propiedades[i].set(bean,null);
					} else if (propiedades[i].getType() == String.class) {
						propiedades[i].set(bean,val);
					} else if (propiedades[i].getType() == Integer.class) {
						propiedades[i].set(bean,Integer.valueOf(val));	
					} else if (propiedades[i].getType() == BigDecimal.class) {
						propiedades[i].set(bean,new BigDecimal(val));
					} else if (propiedades[i].getType() == Double.class) {
						propiedades[i].set(bean,Double.valueOf(val));
					} else if(propiedades[i].getType() == byte[].class){
						byte[] imagen = Base64.decode(val, Base64.DEFAULT);
						propiedades[i].set(bean, Base64.decode(val, Base64.DEFAULT));
					} else {
						propiedades[i].set(bean,null);
					}
					break;
				}
			}
	        if (val == null) {
	        	skip(parser);
			}
	    }
	    return bean;
	}
	
	private String readTag(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag);
	    String contenido = readText(parser);
	    try {
	    	parser.require(XmlPullParser.END_TAG, ns, tag);
	    } catch (XmlPullParserException ex) {
	    	contenido = null;
	    	//skip(parser);
	    }
	    return contenido;
	}
	
	// For the tags title and summary, extracts their text values.
	private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
	    String result = "";
	    if (parser.next() == XmlPullParser.TEXT) {
	        result = parser.getText();
	        parser.nextTag();
	    }
	    return result;
	}
	
	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

	public List<XMLDataObject> getMaps(File archivo, String projectName, XMLDataObject... tmps) throws Exception {
		if (tmps == null || tmps.length == 0) {
			throw new Exception("No indico objetos a leer.");
		}
		InputStream in = null;
		try {			
			cantidadLeer = 0;
			cantidadLeido = 0;
			in = new FileInputStream(archivo);
			XmlPullParser parserCount = Xml.newPullParser();
			parserCount.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parserCount.setInput(in, null);
			parserCount.nextTag();
            contarMapsXML(parserCount, projectName, tmps);
            cantidadLeer *= 2;
            setChanged();
            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
            in.close();
            in = null;
			in = new FileInputStream(archivo);
            XmlPullParser parser = Xml.newPullParser();
//            parser.setInput(in, "ISO-8859-1");
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
        	leerXML(parser, projectName, tmps); 
		} catch (FileNotFoundException e) {
        	Log.e(this.getClass().toString(),e.getMessage(), e);
        	throw new Exception("El archivo no pudo ser encontrado.");
		} catch (XmlPullParserException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
			throw new Exception("El archivo no pudo ser cargado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
			throw new Exception("No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			Log.e(this.getClass().toString(),e.getMessage(), e);
		} finally {
            try {
				in.close();
			} catch (IOException e) {
				Log.e(this.getClass().toString(),e.getMessage(), e);
			}
        }
		return Arrays.asList(tmps);
	}
	
	private void leerXML(XmlPullParser parser, String projectName, XMLDataObject... tmps) throws XmlPullParserException, 
		IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, 
		NoSuchMethodException {
		parser.require(XmlPullParser.START_TAG, ns, projectName);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Log.e(TAG, "Leyendo XML: " + sdf.format(new Date()));
		int eventType = parser.getEventType();
	    while (eventType != XmlPullParser.END_DOCUMENT/* && parser.next() != XmlPullParser.END_TAG*/) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {	  
		        eventType = parser.next();      	
	            continue;
	        }
	        String name = parser.getName();
	        Log.e(TAG, "leerXML: name["+name+"]");
	        if ("trk".equals(name)) {
				Map<String, Object> nameMap = leer(parser, "trk");
				if (nameMap != null) {
					name_gpx = nameMap.get("NAME").toString();	
				}
			}
	        for (int i = 0; i < tmps.length; i++) {
	        	if (tmps[i].getBeanName() == null || tmps[i].getBeanName().isEmpty()) {
	        		//Log.e(TAG, "	TableName: " + tmps[i].getTableName());
	        		leer(parser, tmps[i].getRegistros(), tmps[i].getTableName(), null);
	        		break;
				} else if (name.equals(tmps[i].getBeanName())) {      
					//Log.e(TAG, "	BeanName: " + name);
	        		leer(parser, tmps[i].getRegistros(), name, tmps[i].getTableName());
	        		if ("trkseg".equals(name)) {
	        			for (Map<String, Object> m : tmps[i].getRegistros()) {
							m.put("NAME", name_gpx);
						}
	        			name_gpx = null;
					}
	        		break;
		        } else {
		        }
			}
	        eventType = parser.next();
	    }
	    Log.e(TAG, "Finalizando XML: " + sdf.format(new Date()));
	}
	
	private void leer(XmlPullParser parser, List<Map<String, Object>> maps, String agrupacion, String item) throws XmlPullParserException, 
		IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, 
		InvocationTargetException, NoSuchMethodException {
		if (maps == null) {
			maps = new ArrayList<Map<String,Object>>();
		}
		if (item == null) {
//			Log.e(TAG, "leer(): item es null[" + agrupacion+"]");
//			String name = parser.getName();
		    Map<String, Object> map;
		    int eventType = parser.getEventType(); //parser.next();
		    while (parser.next() != XmlPullParser.END_TAG) {
			    if (parser.getEventType() != XmlPullParser.START_TAG) {
			        continue;
			    }
			    String name = parser.getName();
			    if (name.equals(agrupacion)) {
			    	List<String[]> atributtes = new ArrayList<String[]>();
			        if (parser.getAttributeCount() > 0) {
			        	for (int i = 0; i < parser.getAttributeCount(); i++) {
			        		//map.put(parser.getAttributeName(i),parser.getAttributeValue(i));
			        		atributtes.add(new String[]{parser.getAttributeName(i),parser.getAttributeValue(i)});
						}
			        }
				    map = leer(parser, agrupacion);
			    	if (map != null) {
			    		for (int i = 0; i < atributtes.size(); i++) {
							map.put(atributtes.get(i)[0].toUpperCase(), atributtes.get(i)[1]);
						}
			    		cantidadLeido += 1;
			    		if (!map.isEmpty()) {
				    		maps.add(map);
				            setChanged();
				            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
						}
//			    		maps.add(map);
//			            setChanged();
//			            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
					}
			    } else {
//			    	Log.e(TAG, "leer(): skip(parder)");
			        skip(parser);
			    }
		    }
//		    Log.e(TAG, "leer(): name: " + name);
//		    if (name.equals(agrupacion)) {
		    	
//		    } else {
//		        skip(parser);
//		    }
		} else {
			parser.require(XmlPullParser.START_TAG, ns, agrupacion);
//			 Log.e(TAG, "leer(): agrupacion[" + agrupacion+"] item["+ item + "]");
			while (parser.next() != XmlPullParser.END_TAG) {
			    if (parser.getEventType() != XmlPullParser.START_TAG) {
			        continue;
			    }
			    String name = parser.getName();
//			    Log.e(TAG, "leer(): name[" + name + "]");
//			    Log.e(TAG, "leer(): parser.getAttributeCount[" + parser.getAttributeCount() + "]"); 
			    List<String[]> atributtes = new ArrayList<String[]>();
		        if (parser.getAttributeCount() > 0) {
		        	for (int i = 0; i < parser.getAttributeCount(); i++) {
		        		//map.put(parser.getAttributeName(i),parser.getAttributeValue(i));
		        		atributtes.add(new String[]{parser.getAttributeName(i),parser.getAttributeValue(i)});
					}
		        }
			    Map<String, Object> map; 
			    if (name.equals(item)) {
			    	map = leer(parser, item);
			    	if (map != null) {
			    		cantidadLeido += 1;
			    		for (int i = 0; i < atributtes.size(); i++) {
							map.put(atributtes.get(i)[0].toUpperCase(), atributtes.get(i)[1]);
						}
			    		if (!map.isEmpty()) {
				    		maps.add(map);
				            setChanged();
				            notifyObservers(new XMLObject.BeansProcesados(cantidadLeer, cantidadLeido));
						}
					}
			    } else {
//			    	Log.e(TAG, "leer(): skip(parder)");
			        skip(parser);
			    }
			}  
		}
	}
	
	private Map<String, Object> leer(XmlPullParser parser, String item) throws XmlPullParserException, 
		IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, 
		NoSuchMethodException {
		parser.require(XmlPullParser.START_TAG, ns, item);
//		Log.e(TAG, "leer: " + item);
		Map<String, Object> map = new HashMap<String, Object>();
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String type = null;
	        if (parser.getAttributeCount() > 0) {
	        	type = parser.getAttributeValue(0);
	        }
	        String name = parser.getName();
	        String val = readTag(parser, name);
	        if (val == null) {
	        	skip(parser);
			} else {
				if (type != null) {
					if (type.equals("BLOB")) {
						byte[] blob = Base64.decode(val, Base64.DEFAULT);
						map.put(name.toUpperCase(), blob);
					} else {
						map.put(name.toUpperCase(), val);
					}
				} else {
					map.put(name.toUpperCase(), val);
				}
			}
	    }
	    return map;
	}
		
	private void contarMapsXML(XmlPullParser parser, String projectName,
			XMLDataObject[] tmps) throws IllegalAccessException, XmlPullParserException, IOException, 
			IllegalArgumentException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		parser.require(XmlPullParser.START_TAG, ns, projectName);
		int indiceCantidad = 0;
		boolean encontroCantidad;
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        encontroCantidad = false;
	        String name = parser.getName();
	        for (int i = indiceCantidad; i < tmps.length; i++) {
	        	if (tmps[i].getBeanName() == null || tmps[i].getBeanName().isEmpty()) {
	        		Map<String, Object> map;
	        		String agrupacion = tmps[i].getTableName();
	    		    int eventType = parser.getEventType(); //parser.next();
	    		    while (parser.next() != XmlPullParser.END_TAG) {
	    			    if (parser.getEventType() != XmlPullParser.START_TAG) {
	    			        continue;
	    			    }
	    			    name = parser.getName();
	    			    if (name.equals(agrupacion)) {
	    			        if (parser.getAttributeCount() > 0) {
	    			        	for (int j = 0; j < parser.getAttributeCount(); j++) {
	    			        		if ("cantidad".equals(parser.getAttributeName(j))) {
	    								this.cantidadLeer += Util.getInt(Integer.parseInt(parser.getAttributeValue(0)));
	    				        		indiceCantidad += 1;
	    				        		encontroCantidad = true;
	    				        		skip(parser);
	    				        		break;	
	    							}
	    						}
	    			        } else {
	    			        	this.cantidadLeer += 1;
	    			        }
	    			    } else {
	    			        skip(parser);
	    			    }
	    		    }
				} else if (name.equals(tmps[i].getBeanName())) {      
					if (parser.getAttributeCount() > 0) {
						for (int j = 0; j < parser.getAttributeCount(); j++) {
							if ("cantidad".equals(parser.getAttributeName(j))) {
								this.cantidadLeer += Util.getInt(Integer.parseInt(parser.getAttributeValue(0)));
				        		indiceCantidad += 1;
				        		encontroCantidad = true;
				        		skip(parser);
				        		break;	
							}
						}
					}
		        }
			}
	        if (!encontroCantidad) {
	        	//skip(parser);
			}
	    }  
	}
}
