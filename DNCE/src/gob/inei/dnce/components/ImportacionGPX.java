package gob.inei.dnce.components;

import gob.inei.dnce.dao.xml.XMLDataObject;
import gob.inei.dnce.dao.xml.XMLObject;
import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
import gob.inei.dnce.dao.xml.XMLReader;
import gob.inei.dnce.interfaces.ILeerArchivosGPX;
import gob.inei.dnce.interfaces.Respondible;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.xmlpull.v1.XmlPullParserException;

import android.app.ProgressDialog;
import android.database.SQLException;
import android.os.AsyncTask;
import android.util.Log;

public class ImportacionGPX extends AsyncTask<String, String, String> implements
		Respondible, Observer {

	public static enum TIPO{WAYPOINTS, TRACKS};
	private ProgressDialog pDialog;
	private String msg;
	private String titulo;
	private List<File> archivos;
	private int cantidadCargar;
	private int cantidadCargado;
	private TIPO tipo; 
	private XMLDataObject[] maps;
	private ILeerArchivosGPX form;

	public ImportacionGPX(ILeerArchivosGPX form, String titulo, TIPO lectura) {
		super();
		this.form = form;
		this.titulo = titulo;
		this.tipo = lectura;
	}

	public List<File> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<File> archivos) {
		this.archivos = archivos;
	}

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(this.form.getActivity());
		pDialog.setMessage(titulo + " Por favor espere...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	protected String doInBackground(String... args) {
		try {
			this.maps = null;
			procesarXML();
			msg = "Importación completada correctamente.";		
		} catch (NullPointerException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = "Error de ausencia de datos.";
		} catch (SQLException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = e.getMessage();
		} catch (Exception e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = e.getMessage();
		} finally {
			this.form.postLoad(this.maps);
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(String... progress) {
		pDialog.setProgress(Integer.parseInt(progress[0]));
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		this.form.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				DialogComponent dlg = new DialogComponent(ImportacionGPX. this.form.getActivity(),
						ImportacionGPX.this, DialogComponent.TIPO_DIALOGO.NEUTRAL,
						titulo, msg);
				dlg.showDialog();
			}
		});
	}	

	private void procesarXML() throws Exception {
		try {
			String proyecto = "gpx";//appName;
			this.maps = getListMaps();
			if (this.maps == null) {
				throw new Exception("No se encontró el tipo de lectura a realizar.");
			}
			for (File archivo : archivos) {
				XMLReader.getInstance().deleteObservers();
				XMLReader.getInstance().addObserver(this);
				XMLReader.getInstance().getMaps(archivo, proyecto, this.maps);
				Map<String, Object> mapName = null;
				for (int i = 0; i < this.maps.length; i++) {
					for (Map<String, Object> map : this.maps[i].getRegistros()) {
						map.put("FILE_NAME", archivo.getName());
					}
				}
			}	
		} catch (FileNotFoundException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			throw new Exception("El archivo no pudo ser encontrado.");
		} catch (XmlPullParserException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			throw new Exception("El archivo no pudo ser cargado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			throw new Exception(
					"No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
		} catch (SQLException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			throw new Exception(e.getMessage());
		} finally {
		}
	}

	private XMLDataObject[] getListMaps() {
		switch (this.tipo) {
		case WAYPOINTS:
			return new XMLDataObject[]{new XMLDataObject("wpt", null, XMLDataObject.BORRAR_PRIMERO)};
		case TRACKS:
			return new XMLDataObject[]{/*new XMLDataObject("trk", "gpx", XMLDataObject.BORRAR_PRIMERO),*/ new XMLDataObject("trkpt", "trkseg", XMLDataObject.BORRAR_PRIMERO)};
		default: return null;
		}
	}

	private void calcularPorcentajeProcesado() {
		pDialog.incrementProgressBy(1);		
		publishProgress(String.valueOf(cantidadCargado));		
	}

	@Override
	public void update(Observable observable, Object data) {
		if (data == null) {
			return;
		}
		XMLObject.BeansProcesados procesado = (BeansProcesados) data;
		this.cantidadCargar = procesado.getTotal();
		this.cantidadCargado = procesado.getProcesado();
		calcularPorcentajeProcesado();
		pDialog.setMax(this.cantidadCargar);
	}

	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
	}
}
