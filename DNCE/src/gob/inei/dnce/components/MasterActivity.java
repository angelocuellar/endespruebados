package gob.inei.dnce.components;

import gob.inei.dnce.adapter.MyFragmentPagerAdapter;
import gob.inei.dnce.listeners.NavigationClickListener;

import java.util.Stack;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;

public abstract class MasterActivity extends FragmentActivity {
    protected FragmentViewPager viewPager;
    protected boolean esSalto;
    protected boolean esArrastre;
    protected boolean debeGrabar;
    protected boolean esAvanceDual;
    private int curPage;
    private int prevPage;
    protected MyFragmentPagerAdapter pageAdapter;
    protected NavigationClickListener navigationClickListener;
    protected Stack<Integer> pila = new Stack<Integer>();
    protected boolean ocurrioRetroceso;
    protected boolean ocurrioSalto;
    
    
    //Ram�n
    public void setTitulo(String titulo) {
		if (titulo != null)
			getActionBar().setTitle(titulo);
	}
	
    //Ram�n
	public void setSubTitulo(String subtitulo) {
		if(subtitulo!=null)
			getActionBar().setSubtitle(subtitulo);
	}

	//Ram�n
	public final String getTitulo() {
		return getActionBar().getTitle().toString();
	}
    
	//Ram�n
	public final String getSubtitulo() {
		return getActionBar().getSubtitle().toString();
	}
    
    public void nextFragment(){
		nextFragment(-1);
	}
	
	/**
	 * 
	 * @param iFragment basado en cero
	 */
	public void nextFragment(final int iFragment){
		if(iFragment==-1){
			new Handler().post(new Runnable() {
				@Override
				public void run() {
				}
			});
		}
		else {
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					esSalto = true;
					prevPage = viewPager.getCurrentItem();
					curPage = iFragment;
					viewPager.setCurrentItem(curPage, false);
				}
			});
		}
		
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public int getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(int prevPage) {
		this.prevPage = prevPage;
	}

	public NavigationClickListener getNavigationClickListener() {
		return navigationClickListener;
	}

	public void setNavigationClickListener(
			NavigationClickListener navigationClickListener) {
		this.navigationClickListener = navigationClickListener;
	}

	public void setEsArrastre(boolean flag) {
		esArrastre = flag;
	}
	
	public boolean isSalto() {
		return esSalto;
	}

	public void setEsSalto(boolean esSalto) {
		this.esSalto = esSalto;
	}

	public boolean isOcurrioSalto() {
		return ocurrioSalto;
	}

	public void setOcurrioSalto(boolean ocurrioSalto) {
		this.ocurrioSalto = ocurrioSalto;
	}

	public boolean isDebeGrabar() {
		return debeGrabar;
	}

	public void setDebeGrabar(boolean debeGrabar) {
		this.debeGrabar = debeGrabar;
	}

	public abstract void uploadData();
	
	public abstract void uploadMarco();
	
	public abstract void calificar();

	public boolean isEsAvanceDual() {
		return esAvanceDual;
	}

	public void setEsAvanceDual(boolean esAvanceDual) {
		this.esAvanceDual = esAvanceDual;
	}
}
