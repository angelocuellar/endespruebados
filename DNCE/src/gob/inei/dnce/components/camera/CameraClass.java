package gob.inei.dnce.components.camera;

import gob.inei.dnce.R;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

public class CameraClass extends Activity {
	// Default Camera
	private static final int CAMERA_PIC_REQUEST = 11111;
	public static final String PICTURE_NAME_PROPERTY = "PICTURE_NAME_PROPERTY";
	private static OnPictureTaken mOnPictureTaken;
	public static boolean useDefaultCamera = false;
	// Custom Camera
	public static int RESOLUTION = CustomCamera.RESOLUTION_320x240;
	private CamPreview mPreview;
	private final String LOG_TAG = "Camera Lib : CameraLibrary";
	private RelativeLayout cameraFrame;
	private ImageView imgCapture;
	private RelativeLayout Imagebackground;
	private ImageView imgBackgroundImage;
	private boolean shouldGoBack = true;
	private Bitmap CaptureImage = null;
	private String ImagePath;
	private ProgressDialog mProgressDialog;
	public static String RUTA_FOTOS = null;
	int presionadas = 0;
	int presionadas2 = 0;
	private Uri fileUri;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		File carpeta = new File(RUTA_FOTOS);
		if (!carpeta.exists()) {
			carpeta.mkdirs();
		}
		ImagePath = RUTA_FOTOS + "/temp.jpg";
		if (useDefaultCamera) {
			Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
			startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
		} else {			
//			ImagePath = RUTA_FOTOS + "/temp.jpg";
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setTitle("Processing Image");
			mProgressDialog.setMessage("Please wait...");
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);

//			if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//			    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//			} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//			}
			
			WindowManager.LayoutParams lp = getWindow().getAttributes();
			lp.screenBrightness = 1.0f; //para poner el brillo al maximo
			getWindow().setAttributes(lp);
			
			CreateView();
			mPreview = new CamPreview(getApplicationContext());
			cameraFrame.addView(mPreview);

			imgCapture.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					presionadas2+=1;
					if (presionadas2 > 1) {
						//presionadas2 = 0;
						return;
					}
					
					mPreview.mCamera.takePicture(new ShutterCallback() {

						@Override
						public void onShutter() {

							mProgressDialog.show();
							cameraFrame.setVisibility(View.GONE);
							imgCapture.setVisibility(View.GONE);
							Imagebackground.setVisibility(View.GONE);
						}
					}, null, new PictureCallback() {

						@Override
						public void onPictureTaken(byte[] data, Camera camera) {

							FileOutputStream fos;
							try {

								makeCameraVisible(false);
								File f = new File(ImagePath);
								fos = new FileOutputStream(f);
								fos.write(data);
								fos.close();

								CaptureImage = BitmapFactory
										.decodeFile(new File(ImagePath)
												.getAbsolutePath());

								imgBackgroundImage.setImageBitmap(CaptureImage);

								mProgressDialog.cancel();

							} catch (Exception e) {
								Log.e(LOG_TAG, e.getMessage());
								e.printStackTrace();
								throw new RuntimeException(e.getMessage());
							}

						}
					});
				}
			});
		}
	}

	public static void setOnPictureTakenListner(OnPictureTaken activity) {
		mOnPictureTaken = activity;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK) {
//			if (data != null) {
//				Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//				if (mOnPictureTaken != null)
//					try {
//						mOnPictureTaken.pictureTaken(bitmap, savePicture(bitmap, ImagePath));
//					} catch (IOException e) {
//						Log.e(LOG_TAG, e.getMessage());
//					}
//			}
			BitmapFactory.Options options = new BitmapFactory.Options();
			//options.inSampleSize = 2;
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
			mOnPictureTaken.pictureTaken(bitmap, new File(getRealPathFromURI(fileUri)));
			shouldGoBack = true;
			onBackPressed();
		} else {
			shouldGoBack = true;
			onBackPressed();
		}
	}

	public String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}
	
	private String getRealPathFromURI(Uri contentURI) {
	    String result;
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentURI.getPath();
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
	        cursor.close();
	    }
	    return result;
	}

	public void CreateView() {
		LayoutParams FILL_PARENT = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);

		LayoutParams WRAP_CONTENT = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		LayoutParams BTN_WRAP_CONTENT = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);

		LayoutParams WIDTH_FIIL_PARENT = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		RelativeLayout mainView = new RelativeLayout(this);
		mainView.setLayoutParams(FILL_PARENT);
		mainView.setBackgroundColor(Color.BLACK);

		cameraFrame = new RelativeLayout(this);
		cameraFrame.setLayoutParams(FILL_PARENT);
		cameraFrame.setBackgroundColor(Color.BLACK);

		imgCapture = new ImageView(this);
		imgCapture.setLayoutParams(WRAP_CONTENT);
		imgCapture.setImageResource(R.drawable.capture_icon);
		final String fileName = getIntent().getExtras().getString(PICTURE_NAME_PROPERTY);
		Imagebackground = new RelativeLayout(this);
		Imagebackground.setLayoutParams(FILL_PARENT);
		Imagebackground.setVisibility(View.GONE);
		Imagebackground.setBackgroundColor(Color.BLACK);
		final ButtonComponent btnSave = new ButtonComponent(this, R.style.CustomButton);
		btnSave.setLayoutParams(BTN_WRAP_CONTENT);
		btnSave.setText("GRABAR");
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				btnSave.setEnabled(false);
				presionadas+=1;
				if (presionadas > 1) {
					presionadas = 0;
					return;
				}
				Date date = new Date();
				String Title = new Timestamp(date.getTime()).toString();
				Title = Title.substring(0, Title.indexOf('.'));
				Title = Title.replace('-', '_').replace(':', '_')
						.replace(' ', '_');
				MediaStore.Images.Media.insertImage(getContentResolver(),
						CaptureImage, Title, "");
				File fotoTemp = new File(ImagePath);
				File foto = new File(RUTA_FOTOS+ "/"+fileName);
				try {
					Util.copy(fotoTemp, foto);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (mOnPictureTaken != null) {
					mOnPictureTaken.pictureTaken(CaptureImage, fotoTemp);
					
				}
				CameraClass.this.finish();
			}
		});
		ButtonComponent btnCancel = new ButtonComponent(this, R.style.CustomButton);
		btnCancel.setLayoutParams(BTN_WRAP_CONTENT);
		btnCancel.setText("DESCARTAR");
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				shouldGoBack = true;
				onBackPressed();
			}
		});

		LinearLayout ButtonLayout = new LinearLayout(this);
		ButtonLayout.setLayoutParams(WIDTH_FIIL_PARENT);
		ButtonLayout.setOrientation(LinearLayout.HORIZONTAL);
		ButtonLayout.addView(btnSave);
		ButtonLayout.addView(btnCancel);

		imgBackgroundImage = new ImageView(this);
		imgBackgroundImage.setLayoutParams(FILL_PARENT);
		Imagebackground.addView(imgBackgroundImage);
		Imagebackground.addView(ButtonLayout);

		mainView.addView(cameraFrame);
		mainView.addView(imgCapture);
		mainView.addView(Imagebackground);

		RelativeLayout.LayoutParams btnSaveLayouParams = (RelativeLayout.LayoutParams) ButtonLayout
				.getLayoutParams();
		btnSaveLayouParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		ButtonLayout.setLayoutParams(btnSaveLayouParams);

		RelativeLayout.LayoutParams imgCaptureLayouParams = (RelativeLayout.LayoutParams) imgCapture
				.getLayoutParams();
		imgCaptureLayouParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		imgCaptureLayouParams.addRule(RelativeLayout.CENTER_VERTICAL);
		imgCapture.setLayoutParams(imgCaptureLayouParams);

		setContentView(mainView);
	}

	public void makeCameraVisible(boolean cameraVisible) {
		if (cameraVisible) {
			shouldGoBack = true;
			cameraFrame.setVisibility(View.VISIBLE);
			imgCapture.setVisibility(View.VISIBLE);
			Imagebackground.setVisibility(View.GONE);
			CaptureImage.recycle();
		} else {
			shouldGoBack = false;
			cameraFrame.setVisibility(View.GONE);
			imgCapture.setVisibility(View.GONE);
			Imagebackground.setVisibility(View.VISIBLE);
		}

	}

	@Override
	public void onBackPressed() {
		if (shouldGoBack) {
			super.onBackPressed();
		} else {
			makeCameraVisible(true);
		}
	}
	
	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type){
	      return Uri.fromFile(getOutputMediaFile(type));
	}
	
	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "dnce");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("dnce", "failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMG_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "VID_"+ timeStamp + ".mp4");
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
}