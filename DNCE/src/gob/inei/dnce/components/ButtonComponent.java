package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class ButtonComponent extends Button implements FieldComponent {
	
	private Context context;
	
	public ButtonComponent(Context context, int style) {
		this(new ContextThemeWrapper(context, style), null, android.R.attr.textViewStyle);		
	}
	
	public ButtonComponent(Context context) {
		super(context);
		this.context = context;
		this.setSize(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		this.setPadding(5, 0, 5, 0);
		this.setTypeface(this.getTypeface(), Typeface.BOLD);
		this.setFocusable(true);
		this.setFocusableInTouchMode(true);
		//this.setBackgroundColor(getResources().getColor(R.color.griscabece));
		
		this.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setFocusableInTouchMode(false);
				return false;
			}
		});
	}
	
	public ButtonComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this.setPadding(5, 0, 5, 0);
		this.setTypeface(this.getTypeface(), Typeface.BOLD);
		this.setFocusableInTouchMode(true);
		this.setBackgroundColor(getResources().getColor(R.color.griscabece));
	}
	
	public ButtonComponent(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		this.setFocusable(true);
		this.setFocusableInTouchMode(true);
		this.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setFocusableInTouchMode(false);
				return false;
			}
		});
	}
	
	public ButtonComponent(Context context, int question, String tags) {
		super(context);
		this.context = context;
		this.setText(getResources().getString(question));
		this.setTag(tags);
		this.setPadding(5, 0, 5, 0);
		this.setTypeface(this.getTypeface(), Typeface.BOLD);
		this.setFocusableInTouchMode(true);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
	
	public void setSize(int ancho, int alto) {		
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		lp.leftMargin = 20;		
		lp.rightMargin = 20;
		lp.topMargin = 10;
		lp.bottomMargin = 10;
		lp.gravity = Gravity.CENTER;
		this.setLayoutParams(lp);
	}
	public void setSizeFloat(float ancho, float alto) {		
		LayoutParams lp = new LayoutParams((int)Util.getTamañoEscaladoFloat(this.context, ancho), (int)Util.getTamañoEscaladoFloat(this.context, alto));
		lp.leftMargin = 20;		
		lp.rightMargin = 20;
		lp.topMargin = 10;
		lp.bottomMargin = 10;
		lp.gravity = Gravity.CENTER;
		this.setLayoutParams(lp);
	}
	
	public void setDrawableTop(int topImage) {
		this.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);
		this.setPadding(40, 25, 40, 25);
		this.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(topImage), null, null);		
	}
	
	private Drawable getDrawable(int image) {
		return getResources().getDrawable(image);
	}
	
	public ButtonComponent drawableTop(int topImage) { this.setDrawableTop(topImage); return this;}
//	public ButtonComponent size(int ancho, int alto) { this.setSize(ancho, alto); return this;}
	public ButtonComponent size(float ancho, float alto) { this.setSizeFloat(ancho, alto); return this;}
	public ButtonComponent text(int text) { this.setText(getResources().getString(text)); return this;}
	public ButtonComponent image(int source) {
		this.setBackgroundColor( getResources().getColor(R.color.transparente));
		this.setBackgroundDrawable(getResources().getDrawable(source)); 
	return this;}	

	@Override
	public void setValue(Object aValue) { 
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getValueClass() {
		return ButtonComponent.class;
	}
	
	@Override
	public boolean requestFocus(int direction, Rect previouslyFocusedRect) {
		this.setFocusableInTouchMode(true);
		return super.requestFocus(direction, previouslyFocusedRect);
	}
}
