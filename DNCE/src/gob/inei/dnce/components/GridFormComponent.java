package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogComponent.TIPO_ICONO;
import gob.inei.dnce.components.RadioGroupOtherField.ORIENTATION;
import gob.inei.dnce.interfaces.Item;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.interfaces.RespondibleDialog;
import gob.inei.dnce.interfaces.Respuesta;
import gob.inei.dnce.listeners.GridFormComponentListener;
import gob.inei.dnce.listeners.OnGuardarPregunta;
import gob.inei.dnce.listeners.OnGenerarCodigo;
import gob.inei.dnce.listeners.OnMostrarCodigo;
import gob.inei.dnce.listeners.OnSetValue;
import gob.inei.dnce.listeners.GridFormComponentRestriccion;
import gob.inei.dnce.util.ItemImpl;
import gob.inei.dnce.util.RespuestaLabel;
import gob.inei.dnce.util.RespuestaRadioGroup;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.Toast;

/**
 * 
 * @author Rdelacruz
 * @since 2015
 */
public class GridFormComponent 
extends GridComponent2 
implements View.OnClickListener, View.OnLongClickListener
{

	private final int altoItem, anchoItem;
	private final Fragment fragmento;
	private final Class<? extends DialogFragmentComponentItem> claseDialogo;
	
	private final List<ItemImpl> listaItems;
	private final Map<String,ItemImpl> mapItems;
	private final List<LinearLayout> trOtrosAlFinal;
	private final boolean modoResumen;
	
	private GridFormComponentRestriccion restriccionInmediata;
	private GridFormComponentListener listener;
	private OnGuardarPregunta onGuardar; 
//	private OnEliminarPregunta onEliminarOtroDinamico; 
	
	private boolean construido = false;	
	private LinearLayout lyAgregarOtros;
		
	private final int anchoRadioGroupDefault = 220;//Para el caso que muestran su numero se necesita este ancho
		
	private enum Estado { INICIAL, SI_PRESIONADO, DIALOGO_ABIERTO};
	private Estado estado;
	
	
	private boolean concatenarCodigo;
	private boolean mostrarValorSiNo;//1. Si; 2. No
//	private boolean eliminarOtrosAgregados;
	private OnMostrarCodigo onMostrarCodigo;
	
	private View contenedores[];
	
	private final int LONGITUD_OTROS = 250;
	
	private boolean activarNoCorresponde;
	
	private Integer anchoRadioGroup;
	
	public static enum Valor {
		SI(1),
		NO(2),
		NO_CORRESPONDE(3),		
		MISSING(9);
		
		private final int valor;
		private Valor(int valor) {
			this.valor=valor;
		}
		
		
		protected final int getValor() {
			return valor;
		}

		protected final String getValorString() {
			return valor+"";
		}

		public static Valor getEnum(String valor) {
			if(valor!=null && !valor.isEmpty()) {
				int v = Integer.parseInt(valor);
				if(v==MISSING.getValor())
					return MISSING;
				
				int i = v - 1;
				return Valor.values()[i];
			}
			return null;
		}
		
	}
	
	public static enum TipoOpciones {
		SPINNER,
		PICKER;
	}

	public GridFormComponent(
			Fragment fragmento, 
			int altoItem, int anchoItem, 
			Class<? extends DialogFragmentComponentItem> claseDialogo) {
		this(fragmento,altoItem,anchoItem,claseDialogo,false,0);		
	}
	
	public GridFormComponent(
			Fragment fragmento, 
			int altoItem, int anchoItem, 
			Class<? extends DialogFragmentComponentItem> claseDialogo,
			boolean modoResumen, int nColumnasAdicionales) {
		super(fragmento.getActivity(), 2 + nColumnasAdicionales);
		this.altoItem = altoItem;
		this.anchoItem = anchoItem;
		this.fragmento = fragmento;
		this.claseDialogo = claseDialogo;
		this.listaItems = new ArrayList<ItemImpl>();
		this.mapItems = new HashMap<String, ItemImpl>();
		this.trOtrosAlFinal = new ArrayList<LinearLayout>();
		this.modoResumen = modoResumen;
		this.concatenarCodigo = true;
		
		this.mostrarValorSiNo = true;
		this.activarNoCorresponde = false;
		
//		this.eliminarOtrosAgregados = false;
		this.contenedores = null;
//		this.onEliminarOtroDinamico = null;
		estado = Estado.INICIAL;
		
		this.anchoRadioGroup = null;
		
		//POR DEFECTO
		onMostrarCodigo = new OnMostrarCodigo() {
			@Override
			public String getCodigo(String codigo) {				
				return codigo;
			}
		};
	}
	
	public void setAnchoRadioGroup(Integer anchoRadioGroup) {
		this.anchoRadioGroup = anchoRadioGroup;
	}

	@SuppressWarnings("unused")
	private void onCambioValor(RadioGroupOtherField rg) throws Exception {
		getItem(rg).getRespuesta().onCambioValor();
	}
	
	private ItemImpl getItem(RadioGroupOtherField rg) {
		return (ItemImpl) mapItems.get((String)rg.getTag());
	}
	
	
	//NOTA: En la practica este m�todo es solo para RespuestaRadioGroup, ya que RespuestaLabel tiene su propio onClick()
	@Override
	public void onClick(View v) {
		
		try {
			//Toast.makeText(getContext(), "onClick "+v.getClass(), Toast.LENGTH_LONG).show();
			
			boolean exito = false;
			String msj = null; 
			
			if(estado==Estado.INICIAL) {
				int i = (Integer) v.getTag();
				RadioGroupOtherField rg = (RadioGroupOtherField) v.getTag(R.id.padre);				
				ItemImpl itActual = getItem(rg);				
				RespuestaRadioGroup resp = (RespuestaRadioGroup) itActual.getRespuesta();
				
				if(i==1) {//SI					
					msj = restriccionInmediata!=null ? restriccionInmediata.validar(itActual.getCodigo(), Valor.SI) : null;
					
					if(msj==null) {
						exito = true;
						estado = Estado.SI_PRESIONADO;						
						if(abrirDialogo(itActual)) {
							if(!modoResumen) {
								resp.onCambioValor(i+"");
							}	
						}
						else {
							estado = Estado.INICIAL;
						}							
						
					}
					else {
						if(resp.isTipoSI_NO()) {
							//PASE AUTOMATICO AL NO, si no cumple restriccion 
							if(restriccionInmediata==null || restriccionInmediata.validar(itActual.getCodigo(),Valor.NO)==null) {
								resp.setValor("2");
							}
						}
					}
				}
				else if(i==2) {//NO
					msj = restriccionInmediata!=null ? restriccionInmediata.validar(itActual.getCodigo(),Valor.NO) : null;
					
					if(msj==null) {
						exito = true;
						if(!modoResumen) {							
							resp.onCambioValor(i+"");
							
							if(listener!=null) {							
								boolean todosNO = true;
								String valorNO = Valor.NO.getValorString();
								for(ItemImpl it: listaItems) {
									String val = it.getRespuesta().getValor();
									if(!(val!=null && val.equals(valorNO))) {
										todosNO = false;
										break;
									}
								}								
								
								if(todosNO)
									listener.onTodosNo();
							}
						}
					}
					else {
						//PASE AUTOMATICO AL SI, si no cumple restriccion (DESCARTADO)
						//Es mejor que el usuario presione SI para ingresar los datos del Dialog
						/*
						if(resp.isTipoSI_NO()) {
							if(restriccion==null || restriccion.validar(itActual.getCodigo(), true)==null) {
								itActual.getRespuesta().setValor("1");
							}
						}
						*/
					}
				}	
				else if(i==3) {//NO CORRESPONDE
					msj = restriccionInmediata!=null ? restriccionInmediata.validar(itActual.getCodigo(), Valor.NO_CORRESPONDE) : null;
					
					if(msj==null) {
						exito = true;
					}
					else {
						//con exito=false se limpia
						
						//resp.setValor("");//NO USAR sale ERROR de parseo
					}
				}
			}
			
			if(msj!=null) {				
				mostrarMensaje(msj);
			}
			
			if(exito) {
				OnClickListener onClickListenerRadioButton = (OnClickListener)v.getTag(R.id.onClickListener);
				if(onClickListenerRadioButton!=null)
					onClickListenerRadioButton.onClick(v);
			} else {
				RadioButton rb = (RadioButton) v;
				rb.setChecked(false);
			}
		}
		catch(Exception e) {
			manejarExcepcion(e);
		}
		
	}

	private void mostrarMensaje(String mensaje) {
		Respondible r = new Respondible() {
			@Override
			public void onCancel() {
				
			}
			
			@Override
			public void onAccept() {
				
			}
		}; 
		
		DialogComponent dialog= new DialogComponent(this.getContext(), r,
                TIPO_DIALOGO.NEUTRAL, getResources().getString(R.string.app_name),
                mensaje, TIPO_ICONO.ALERT);  
  		dialog.showDialog();
  		
  		/*
  		ToastMessage.msgBox(this.getContext(), mensaje,
				ToastMessage.MESSAGE_INFO,
				ToastMessage.DURATION_LONG);
		*/
	}
	
	private void manejarExcepcion(Exception e) {
		String mensaje = "ERROR: "+e.toString();
		
		Toast toast = Toast.makeText(this.getContext(),mensaje,Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
		toast.show();
		
		e.printStackTrace();
	}
	
	@Override
	public boolean onLongClick(View v) {
		
		RadioGroupOtherField rg = (RadioGroupOtherField) v.getTag(R.id.padre);				
		final ItemImpl it = (ItemImpl) mapItems.get((String)rg.getTag());
		
		FragmentManager fm = fragmento.getFragmentManager();
		
		OnLongDialog dialogo = new OnLongDialog();		
		dialogo.setCodigo(null);
		dialogo.setTitulo("Desea limpiar el valor de '"+it.toString()+"'");
		dialogo.setParent(fragmento);
		dialogo.setAncho(DialogFragmentComponent.MATCH_PARENT);
		dialogo.setResponsibleListener(new RespondibleDialog() {
			
			@Override
			public void onCancel() {
				
			}
			
			@Override
			public void onAccept() {
				try {
					it.getRespuesta().setValor(null);
				} catch (Exception e) {
					manejarExcepcion(e);
				}
			}
			
			@Override
			public void onCerrar() {
				
			}
		});	
				
		dialogo.show(fm, "dialogTag");
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		//Toast.makeText(getContext(), "onInterceptTouchEvent "+ev.getClass().getDeclaringClass(), Toast.LENGTH_LONG).show();
		
		if(!modoResumen) {
			if(estado==Estado.INICIAL) {
				//Actualizando estados de RadioGroup antes de onClick
				for(ItemImpl it: listaItems) {
					((RespuestaRadioGroup)it.getRespuesta()).registrarValor();
				}
			}
		}
		return super.onInterceptTouchEvent(ev);
	}

	/*
	@Override
	public void setLayoutParams(android.view.ViewGroup.LayoutParams params) {
		super.setLayoutParams(params);		
		component();//para renderizar el GriComponent2
	}
	*/
	
	public GridComponent2 component(){
		if(!construido) {
			super.component();			
			
			/*
			//TableRow.LayoutParams lp = new TableRow.LayoutParams();
			//lp.span = 2;
			TableRow tr = new TableRow(getContext());
			TextView tv = new TextView(getContext());
			tv.setText("PRUEBA");
			//addView(addLayoutObject(tv,2,true));			
			//tly.addView(tr);
			*/
			
			agregarOtrosDinamicos();
			
			construido = true;
		}
		return this;
	}
	
	private void agregarOtrosDinamicos() {
		//ver porque se borran las filas al cambiar de orientacion
		if(lyAgregarOtros!=null)
			addView(lyAgregarOtros);
					
		for(LinearLayout ly : trOtrosAlFinal) {
			addView(ly);
		}
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		agregarOtrosDinamicos();
	}
	
	public Data agregarEspecifique(String codigo, int etiquetaID) {
		return agregarEspecifique(codigo,getResources().getString(etiquetaID));
	}
	
	public Data agregarEspecifique(String codigo, String etiqueta) {
		return agregarEspecifique(codigo,etiqueta,false);
	}
	
	public Data agregarEspecifique(String codigo, int etiquetaID, boolean agregarAlFinal) {
		return agregarEspecifique(codigo,getResources().getString(etiquetaID), agregarAlFinal);
	}
	
	public Data agregarEspecifique(String codigo, String etiqueta, boolean agregarAlFinal) {
		ItemImpl it = agregarEspecifiquePrivado(codigo,etiqueta,agregarAlFinal);
		Data d = new Data();
		d.textField = it.getTxtEspecifiqueEtiqueta();
		d.radioGroup = it.getRadioGroup();
		return d;
	}
	
	//Codigo Externo: El que se ve
	//Codigo Interno: El que pasa al Dialog
	private ItemImpl agregarEspecifiquePrivado(String codigoInterno, String etiqueta, boolean agregarAlFinal) {
		String codigoExterno = onMostrarCodigo.getCodigo(codigoInterno);
		
		int anchoLabel = 0;
		int altoLabel = altoItem;
		
		LabelComponent lblComp = new LabelComponent(getContext())
		.textSize(20).negrita().colorFondo(R.color.griscabece);
		
		if(etiqueta==null || etiqueta.trim().isEmpty()) {
			if(concatenarCodigo) {
				anchoLabel = 35*codigoExterno.length();//HEURISTICA					
				lblComp.setText(codigoExterno+".");
			}
		}
		else {
			anchoLabel = (int)(anchoItem*0.5);
			
			if(concatenarCodigo) {				
				lblComp.setText(codigoExterno+". "+etiqueta);
			}
			else {
				lblComp.setText(etiqueta);
			}
			
			int n = etiqueta.length(); 
			if(n>20) {//HEURISTICA
				altoLabel += (n/25)*altoItem;
			}
		}
		lblComp.size(altoLabel, anchoLabel);
		
		TextField tf = new TextField(getContext())
		.alfanumerico().size(altoItem, anchoItem - anchoLabel -10)
		.maxLength(LONGITUD_OTROS).soloTexto()
		;		
		tf.setHint("(Especifique)");
		((LinearLayout.LayoutParams)tf.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;
		
		//if(nombreInicial!=null)
		//	tf.setValue(nombreInicial);
		
		LinearLayout lyB = new LinearLayout(getContext());
		lyB.setOrientation(LinearLayout.HORIZONTAL);
		lyB.addView(lblComp);
		lyB.addView(tf);
						
		ItemImpl it = crearItem(codigoInterno, codigoExterno, etiqueta);		
		it.setRequerido(false);		
		it.setTxtEspecifiqueEtiqueta(tf);	
		
		RespuestaRadioGroup.TIPO tipo = agregarAlFinal? RespuestaRadioGroup.TIPO.OTRO_DINAMICO : RespuestaRadioGroup.TIPO.OTRO_FIJO;
		crearComponenteResultado(it,lblComp, tipo);
		
		if(agregarAlFinal) {
			it.setOtro(true);
			it.setSiRequerido(true);
//			it.setEliminarOtro(eliminarOtrosAgregados);
			
			LinearLayout ly = new LinearLayout(getContext());
			//ly.setOrientation(LinearLayout.VERTICAL);
			ly.setLayoutParams(new TableRow.LayoutParams(
			TableRow.LayoutParams.MATCH_PARENT,
			2*altoItem
			));
			
			ly.addView(lyB);
			ly.addView(it.getRespuesta().getView());
			
			if(construido) {
				addView(ly);
			}
					
			trOtrosAlFinal.add(ly);
		}
		else {
			lyB.setLayoutParams(new LinearLayout.LayoutParams(anchoItem,altoLabel));			
			addComponent(lyB);			
			addComponent(it.getRespuesta().getView());
		}
				
		return it;
	}
		
	@SuppressLint("DefaultLocale")
	private <T extends Item> ItemImpl agregarEspecifiqueDinamico(TipoOpciones tipoOpciones, String codigoInterno, String etiquetaInicial, 
			boolean agregarAlFinal, final List<T> opciones) {
		
		final ItemImpl it = agregarEspecifiquePrivado(codigoInterno,null,agregarAlFinal);
		
		if(opciones!=null) {
			if(tipoOpciones==TipoOpciones.SPINNER) {
				it.getTxtEspecifiqueEtiqueta().setOnLongClickListener(new OnLongClickListener() {			
					@Override
					public boolean onLongClick(View v) {
						if(!opciones.isEmpty()) {
							FragmentManager fm = fragmento.getFragmentManager();
							OtroDialog<T> dialogo = new OtroDialog<T>(it.getTxtEspecifiqueEtiqueta(),opciones);				
							dialogo.setTitulo(it.toString());				
							dialogo.setParent(fragmento);
							dialogo.setAncho(DialogFragmentComponent.MATCH_PARENT);
							dialogo.show(fm, "OtroTag");
						}
						return false;
					}
				});		
			}			
		}
		
		if(etiquetaInicial!=null) {
			it.getTxtEspecifiqueEtiqueta().setValue(etiquetaInicial.toUpperCase());
		}
		
		return it;
	}

//	public <T1 extends Item, T2 extends Item> void agregarEspecifiqueDinamico(List<T1> itemsIniciales, List<T2> opciones) {
//		agregarEspecifiqueDinamico(itemsIniciales,opciones,null);
//	}
	
	public <T1 extends Item, T2 extends Item> void agregarEspecifiqueDinamico(List<T1> itemsIniciales, OnGenerarCodigo onGenerarCodigo) {
		agregarEspecifiqueDinamico(itemsIniciales, TipoOpciones.SPINNER, null, onGenerarCodigo);
	}
	
	public <T1 extends Item, T2 extends Item> void agregarEspecifiqueDinamico(List<T1> itemsIniciales, TipoOpciones tipoOpciones, List<T2> opciones, OnGenerarCodigo onGenerarCodigo) {
		//VALORES INICIALES
		for(Item it : itemsIniciales) {
			String codigo = it.getCodigo();
			ItemImpl ite = agregarEspecifiqueDinamico(tipoOpciones,codigo, null, true, opciones);
			String nombreInicial = it.getNombre();
			if(nombreInicial!=null)
				ite.getTxtEspecifiqueEtiqueta().setText(nombreInicial);
		}
		
		//CREANDO EL BOTON AGREGAR
		crearBotonAgregarOtros(tipoOpciones,opciones,onGenerarCodigo);
	}
	
	private <T2 extends Item> void crearBotonAgregarOtros(final TipoOpciones tipoOpciones, final List<T2> opciones, final OnGenerarCodigo onGenerarCodigo) {
		//Creando el boton AGREGAR
		ButtonComponent btnAgregarOtros = new ButtonComponent(getContext(),
				gob.inei.dnce.R.style.btnStyleButtonGreen)
				.size(200,altoItem);
		btnAgregarOtros.setText("AGREGAR OTROS");
		
		
		lyAgregarOtros = new LinearLayout(getContext());
		lyAgregarOtros.setGravity(Gravity.CENTER_VERTICAL);
		lyAgregarOtros.setOrientation(LinearLayout.VERTICAL);
		lyAgregarOtros.setLayoutParams(new TableRow.LayoutParams(
				TableRow.LayoutParams.MATCH_PARENT,
				3*altoItem//TableRow.LayoutParams.WRAP_CONTENT//altoItem
				));
		
		if(tipoOpciones==TipoOpciones.SPINNER) {
			btnAgregarOtros.setOnClickListener(new View.OnClickListener() {			
				@Override
				public void onClick(View v) {
					agregarEspecifique(tipoOpciones, opciones, null, true, onGenerarCodigo);				
				}
			});
			lyAgregarOtros.addView(btnAgregarOtros);
		}
		else if(tipoOpciones==TipoOpciones.PICKER) {
			LinearLayout lyPicker = new LinearLayout(getContext());
			lyPicker.setOrientation(LinearLayout.HORIZONTAL);
			lyPicker.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT, 
					2*altoItem));
			
			final TextPicker txtPicker = new TextPicker(getContext());
			for(Item it : opciones) {
				txtPicker.agregar(it.getCodigo(), it.getNombre());
			}
			
			lyPicker.addView(txtPicker);
			
			btnAgregarOtros.setOnClickListener(new View.OnClickListener() {			
				@Override
				public void onClick(View v) {
					TextPicker.Item it = txtPicker.getItemSeleccionado();
					
					if(it!=null)
						agregarEspecifique(tipoOpciones, opciones, it.etiqueta, true, onGenerarCodigo);				
				}
			});
			lyPicker.addView(btnAgregarOtros);
			
			lyAgregarOtros.addView(lyPicker);
		}
	}
	
//	private String getCodigoExterno() {
//		//Obteniendo ultimo key
//		Item ultimo = listaItems.get(listaItems.size()-1);
//		String key_anterior = ultimo.getCodigo();
//		int nFila = listaItems.size() + 1;
//		return onMostrarCodigo.getCodigoMostrado(nFila, key_anterior);
//	}
	
	public <T extends Item> void agregarEspecifique(
			TipoOpciones tipoOpciones,
			List<T> opciones, 
			String etiquetaInicial,
			boolean agregarAlFinal, 
			OnGenerarCodigo onGenerarCodigo) {		
		
		//Obteniendo ultimo key
		Item ultimo = listaItems.get(listaItems.size()-1);
		String key_anterior = ultimo.getCodigo();
		/*
		int k_anterior = Integer.parseInt(key_anterior);
		
		//Equivale a: key = intToString(k_anterior+1,key_anterior.length())
		int digits = key_anterior.length();
		char[] zeros = new char[digits];
        Arrays.fill(zeros, '0');        
        DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
        String key = df.format(k_anterior+1);
        */
		
        int nfila = listaItems.size() + 1;
        String codigoInternoGenerado = onGenerarCodigo.getCodigo(nfila, key_anterior);
        agregarEspecifiqueDinamico(tipoOpciones,codigoInternoGenerado, etiquetaInicial, agregarAlFinal, opciones);
	}
	
	public LabelComponent agregarTitulo(int tituloID) {
		return agregarTitulo(this.getResources().getString(tituloID));
	}
	
	public LabelComponent agregarTitulo(String titulo) {
		int altoLabel = LinearLayout.LayoutParams.WRAP_CONTENT;
		/*
		int altoLabel = altoItem;		
		if(titulo.length()>45) {//HEURISTICA
			altoLabel = 2*altoItem;
		}
		*/		
		LabelComponent lbl1 = new LabelComponent(this.getContext())
		.size(altoLabel, anchoItem)
		.textSize(22)		
		.centrar().negrita().colorFondo(R.color.celesteclaro)
		.text(titulo)
		;
		
		addComponent(lbl1,2);
		
		return lbl1;
	}
	
	public RadioGroupOtherField agregar(String key, int etiquetaID) {
		return agregar(key, getResources().getString(etiquetaID));
	}
	
	public RadioGroupOtherField agregar(String codigo, String etiqueta) {
		return agregar(codigo,etiqueta,altoItem);
	}
	
	private ItemImpl crearItem(String codigoInterno, String codigoExterno, String etiqueta) {
		ItemImpl it = new ItemImpl();
		it.setCodigoInterno(codigoInterno);
		it.setCodigoExterno(codigoExterno);
		it.setNombre(etiqueta);
		it.setConcatenarCodigo(concatenarCodigo);
		return it;
	}
	
	public RadioGroupOtherField agregar(String codigo, String etiqueta, int alto) {
		ItemImpl it = crearItem(codigo, codigo, etiqueta);
		
		String etiquetaFinal = it.toString();
		
		if(etiquetaFinal.length()>=40)
			alto = 2*altoItem;
		if(etiquetaFinal.length()>=100)
			alto = 3*altoItem;
		
		LabelComponent lblEtiqueta = new LabelComponent(this.getContext())
		.size(alto, anchoItem)
		.textSize(20)		
		//.centrar()
		.negrita().colorFondo(R.color.griscabece)			
		;
		
		lblEtiqueta.text(etiquetaFinal);
		addComponent(lblEtiqueta);
				
		addComponent(crearComponenteResultado(it,lblEtiqueta,RespuestaRadioGroup.TIPO.NORMAL));
		
		return it.getRadioGroup();
	}
	
	private View crearComponenteResultado(final ItemImpl it, LabelComponent lblEtiqueta, RespuestaRadioGroup.TIPO tipo) {
		Respuesta respuesta = null;
		String codigo = it.getCodigo();	
		
		if(modoResumen) {
			
			OnClickListener listener = new OnClickListener() {				
				@Override
				public void onClick(View v) {					
					try {
						LabelComponent lblResultado = (LabelComponent) v;
						ItemImpl it = (ItemImpl) mapItems.get((String)lblResultado.getTag());
						abrirDialogo(it);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			
			lblEtiqueta.setOnClickListener(listener);	
			lblEtiqueta.setTag(codigo);
			
			LabelComponent lblResultado = new LabelComponent(this.getContext())
			.size(altoItem, 100)
			.textSize(20)		
			.centrar().negrita().colorFondo(R.color.griscabece)		
			;
			lblResultado.setOnClickListener(listener);
			
			respuesta = new RespuestaLabel(lblResultado);
		}
		else {
				
			RadioGroupOtherField rg = null;
			if(mostrarValorSiNo) {
				if(activarNoCorresponde)
					rg = new RadioGroupOtherField(getContext(), R.string.op_valor_si, R.string.op_valor_no, R.string.op_valor_no_corresponde);
				else
					rg = new RadioGroupOtherField(getContext(), R.string.op_valor_si, R.string.op_valor_no);
			}
			else {
				if(activarNoCorresponde)
					rg = new RadioGroupOtherField(getContext(), R.string.op_si, R.string.op_no, R.string.op_no_corresponde);
				else
					rg = new RadioGroupOtherField(getContext(), R.string.op_si, R.string.op_no);
			}
			
			int ancho;
			if(anchoRadioGroup==null) {
				if(activarNoCorresponde)
					ancho = anchoRadioGroupDefault + 50;
				else
					ancho = anchoRadioGroupDefault;
			}
			else {
				ancho = anchoRadioGroup;
			}
			
			if(activarNoCorresponde) {
				rg.size(altoItem+50, ancho);//para que entre no corresponde
				rg.orientation(ORIENTATION.VERTICAL);
			}
			else {
				rg.size(altoItem, ancho);
			}
			
			//rg.setTag(R.id.item,it);		
			rg.setTag(R.id.padre, this);
			
			int n = rg.getChildCount();
			for (int i = 0; i < n; i++) {
				View v = rg.getChildAt(i); 
				inicializarButton((RadioButton)v,rg);
			}
			
			RespuestaRadioGroup r = new RespuestaRadioGroup(rg, tipo);
			r.setListener(new RespuestaRadioGroup.Listener() {			
				@Override
				public void onCambioValor(boolean si) throws Exception {
					//GridFormComponent.this.onCambioValor(it,valor);					
					if(listener!=null)
						listener.onCambioValor(it.getCodigo(), si);
				}
			});
			
			respuesta = r;
		}		
		it.setRespuesta(respuesta);
		
		
		listaItems.add(it);
		mapItems.put(codigo, it);
		
		respuesta.getView().setTag(codigo);
		return respuesta.getView();	
	}
	
	private void inicializarButton(RadioButton rb,RadioGroupOtherField rg) {
		rb.setOnClickListener(this);	
		rb.setOnLongClickListener(this);		
		rb.setTag(R.id.padre,rg);
		
//		rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {			
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				String m = "hola mundo";
//				m = "abc";
//			}
//		});
	}
	
	@Deprecated
	public Data agregarEspecifiqueSI(String key, String etiqueta) {
		Data d = new Data();
		RadioGroupOtherField rg = this.agregar(key, etiqueta); 
		ItemImpl it = mapItems.get(key);
		
		rg.size(2*altoItem, 500);
		rg.orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
				
		d.textField = new TextField(getContext())
		.maxLength(50)
		.size(altoItem, 400)
		.hint(R.string.especifique)
		.alfanumerico();		
		//d.checkBox.setTag(R.id.textview, d.textField);		
		//Util.lockView(getContext(), true, d.textField);
		it.setTxtEspecifiqueSi(d.textField);
		
		RadioButton rb = rg.agregarEspecifique(0, d.textField);//agregando al SI
		inicializarButton(rb,rg);
		
		d.radioGroup = rg;
		
		return d;
	}

	//public
	@SuppressWarnings("unused")
	private void limpiarDialogos() throws Exception {
		for(ItemImpl it : listaItems) {
			if(onGuardar!=null) {
				onGuardar.onGuardar(it, false);
			}
		}
	}	
	
	private boolean abrirDialogo(final ItemImpl it) throws Exception {
		if(claseDialogo==null)
			return false;
		
		//para evitar que se abran 2 dialogos iguales, al presionar dos veces seguidas
		//if(!dialogoAbierto) {
			String nombre = "";
			if(it.isEspecifique())
				nombre = it.getEspecifique();
			else
				nombre = it.getNombre();
			
			if(nombre.trim().isEmpty()) {
				mostrarMensaje("Debe especificar el item '"+it.toString()+"'");
				deshacerValorRadioGroup(it);
				return false;
			}
			
			FragmentManager fm = fragmento.getFragmentManager();
			
			DialogFragmentComponentItem dialogo = claseDialogo.newInstance();		
			
			dialogo.setCodigo(it.getCodigo());
			dialogo.setTitulo(it.toString());
			dialogo.setItem(nombre);
			dialogo.setParent(fragmento);
			dialogo.setAncho(DialogFragmentComponent.MATCH_PARENT);
			dialogo.setResponsibleListener(new RespondibleDialog() {
				
				@Override
				public void onCancel() {
					try {
						deshacerValorRadioGroup(it);
					} catch (Exception e) {
						manejarExcepcion(e);
					}						
				}
				
				@Override
				public void onAccept() {
					
				}
				
				@Override
				public void onCerrar() {
					//dialogoAbierto = false;
					estado = Estado.INICIAL;
				}
			});
			
			/*
			Bundle args = new Bundle();
			args.putSerializable("detalle", detalle);
			args.putSerializable("index", index);
			f.setArguments(args);
			*/		
					
			dialogo.show(fm, "dialogTag");
						
			//dialogoAbierto = true;
			estado = Estado.DIALOGO_ABIERTO;
		//}
			
			return true;
	}
	
	private void deshacerValorRadioGroup(ItemImpl it) throws Exception {
		Respuesta r = it.getRespuesta();
		if(r instanceof RespuestaRadioGroup) {
			RespuestaRadioGroup rrg = (RespuestaRadioGroup)r;
			Integer valorAntesOnClick = rrg.getValorRegistrado();
			if(valorAntesOnClick==null //ningun valor seleccionado 
					|| valorAntesOnClick!=1) //seleccionado SI
				rrg.setValor(valorAntesOnClick);
		}
	}
	
	
	
	public class Data {
		public RadioGroupOtherField radioGroup;
		public TextField textField;
	}
	
	private void setRelevancia(ItemImpl it, boolean relevancia) {
		RadioGroupOtherField rg = it.getRadioGroup();
		//RadioGroupOtherField rg = mapRadioGroups.get(codigo);
				
		if(relevancia) {
			/*
			LabelComponent lb = (LabelComponent) rg.getTag(R.id.textview);
			SpannableString content = new SpannableString(lb.getText().toString());
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			lb.setText(content);
			 */
			
			rg.setVisibility(View.VISIBLE);
			Util.lockView(getContext(), false, rg);
		}
		else {
			rg.setVisibility(View.INVISIBLE);
			//rg.setVisibility(View.GONE);//para que el campo OTRO no ocupe espacio por gusto
			
			Util.lockView(getContext(), true, rg);
		}
		
		if(it.isEspecifique()) {
			Util.lockView(getContext(), !relevancia, it.getTxtEspecifiqueEtiqueta());
		}
		
		//rg.setTag(R.id.relevancia, relevancia);
		it.setRelevancia(relevancia);
	}
	
	public void setRelevancia(boolean relevancia) {
		for(ItemImpl it : listaItems) {
			setRelevancia(it,relevancia);
		}
		
		if(contenedores!=null) {
			int vis;
			if(relevancia)
				vis = View.VISIBLE;
			else
				vis = View.GONE;
			
			for(View v:contenedores) {
				v.setVisibility(vis);
			}
		}
	}
	
	public void setRelevancia(String codigo, boolean relevancia) {
		setRelevancia(mapItems.get(codigo),relevancia);
	}
	
	private Map<String,Item> getRespuestasAnteriores() throws Exception {
		List<? extends Item> listaRespuestasAnteriores = onGuardar.onAntesGuardar();
		Map<String,Item> mapRespuestasAnteriores = null;
		if(listaRespuestasAnteriores!=null) {
			mapRespuestasAnteriores = new HashMap<String,Item>();
			for(Item it : listaRespuestasAnteriores) {
				mapRespuestasAnteriores.put(it.getCodigo(), it);
			}		 
		}
		return mapRespuestasAnteriores;
	}
	
	public boolean guardarSinValidar() throws Exception {
		if(onGuardar==null) {
			throw new Exception("Listener onGuardar indefinido!!");
		}
		
		Map<String,Item> mapRespuestasAnteriores = getRespuestasAnteriores();
		for(ItemImpl it : listaItems) {
			String valor = it.getRespuesta().getValor();
			boolean guardado = guardarSinValidar(it, valor, mapRespuestasAnteriores);			
			if(!guardado)
				return false;
		}		
		return true;
	}
	
	public boolean guardar() throws Exception {		
		if(onGuardar==null) {
			throw new Exception("Listener onGuardar indefinido!!");
		}
		
		Map<String,Item> mapRespuestasAnteriores = getRespuestasAnteriores();
		
		for(ItemImpl it : listaItems) {
			Boolean guardado = guardar(it, true,mapRespuestasAnteriores);			
			if(guardado==null)
				continue;
			
			if(!guardado)
				return false;
		}
		
		onGuardar.onDespuesGuardar();	
		
		//if(mostrarMensaje) {
		//	ToastMessage.msgBox(this.getContext(), "GUARDADO",
		//			ToastMessage.MESSAGE_INFO,
		//			ToastMessage.DURATION_LONG);
		//}		
		return true;
	}
	
	public void guardar(RadioGroupOtherField rg) throws Exception {
		if(onGuardar==null) {
			//throw new Exception("Listener onGuardar indefinido!!");
			return ;
		}
		
		ItemImpl it = mapItems.get( (String)rg.getTag() );
		guardar(it, false, null);
	}
	
	private Boolean guardar(ItemImpl it, boolean validar, Map<String,Item> mapRespuestasAnteriores) throws Exception {
		Boolean guardar = true;
		
		
		String valor = it.getRespuesta().getValor();
		
		Respuesta r = it.getRespuesta();
		RespuestaRadioGroup rg = null;
		if(r instanceof RespuestaRadioGroup) {
			rg = (RespuestaRadioGroup)r;
		}
		
		if(it.isRelevante()) {
			if(validar) {
//				String item = it.toString();				
				String mensaje = it.getRespuesta().validar(valor);
				
				if(mensaje==null) {
					//	mensaje = onGuardar.onValidar(valor,it);
					
					if(!modoResumen) {//solo para RespuestaRadioGroup
						if(restriccionInmediata!=null)
							mensaje = restriccionInmediata.validar(it.getCodigo(), Valor.getEnum(valor));
					}
				}
							
				if(mensaje!=null) {
					if(mensaje.isEmpty()) {
						guardar = null; //se ignora el item						
					}
					else {
						mostrarMensaje(mensaje);
						it.requestFocus();
						guardar = false;
					}
				}
				
			}
		}
		
		if(rg!=null && rg.getTipo()==RespuestaRadioGroup.TIPO.OTRO_DINAMICO && guardar==null) {
			onGuardar.onEliminarOtroDinamico(it);
		}
		else if(guardar) {
			guardarSinValidar(it,valor,mapRespuestasAnteriores);			
		}		
		
		return guardar;
	}
	
	
	
	private boolean guardarSinValidar(ItemImpl it, String valor, Map<String,Item> mapRespuestasAnteriores) throws Exception {
		boolean ejecutarGuardar = true;
		
		//Para acelerar el guardado, ignoramos el guardado de items con el mismo valor que tenian anteriormente (en la BD)
		//cuando es Especifique siempre se deberia ejecutar para que guarde el campo ESPECIFIQUE.
		if(!it.isEspecifique() && mapRespuestasAnteriores!=null) {			
			Item respuestaAnterior = mapRespuestasAnteriores.get(it.getCodigo());
			String valorAnterior = respuestaAnterior!=null ? respuestaAnterior.getValor() : null;
			
			if(valor!=null && valorAnterior!=null && valor.equals(valorAnterior))
				ejecutarGuardar = false;
		}
		
		if(ejecutarGuardar) {
			boolean limpiar = false;
			
			if(it.isRelevante()) {
				//NO o MISSING
				if(it.getRespuesta().debeLimpiar()) {				
					//onGuardar.onLimpiar(it);
					limpiar = true;
				}
			}
			else {
				//NO RELEVANTE
				//onGuardar.onLimpiar(it);
				limpiar = true;
			}
			
			onGuardar.onGuardar(it, limpiar);
		}
		
		return true;
	}
	
	public static interface OnIterarItem {
		void iterar(RespuestaLabel rl);
	}
	
	public void iterarItems(OnIterarItem onIterarItem) throws Exception {
		if(modoResumen) {
			for(ItemImpl it : listaItems) {
				RespuestaLabel rl = (RespuestaLabel) it.getRespuesta();				
				onIterarItem.iterar(rl);
			}
		}
	}
		
	public void setValue(OnSetValue onSetValue) throws Exception {
		for(ItemImpl it : listaItems) {
			String codigo = it.getCodigo();
			String valor = onSetValue.getValue(codigo);
			it.getRespuesta().setValor(valor);
			if(it.isEspecifique()) {
				String nombreInicial = onSetValue.getEspecifique(codigo);
				if(nombreInicial!=null)
					it.getTxtEspecifiqueEtiqueta().setText(nombreInicial);
			}
		}
	}
	
	public void setOnGuardar(OnGuardarPregunta onGuardar) {
		this.onGuardar = onGuardar;
	}
	
	public void setSiRequerido(boolean siRequerido) {
		for(ItemImpl it : listaItems) {
			it.setSiRequerido(siRequerido);
		}
	}
	
	public void setDesactivarNo() {
		for(ItemImpl it : listaItems) {
			it.desactivarNo();
		}
	}
	
	public void setRequerido(boolean requerido) {
		for(ItemImpl it : listaItems) {
			it.setRequerido(requerido);
		}
	}
	
	public void setOmitible(boolean omitible) {
		for(ItemImpl it : listaItems) {
			it.setOmitible(omitible);
		}
	}
	
	public class OnLongDialog extends DialogFragmentComponentItem {

		@Override
		protected void buildFields(String codigo) throws Exception {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void cargarDatosOrThrow(String codigo) throws Exception {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void addComponents(LinearLayout contenedor, String codigo) {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected boolean onAceptar() throws Exception {
			return true;
		}
		
	}
		
	public class OtroDialog<T extends Item> extends DialogFragmentComponent {
		
		private String titulo;
		private TextField txtLabel;
		private List<T> lista;
		
		public OtroDialog(TextField txtLabel, List<T> lista) {
			setRetainInstance(true);
			this.txtLabel = txtLabel;
			this.lista = lista;
		}

		@Override
		public final View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			ScrollView viewRoot = createForm();
			LinearLayout layout = (LinearLayout) viewRoot.getChildAt(0);
			
			this.getDialog().setTitle(titulo);
			
			final SpinnerField spnLabel = new SpinnerField(getContext()).size(altoItem+20, LinearLayout.LayoutParams.MATCH_PARENT);
						
			List<Item> listaItems = new ArrayList<Item>();
			
			//Item vacio
	        ItemImpl it = new ItemImpl();
	        it.setCodigoInterno("");
	        it.setCodigoExterno("");
	        it.setNombre("");
			listaItems.add(it);
			
			//Otros Items
			for(T obj : lista) {
				listaItems.add(obj);
			}
			
			spnLabel.setAdapter(new EntitySpinnerAdapter<Item>(getContext(), android.R.layout.simple_spinner_item, listaItems));
			
			spnLabel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				
				private boolean inicializado = false;

				@SuppressLint("DefaultLocale")
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					Item it = (Item) spnLabel.getSelectedItem();
					
					if(inicializado) {
						String texto = it.getNombre().toUpperCase()
								.replace('�', 'A')
								.replace('�', 'E')
								.replace('�', 'I')
								.replace('�', 'O')
								.replace('�', 'U')
								;
						txtLabel.setValue(texto);
						
						dismiss();//cerrando el DIALOG
					}
					
					inicializado = true;
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					
				}
			});
			
			
			ButtonComponent btn = new ButtonComponent(getContext(),
					gob.inei.dnce.R.style.btnStyleButtonGreen)
					.size(200,altoComponente);
			btn.setText("Cancelar");
			btn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//grid.agregarEspecifique(lista, itAdp);
					dismiss();
				}
			});
			
			layout.addView(createQuestionSection(spnLabel,btn));
			
			return viewRoot;
		}
		
		@Override
		protected View createUI() {
			return null;
		}

		@Override
		protected void buildFields() throws Exception {
			
		}

		public String getTitulo() {
			return titulo;
		}

		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		
		
	}

	public void setListener(GridFormComponentListener listener) {
		this.listener = listener;
	}


	public void setRestriccionInmediata(GridFormComponentRestriccion restriccionInmediata) {
		this.restriccionInmediata = restriccionInmediata;
	}

	public void setMostrarSoloEtiqueta() {
		this.concatenarCodigo = false;
	}
	
	public String getValor(String codigo) {
		ItemImpl it = mapItems.get(codigo);
		return it!=null ? it.getRespuesta().getValor() : null;
	}

	public boolean isMostrarValorSiNo() {
		return mostrarValorSiNo;
	}

	public void setMostrarValorSiNo(boolean mostrarValorSiNo) {
		this.mostrarValorSiNo = mostrarValorSiNo;
	}


	public void setContenedor(View ... views) {
		this.contenedores = views;
	}
//	public void setOnEliminarOtroDinamico(OnEliminarPregunta onEliminarOtroDinamico) {
//		this.onEliminarOtroDinamico = onEliminarOtroDinamico;
//	}
//	

	public boolean isActivarNoCorresponde() {
		return activarNoCorresponde;
	}

	public void setActivarNoCorresponde(boolean activarNoCorresponde) {
		this.activarNoCorresponde = activarNoCorresponde;
	}

	public void setOnMostrarCodigo(OnMostrarCodigo onMostrarCodigo) {
		this.onMostrarCodigo = onMostrarCodigo;
	}		
	
	
}
