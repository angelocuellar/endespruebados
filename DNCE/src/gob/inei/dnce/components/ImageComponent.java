package gob.inei.dnce.components;

import gob.inei.dnce.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;

public class ImageComponent extends ImageButton{
	
	private int[] images;
	private int imageIndex = -1;

	public ImageComponent(Context context) {
		super(context);
		this.setSize(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		this.setFocusable(true);
		this.setFocusableInTouchMode(true);
	}
	
	public ImageComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this.setFocusableInTouchMode(true);
	}
	
	public ImageComponent(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public ImageComponent(Context context, final int image, final int image2) {
		super(context);
//		final int d = this.getDrawingCacheBackgroundColor();
		//this.setTag(tags);
		this.setImageDrawable(getResources().getDrawable(image));
		this.setBackgroundColor(getResources().getColor(R.color.transparente));
		//this.setFocusableInTouchMode(true);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		this.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.requestFocus();
				if(event.getAction()==MotionEvent.ACTION_DOWN){
					ImageComponent.this.setImageDrawable(getResources().getDrawable(image2));
				}
				if(event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL){
					ImageComponent.this.setImageDrawable(getResources().getDrawable(image));
				}
				return false;
			}
		});
	}
	
	public void setImages(int... images) throws IllegalArgumentException {
		if (images.length == 0) {
			throw new IllegalArgumentException("No puede agregar una arreglo de im�genes vacio.");
		}
		this.images = images;
		this.setBackgroundColor(getResources().getColor(R.color.transparente));
		this.setImageDrawable(getResources().getDrawable(images[0]));
		this.imageIndex = 0;
	}
	
	public void touch() {
		this.imageIndex += 1;
		if (this.imageIndex > this.images.length - 1) {
			this.imageIndex = 0;
		} 
		this.setImageDrawable(getResources().getDrawable(images[imageIndex]));
	}
	
	public void setImageIndex(int index) throws ArrayIndexOutOfBoundsException {
		if (images.length - 1  < index) {
			throw new ArrayIndexOutOfBoundsException("No existe esa imagen.");
		}
		this.imageIndex = index;
		this.setImageDrawable(getResources().getDrawable(images[imageIndex]));
	}
	
	public int getImageIndex() {
		return this.imageIndex;
	}

	protected void setSize(int ancho, int alto) {
		LayoutParams lp = new LayoutParams(ancho, alto);
		lp.gravity = Gravity.CENTER;
		this.setLayoutParams(lp);
	}

	public ImageComponent size(int ancho, int alto) { this.setSize(ancho, alto); return this;}
	public ImageComponent image(int image) { this.setImageDrawable(getResources().getDrawable(image)); return this;}
	public ImageComponent images(int... images) { this.setImages(images); return this;}
	//public ButtonComponent text(int text) { this.setText(getResources().getString(text)); return this;}
}
