package gob.inei.dnce.components;

import gob.inei.dnce.R;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ToastMessage {
	
	public static final String TAG = "ToastMessage";
	
	public static final int MESSAGE_INFO = 1;
	public static final int MESSAGE_ERROR = 2;
	
	public static final int DURATION_LONG = 1;
	public static final int DURATION_SHORT = 0;
	
	public static void msgBox(Activity activity, String mensaje, int type, int duration){
		
		type = type == MESSAGE_ERROR?MESSAGE_ERROR:MESSAGE_INFO;
		duration = duration == DURATION_LONG?DURATION_LONG:DURATION_SHORT;
		if(activity == null) {
			Log.d(TAG, "activity no puede ser null");
			return;
		}
		if(mensaje == null) mensaje="";
		
		LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();
		View layout_root = null, layout_body = null;
		
		layout_root = inflater.inflate(R.layout.custom_toast, (ViewGroup) activity.findViewById(R.id.toast_layout_root));
		layout_body = (LinearLayout) layout_root.findViewById(R.id.toast_layout_body);
		
		if(type == MESSAGE_ERROR) layout_body.setBackgroundResource(R.drawable.bg_error);
		else layout_body.setBackgroundResource(R.drawable.bg_highlight);
		layout_body.setPadding(8, 8, 8, 8);
		
		TextView text = (TextView) layout_body.findViewById(R.id.txtMessageToast);
		text.setText(mensaje);

		Toast toast = new Toast((Context)activity);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(duration);
		toast.setView(layout_root);
		toast.show();
	}
	
	public static void msgBox(Context context, String mensaje, int type, int duration){
		
		type = type == MESSAGE_ERROR?MESSAGE_ERROR:MESSAGE_INFO;
		duration = duration == DURATION_LONG?DURATION_LONG:DURATION_SHORT;
		if(mensaje == null) mensaje="";
		
		LayoutInflater inflate = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        
        LinearLayout layout_body = (LinearLayout) v.findViewById(R.id.toast_layout_body);
        
        if(type == MESSAGE_ERROR) layout_body.setBackgroundResource(R.drawable.bg_error);
		else layout_body.setBackgroundResource(R.drawable.bg_highlight);
		layout_body.setPadding(8, 8, 8, 8);
		
		TextView text = (TextView) layout_body.findViewById(R.id.txtMessageToast);
		text.setText(mensaje);

		Toast toast = new Toast(context);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(duration);
		toast.setView(v);
		toast.show();
	}
}
