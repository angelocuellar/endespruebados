package gob.inei.dnce.components;

import gob.inei.dnce.util.Util;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class LabelComponent extends TextView {
	
	private Context context;
	private int defaultHeightSize = 54;
	private int textSize = 16;
	private int cellSpacing = 2;
	public enum STYLE {STYLE, COLOR, TAMANIO}
	
	public LabelComponent(Context context, int style) {
		this(new ContextThemeWrapper(context, style));
	}
	
	public LabelComponent(Context context) {
		super(context);
		this.context = context;
		this.setSize(defaultHeightSize, LayoutParams.WRAP_CONTENT);
		this.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		this.setTextSize(textSize);
	}

	public void setSize(int alto, int ancho) {
		this.setSize(alto, ancho, 0, true);
	}
	public void setSizeFloat(float alto, float ancho) {
		LayoutParams lp = new LayoutParams((int)Util.getTamañoEscaladoFloat(this.context, ancho), (int)Util.getTamañoEscaladoFloat(this.context, alto));
		lp.setMargins(2, 2, 2, 2);
		this.setLayoutParams(lp);
	}

	public void setSize(int alto, int ancho, float peso, boolean hasMargin) {		
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		if(hasMargin){
			lp.gravity = Gravity.CENTER;
			lp.bottomMargin = cellSpacing;
			lp.topMargin = cellSpacing;
			lp.leftMargin = cellSpacing;
			lp.rightMargin = cellSpacing;
			this.setPadding(7, 3, 7, 3);
		}
		if (peso != 0) {
			lp.weight = peso;
		}
		this.setLayoutParams(lp);
	}
	
	private void posicionar(int posicion) {
		this.setGravity(posicion);
	}
	
	public void setColorFondo(int color) {
		this.setBackgroundColor(getResources().getColor(color));
	}
	
	public void setColorTexto(int color) {
		this.setTextColor(getResources().getColor(color));
	}
	
	public void setTextBold() {
        this.setTypeface(Typeface.DEFAULT_BOLD);
	}
	public void setTextBoldRange(int inicio,int fin)
	{
//		this.setTypeface(Typeface. )
		this.setText(getResources().getString(inicio));
	}
	
	public void setTextFormat(int text, STYLE style, int format, int from, int to) {
		String txt = getResources().getString(text);
		SpannableStringBuilder sps = new SpannableStringBuilder(txt);
		try {
			switch (style) {
				case STYLE:
					sps.setSpan(new StyleSpan(format), from, to, Spannable.SPAN_EXCLUSIVE_INCLUSIVE); break;
				case COLOR:
					sps.setSpan(new ForegroundColorSpan(format), from, to, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);break;
				case TAMANIO:
					sps.setSpan(new AbsoluteSizeSpan(format), from, to, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);break;
				default:
					break;
			}
		} catch (Exception e) {
			Log.e("LabelComponent", e.getMessage());
		}
		
		this.setText(sps);
	}
	
//	public LabelComponent size(int alto, int ancho) { this.setSize(alto, ancho); return this;}
	public LabelComponent size(float alto, float ancho) { this.setSizeFloat(alto, ancho); return this;}
	public LabelComponent size(int alto, int ancho, float peso) { this.setSize(alto, ancho, peso, true); return this;}
	public LabelComponent size(int alto, int ancho, float peso, boolean hasMarguin) { this.setSize(alto, ancho, peso, hasMarguin); return this;}
	public LabelComponent text(int text) { this.setText(getResources().getString(text)); return this;}
	public LabelComponent textFormat(int text, STYLE style, int format, int from, int to) { this.setTextFormat(text, style, format, from, to); return this;}
	public LabelComponent text(String text) { this.setText(text); return this;}
	public LabelComponent centrar() { this.posicionar(Gravity.CENTER); return this;}
	public LabelComponent alinearIzquierda() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.LEFT); return this;}
	public LabelComponent alinearDerecha() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.RIGHT); return this;}
	public LabelComponent textSize(float size) { this.setTextSize(size); return this;}
	public LabelComponent colorTexto(int color) { this.setColorTexto(color); return this;}
	public LabelComponent colorFondo(int color) { this.setColorFondo(color); return this;}
	public LabelComponent negrita() { this.setTextBold(); return this;}
	public LabelComponent padding(int left, int top, int right, int bottom) { this.setPadding(left, top, right, bottom); return this;}
}
