package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageViewField extends ImageView implements FieldComponent {
	
	public static final int BASE_DATOS = 1;
	public static final int ARCHIVO = 2;
	
	private String path;
	private Bitmap bitmap;
	private boolean zoom;
	private int placeToSave = BASE_DATOS;
	private String fileName;
	private String pathSave;
	private Context context;

	public ImageViewField(Context context) {
		super(context);
		this.context = context;
		this.setSize(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		this.setOnTouchListener(new View.OnTouchListener() {
			private Matrix savedMatrix = new Matrix();
			private Matrix matrix = new Matrix();
			private PointF start = new PointF();
			private PointF mid = new PointF();
			static final int NONE = 0;
			static final int DRAG = 1;
			static final int ZOOM = 2;
			int mode = NONE;
			float oldDist = 1f;
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(!zoom) return true;
				ImageView view = (ImageView) v;
				view.setScaleType(ImageView.ScaleType.MATRIX);
				float scale;

				// Handle touch events here...
				switch (event.getAction() & MotionEvent.ACTION_MASK) {

				   case MotionEvent.ACTION_DOWN: //first finger down only
				      savedMatrix.set(matrix);
				      start.set(event.getX(), event.getY());
				      Log.d("mode=DRAG", "mode=DRAG" );
				      mode = DRAG;
				      break;
				   case MotionEvent.ACTION_UP: //first finger lifted
				   case MotionEvent.ACTION_POINTER_UP: //second finger lifted
				      mode = NONE;
				      Log.d("mode=NONE", "mode=NONE" );
				      break;
				   case MotionEvent.ACTION_POINTER_DOWN: //second finger down
				      oldDist = spacing(event);
				      Log.d("oldDist", "oldDist=" + oldDist);
				      if (oldDist > 5f) {
				         savedMatrix.set(matrix);
				         midPoint(mid, event);
				         mode = ZOOM;
				         Log.d("mode=ZOOM", "mode=ZOOM" );
				      }
				      break;

				   case MotionEvent.ACTION_MOVE: 
				      if (mode == DRAG) { //movement of first finger
				         matrix.set(savedMatrix);
				         if (view.getLeft() >= -392){
				            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
				         }
				      }
				      else if (mode == ZOOM) { //pinch zooming
				         float newDist = spacing(event);
				         Log.d("newDist", "newDist=" + newDist);
				         if (newDist > 5f) {
				            matrix.set(savedMatrix);
				            scale = newDist / oldDist; //thinking i need to play around with this value to limit it**
				            matrix.postScale(scale, scale, mid.x, mid.y);
				         }
				      }
				      break;
				   }

				   // Perform the transformation
				   view.setImageMatrix(matrix);

				   return true; // indicate event was handled
			}
			
			private float spacing(MotionEvent event) {
			   float x = event.getX(0) - event.getX(1);
			   float y = event.getY(0) - event.getY(1);
			   return x;// FloatMath.sqrt(x * x + y * y);
			}
				
			private void midPoint(PointF point, MotionEvent event) {
			   float x = event.getX(0) + event.getX(1);
			   float y = event.getY(0) + event.getY(1);
			   point.set(x / 2, y / 2);
			}
		});
	}

	public ImageViewField(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.setSize(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	}

	public ImageViewField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		this.setSize(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	}
	
	public void setSize(int alto, int ancho) {
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		lp.gravity = Gravity.CENTER;
		lp.bottomMargin = 3;lp.leftMargin = 3;lp.topMargin = 3;lp.rightMargin = 3;
		this.setLayoutParams(lp);
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public int getPlaceToSave() {
		return placeToSave;
	}

	public void setPlaceToSave(int placeToSave) {
		this.placeToSave = placeToSave;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathSave() {
		return pathSave;
	}

	public void setPathSave(String pathSave) {
		this.pathSave = pathSave;
	}

	public ImageViewField size(int alto, int ancho) { this.setSize(alto, ancho); return this; }
	public ImageViewField path(String path) { this.setPath(path); return this; }
	public ImageViewField zoom() { this.zoom = true; return this; }
	public ImageViewField saveIn(int placeToSave) { this.setPlaceToSave(placeToSave); return this; }
	public ImageViewField fileName(String name) { this.setFileName(name); return this; }
	public ImageViewField pathSave(String path) { this.setPathSave(path); return this; }

	@Override
	public void setValue(Object aValue) {
		if (aValue != null && !(aValue instanceof Bitmap)) {
			return;
		}
		bitmap = (Bitmap) aValue;
		if (bitmap != null) {
			this.setImageBitmap(bitmap);
		} else {
			this.setImageResource(android.R.color.transparent);
		}
	}

	@Override
	public Object getValue() {
		return bitmap;
	}

	@Override
	public Class<?> getValueClass() {
		return Bitmap.class;
	}
}
