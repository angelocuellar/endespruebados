package gob.inei.dnce.components;

import gob.inei.dnce.util.Util;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;

public class DecimalField extends NumberField {

	private int decimales = 0;
	private List<BigDecimal> excepValues;

	public DecimalField(final Context context) {
		this(context, true);
	}
	
	public DecimalField(final Context context, boolean hasTextWatcher) {
		this(context, 0, hasTextWatcher);
	}

	public DecimalField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DecimalField(final Context context, int decimales, boolean hasTextWatcher) {
		super(context,hasTextWatcher);
		this.setInputType(getInputType(TextBoxField.INPUT_TYPE.DECIMALES));
		this.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		this.setDecimales(decimales);
		this.setPadding(4, 0, 7, 0);
	}

	public void setDecimales(int decimales) {
		this.decimales = decimales;
		if(this.decimales>0){
			this.setMaxLength(this.getMaxLength()+decimales+1);
			this.setTextWatcher(new Util.MyTextWatcher(this, Util.MyTextWatcher.DECIMAL).decimales(decimales));			
		}
	}
	
	//Debe permitir "a" cifras enteras y "b" cifras decimales. 
	// Lo mismo para el valor  MISSING
	//RAMON
	public DecimalField numeric(int a, int b) {		
		setDecimales(b);
		maxLength(a);//maxLength(a-1);
		getLayoutParams().width = 20*a;
		return this;
	}

	public Class<BigDecimal> getValueClass() {
		return BigDecimal.class;
	}

	/* Fluent API */
	public DecimalField maxLength(int length) {
		this.setMaxLength(length);
		return this;
	}

//	public DecimalField size(int alto, int ancho) {
//		super.setSize(alto, ancho);
//		return this;
//	}


	public DecimalField completarCaracteres(int espacios, String caracter) {
		this.setCompletarCaracteres(true, espacios);
		this.setCaracterCompletar(caracter);
		return this;
	}

	public DecimalField readOnly() {
		this.setReadOnly();
		return this;
	}

	public DecimalField visible() {
		this.setVisible();
		return this;
	}

	public DecimalField textAutomatico() {
		this.setTextAutomatico();
		return this;
	}

	public DecimalField hint(int id) {
		this.setHintText(id);
		return this;
	}

	public DecimalField callback(String callback) { this.setCallback(callback); return this; }
	public DecimalField callbackOnFocus(String callback) { this.setCallbackOnFocus(callback); return this; }
	public DecimalField decimales(int decimales) { this.setDecimales(decimales); return this; }
	public DecimalField alinearIzquierda() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.LEFT); return this;}
	public DecimalField reescribir(int numVeces) { this.setReescribir(numVeces); return this;}
	public DecimalField rango(final BigDecimal minimo, final BigDecimal maximo, final LinkedList otros, final BigDecimal omision) { this.setRango(minimo, maximo, otros, omision); return this;}
	public DecimalField size(float alto, float ancho) {super.setSizeFloat(alto, ancho); return this;}
	
	@Override
	public void verificarRangos() {
//		Log.e(this.getClass().getSimpleName(), "verificarRangos");
		this.setRangoOk(false);
		if (this.getValue() == null) {
			this.setRangoOk(true);
			return;
		}
		BigDecimal val = new BigDecimal(this.getValue().toString());
		BigDecimal min = new BigDecimal(this.getMinValue().toString());
		BigDecimal max = new BigDecimal(this.getMaxValue().toString());
		setMensajeRango("");
		if (getOmision() != null) {
			BigDecimal omision = new BigDecimal(getOmision().toString());
			if (val.compareTo(omision) == 0) {
				this.setRangoOk(true);	
				return;
			}
		}
		if (val.compareTo(min) < 0 || val.compareTo(max) > 0) {
			if (getExcepValues() == null && getOmision()==null) {
				this.setRangoOk(false);
				this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
					+ min + "\" hasta \"" + max
					+ "\" y no existe valor por omisi\u00f3n\"");
				return ;
			} else {
				if (getExcepValues() != null && getOmision()==null){
					if (excepValues.indexOf(val) == -1) {
						String omisiones = "";
						for (Object o : getExcepValues()) {
							BigDecimal i = new BigDecimal(o.toString());
							omisiones += i.toString() + ", ";
						}
						this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
								+ min + "\" hasta \"" + max + "\" y el valor por omisi\u00f3n es \""
								+ omisiones.substring(0, omisiones.length() - 1));
						return;
					}
				}
				else {
					this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
							+ min + "\" hasta \"" + max + "\" y el valor por omisi\u00f3n es \""
							+ getOmision().toString());
					return;
				}
				
			}			
		}
		this.setRangoOk(true);
	}

	@Override
	public List<? extends Object> getExcepValues() {
		return excepValues;
	}

	@Override
	public void setExcepValues(List<? extends Object> excepVals) {
		this.excepValues = (List<BigDecimal>) excepVals;
	}
	
	public void setRango(final BigDecimal minimo, final BigDecimal maximo, final LinkedList otros, final BigDecimal omision) {
		setMinValue(minimo);
		setMaxValue(maximo);
		setOmision(omision);
		setExcepValues(otros);
		setCallback("verificarRangos");
	}
	
	public final int getDecimales() {
		return decimales;
	}
}
