package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;

public abstract class TextBoxField extends EditText implements FieldComponent {

	public static enum INPUT_TYPE {
		ENTEROS, DECIMALES, TEXTO, EMAIL, TEXTO_MULTILINEA, TEXTO_NUMEROS, TELEFONO, TEXTO_NUMEROS_PUNTO
	};

	private int textSize = 19;
	protected Util.MyTextWatcher textWatcher;
	private Paint mPaint;
	private int maxLength = 50;
	protected List<String> callbacks = null;
	protected List<String> callbacksOnFocus = null;
	protected List<String> callbacksOnFocusGained = null;
	private String caracter = "0";
	private int espacios = 0;
	private boolean esCompletarCaracteres = false;
	private int vecesIngresado = 0;
	private int vecesIngresar = 1;
	private String valorAnterior = null;
	protected Object componentValue;
	protected Context context;
	
	private boolean readonly = false;

	public enum DIRECCION_COMPLETAR {
		DERECHA, IZQUIERDA
	};

	protected DIRECCION_COMPLETAR direccionCompletar = DIRECCION_COMPLETAR.DERECHA;

	public TextBoxField(Context context) {
		this(context, null);
		this.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.backwithborder));
	}

	public void setTextWatcher(Util.MyTextWatcher textWatcher) {
		if (this.textWatcher != null) {
			this.removeTextChangedListener(this.textWatcher);
		}

		this.textWatcher = textWatcher;
		this.addTextChangedListener(this.textWatcher);
	}

	public DIRECCION_COMPLETAR getDireccionCompletar() {
		return direccionCompletar;
	}

	public void setDireccionCompletar(DIRECCION_COMPLETAR direccionCompletar) {
		this.direccionCompletar = direccionCompletar;
	}

	protected String completarText(Object aValue) {
		if (aValue == null) {
			componentValue = null;
			return "";
		}
		if (aValue.toString().trim().equals("")) {
			componentValue = null;
			return "";
		}
		
		componentValue = aValue;
		String valueToString = componentValue.toString();
		if (esCompletarCaracteres) {
			StringBuilder cadena = new StringBuilder();
			for (int i = 0; i < espacios - valueToString.length(); i++) {
				cadena.append(caracter);
			}
			if (direccionCompletar == DIRECCION_COMPLETAR.IZQUIERDA) {
				valueToString = cadena.toString() + valueToString;
			} else if (direccionCompletar == DIRECCION_COMPLETAR.DERECHA) {
				valueToString = valueToString + cadena.toString();
			}
		}
		return valueToString;
	}

	@Override
	public void setValue(Object aValue) {
		this.valorAnterior = Util.getText(componentValue, null);
		if (aValue == null) {
			return;
		}
		this.setText(completarText(aValue));
	}

	@Override
	public Object getValue() {
		return this.componentValue;
	}

	public void setCompletarCaracteres(boolean opcion, int espacios) {
		esCompletarCaracteres = opcion;
		this.espacios = espacios;
	}

	public void setCaracterCompletar(String caracter) {
		this.caracter = caracter;
	}

	public TextBoxField(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		mPaint = new Paint();
		mPaint.setStyle(Paint.Style.STROKE);
		this.setBackgroundColor(getResources().getColor(R.color.cajaweb));
		mPaint.setColor(0x800000FF);
		this.setPadding(7, 0, 7, 0);
		this.setEms(10);
		this.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				maxLength) });
		this.setTextSize(textSize);
	}

	public TextBoxField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public void setMaxLength(int length) {
		if (length <= 0) {
			return;
		}
		this.maxLength = length;
		this.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				maxLength) });
	}

	public int getInputType(INPUT_TYPE inputType) {
		switch (inputType) {
		case TEXTO:
			return InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
					| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		case ENTEROS:
			return InputType.TYPE_CLASS_NUMBER;
		case DECIMALES:
			return InputType.TYPE_CLASS_NUMBER
					| InputType.TYPE_NUMBER_FLAG_DECIMAL;
		case EMAIL:
			return InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
		case TEXTO_MULTILINEA:
			return InputType.TYPE_TEXT_FLAG_MULTI_LINE
					| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;
		case TEXTO_NUMEROS: case TEXTO_NUMEROS_PUNTO :
			return InputType.TYPE_CLASS_TEXT
					| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
					| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		case TELEFONO:
			return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE;
		default:
			return InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;
		}
	}

	public void setSize(int alto, int ancho) {
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		lp.setMargins(2, 2, 2, 2);
		this.setLayoutParams(lp);
	}
	public void setSizeFloat(float alto, float ancho) {
		LayoutParams lp = new LayoutParams((int)Util.getTamañoEscaladoFloat(this.context, ancho), (int)Util.getTamañoEscaladoFloat(this.context, alto));
		lp.setMargins(2, 2, 2, 2);
		this.setLayoutParams(lp);
	}

	public void setReadOnly() {
		this.setReadOnly(true);
	}

	public void setVisible() {
		this.setVisible(false);
	}

	protected void setHintText(int id) {
		this.setHint(getResources().getString(id));
	}

	protected void setVisible(boolean flag) {
		this.setVisibility(flag ? View.VISIBLE : View.GONE);
	}

	protected void setTextAutomatico() {
		this.setTextAutomatico(true);
	}

	public void setReadOnly(boolean flag) {
		this.setCursorVisible(!flag);
		this.setFocusable(!flag);
		this.setFocusableInTouchMode(!flag);
		this.readonly = flag;
	}
	
	public boolean isReadOnly() {
		return readonly;
	}

	public void setTextAutomatico(boolean flag) {
		if (flag) {
			this.setTypeface(null, Typeface.BOLD);
			this.setTextColor(getResources().getColor(R.color.blue));
			this.setReadOnly(true);
			this.setBackgroundColor(getResources().getColor(R.color.blanco));
		} else {
			this.setTypeface(null, Typeface.NORMAL);
			this.setTextColor(getResources().getColor(R.color.black));
			// this.setText("");
			this.setReadOnly(false);
			this.setBackgroundColor(getResources().getColor(R.color.cajaweb));
		}
	}

	public void posicionar(int posicion) {
		this.setGravity(posicion);
	}

	public void callback() {
		this.setText(completarText(this.getText().toString().trim()));
	}

	public List<String> getCallbacks() {
		return this.callbacks;
	}

	public List<String> getCallbacksOnFocus() {
		return this.callbacksOnFocus;
	}
	
	public List<String> getCallbacksOnFocusGained() {
		return this.callbacksOnFocusGained;
	}

	public void setCallback(String method) {
		if (callbacks == null) {
			this.callbacks = new ArrayList<String>();
		}
		Log.e("","CALL: "+method);
		this.callbacks.add(method);
	}

	public void setCallbacks(List<String> callbacks) {
		this.callbacks = callbacks;
	}

	public void setCallbackOnFocus(String method) {
		if (callbacksOnFocus == null) {
			this.callbacksOnFocus = new ArrayList<String>();
		}
		this.callbacksOnFocus.add(method);
	}
	
	public void setCallbackOnFocusGained(String method) {
		if (callbacksOnFocusGained == null) {
			this.callbacksOnFocusGained = new ArrayList<String>();
		}
		this.callbacksOnFocusGained.add(method);
	}

	public int getMaxLength() {
		return maxLength;
	}

	// public void ingresado() {
	// vecesIngresado += 1;
	// }

	public int getVecesIngresado() {
		return vecesIngresado;
	}

	public int getVecesIngresar() {
		return vecesIngresar;
	}

	public void setReescribir(int numVeces) {
		vecesIngresar = numVeces;
	}

	public boolean esCoincidente() {
		if (vecesIngresado == 0 && vecesIngresar < 2) {
			vecesIngresado += 1;
			return true;
		}
		if (vecesIngresado == 0) {
			this.setValue("");
			vecesIngresado += 1;
			return true;
		}
		if (valorAnterior == null) {
			vecesIngresado = 0;
			return false;
		}
		vecesIngresado += 1;
		String valorActual = Util.getText(this.getValue(), "");
		Log.e(getClass().getSimpleName(), "Equals: " + valorAnterior + " = "
				+ valorActual);
		if (valorAnterior.equals(valorActual)) {
			if (vecesIngresado != vecesIngresar) {
				this.setValue("");
			}
			return true;
		} else {
			this.setValue("");
			vecesIngresado = 0;
			return false;
		}
	}

	public boolean esDigitacionFinalizada() {
		Log.e(getClass().getSimpleName(), "Equals: " + vecesIngresado + " = "
				+ vecesIngresar);
		boolean resultado = vecesIngresado == vecesIngresar;
		if (resultado) {
			vecesIngresado = 0;
		}
		return resultado;
	}
	
	public void setOutKeyOpen() {	
		this.setInputType(InputType.TYPE_NULL);		
	}

	// evento lanzado cuando se presiona cualquier tecla del teclado virtual
	// @Override
	// public InputConnection onCreateInputConnection(EditorInfo outAttrs){
	// return new KeyInputConnection(super.onCreateInputConnection(outAttrs),
	// true);
	// }
	//
	// private class KeyInputConnection extends InputConnectionWrapper {
	// public KeyInputConnection(InputConnection target, boolean mutable){
	// super(target, mutable);
	// }
	//
	// // @Override
	// // public boolean sendKeyEvent(KeyEvent event){
	// // if(event.getAction() == KeyEvent.ACTION_DOWN &&
	// // event.getKeyCode() == KeyEvent.KEYCODE_DEL){
	// // }
	// // return super.sendKeyEvent(event);
	// // }
	//
	// @Override
	// public boolean deleteSurroundingText(int beforeLength, int afterLength){
	// //backspace
	// if(beforeLength == 1 && afterLength == 0){
	// return sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
	// KeyEvent.KEYCODE_DEL))
	// && sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));
	// }
	// return super.deleteSurroundingText(beforeLength, afterLength);
	// }
	//
	// @Override
	// public boolean finishComposingText(){
	// boolean v=false;
	// try {
	// v=super.finishComposingText();
	// } catch (Exception e) {
	// Log.e("TextBoxField", "finishComposingText");
	// }
	// return v;
	// }
	//
	// @Override
	// public boolean reportFullscreenMode(boolean enabled) {
	// boolean v=false;
	// try {
	// v=super.reportFullscreenMode(enabled);
	// } catch (Exception e) {
	// Log.e("TextBoxField", "reportFullscreenMode");
	// }
	// return v;
	// }
	//
	// @Override
	// public CharSequence getTextBeforeCursor(int n, int flags) {
	// CharSequence cs = null;
	// try {
	// cs=super.getTextBeforeCursor(n, flags);
	// } catch (Exception e) {
	// Log.e("TextBoxField", "getTextBeforeCursor");
	// }
	// return cs == null ? "" : cs;
	// }
	// }
}