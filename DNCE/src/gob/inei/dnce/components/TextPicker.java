package gob.inei.dnce.components;

import java.util.ArrayList;
import java.util.List;

import gob.inei.dnce.R;
import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class TextPicker 
extends LinearLayout
{
	private LinearLayout lyComponente;
	
	private final TextField txtField;
	private final ImageButton imgBtnArriba;
	private final ImageButton imgBtnAbajo;
	
	private final List<Item> lista;
	private int pos=-1;

	public TextPicker(Context context) {
		super(context);
		
		lyComponente = (LinearLayout) View.inflate(getContext(), R.layout.text_picker , null);
		txtField = (TextField) lyComponente.findViewById(R.id.textview);
		txtField.readOnly();
		
		imgBtnArriba = (ImageButton) lyComponente.findViewById(R.id.arriba);
		imgBtnArriba.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				mover(1);
			}
		});
		
		imgBtnAbajo = (ImageButton) lyComponente.findViewById(R.id.abajo); 
		imgBtnAbajo.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				mover(-1);
			}
		});
		
		this.addView(lyComponente);
		
		lista = new ArrayList<TextPicker.Item>();
		
		/*
		LinearLayout ly = (LinearLayout) View.inflate(getContext(), R.layout.view_busqueda, null);		
		btnBuscar = (Button) ly.findViewWithTag("buscar");	
		tvResultado = (EditText) ly.findViewWithTag("resultado");
		*/
	}
	
	private void mover(int n) {
		int p = pos + n;
		if(p>=0 && p<lista.size()) {
			txtField.setValue(lista.get(p).toString());
			pos = p;
		}
	}
	
	public TextPicker agregar(String codigo, String etiqueta) {
		Item it = new Item();
		it.codigo = codigo;
		it.etiqueta = etiqueta;
		lista.add(it);
		return this;
	}

	public TextField getTxtField() {
		return txtField;
	}

	public static class Item {
		public String codigo;
		public String etiqueta;
		@Override
		public String toString() {
			return codigo+". "+etiqueta;
		}
		
		
	}

	public Item getItemSeleccionado() {
		if(pos<0)
			return null;
		return lista.get(pos);
	}
}
