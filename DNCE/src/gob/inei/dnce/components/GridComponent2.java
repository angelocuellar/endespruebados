package gob.inei.dnce.components;

import gob.inei.dnce.R;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Configuration;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

public class GridComponent2 extends TableLayout {
	private List<Objeto> lstObjeto;
	private List<List<Objeto>> llstObjeto;
	private List<TableRow> lstShow;
	private List<TableRow> lsttr;
	private List<TableLayout> lsttl;
	private int newColumns;
	private int cellspacingl;
	private int cellspacingr;
	private int cellspacingb;
	private int cellspacingt;
	private Integer val;
	private List<Integer> lstPosition;
	private SparseIntArray spaPosition;
	private List<TableRow> lstRowShow;
	private static final int CELLSPACING = 2;
	private int columnas;
	private int colorFondo;
	private boolean autoAjuste;
	private Context context;
	private TableRow tr;
	private TableLayout tly;
	private int anchoGrid;
	private HorizontalScrollView hScroll;
	public static int COLOR_FONDO = -1;

	public GridComponent2(Context context, int columnas) {
		this(context, Gravity.CENTER, columnas);
	}
	public GridComponent2(Context context, int columnas, boolean autoAjuste) {
		this(context, Gravity.CENTER, columnas, CELLSPACING, autoAjuste);
	}
	public GridComponent2(Context context, int columnas, boolean autoAjuste, boolean scroll) {
		this(context, Gravity.CENTER, columnas, CELLSPACING, autoAjuste, scroll);
	}
	public GridComponent2(Context context, int gravity, int columnas) {
		this(context, gravity, columnas, CELLSPACING);
	}
	public GridComponent2(Context context, int gravity, int columnas, int cellspacing) {
		this(context, gravity, columnas, cellspacing, true);
	}
	public GridComponent2(Context context, int gravity, int columnas, int cellspacing, boolean autoAjuste) {
		this(context, gravity, columnas, cellspacing, autoAjuste, true);
	}
	public GridComponent2(Context context, int gravity, int columnas, int cellspacing, boolean autoAjuste, boolean scroll) {
		super(context);
		this.context = context;
		this.columnas = columnas;
	    this.newColumns = -1;
	    this.val = 0;
	    this.cellspacingl = cellspacing;
	    this.cellspacingr = cellspacing;
	    this.cellspacingt = cellspacing;
	    this.cellspacingb = cellspacing;
	    this.autoAjuste = autoAjuste;
	    if (COLOR_FONDO == -1) {
	    	this.colorFondo = R.color.marco;
		} else {
			this.colorFondo = COLOR_FONDO;
		}
	    
	    this.lsttr = new ArrayList<TableRow>();
	    this.lsttl = new ArrayList<TableLayout>();
	    this.lstObjeto = new ArrayList<GridComponent2.Objeto>();
	    this.llstObjeto = new ArrayList<List<Objeto>>();
	    this.llstObjeto.add(lstObjeto);
	    this.lstShow = new ArrayList<TableRow>();
	    this.lstPosition = new ArrayList<Integer>();
	    this.spaPosition = new SparseIntArray();
	    this.lstRowShow = new ArrayList<TableRow>();
	    if(scroll) this.hScroll = new HorizontalScrollView(context);
	    TableRow.LayoutParams tlp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
	    this.setLayoutParams(tlp); 
	}
	
	public void addComponent(View view){
		this.addComponent(1, view);
	}
	public void addComponent(int colspan, View view){
		this.addComponent(colspan, 1, view);
	}
	/* metodos que se adaptan al GridComponent */
	public void addComponent(View view, int colspan){
		this.addComponent(colspan, view);
	}
	public void addComponent(View view, int colspan, int rowspan){
		this.addComponent(colspan, rowspan, view);
	}
	////////////////////////////////////////////////////
	public void addComponent(int colspan, int rowspan, View view){
		if(view==null) return;
		
		int res = 0, crw = 0;
		if(lstObjeto.size() < (this.newColumns!=-1?this.newColumns:this.columnas)){
			lstObjeto.add(new Objeto(view, colspan, rowspan, llstObjeto.size(), lstObjeto.size()+1));
		} else {
			val = 0;
			for(int y=0;y<llstObjeto.size();y++){
				for(int z=0;z<llstObjeto.get(y).size();z++){
					if(llstObjeto.get(y).get(z).rowspan + llstObjeto.get(y).get(z).row - 1 > llstObjeto.size()){
						crw = crw + 1;
					}
				}
			}
			val = crw;
			lstObjeto = new ArrayList<GridComponent2.Objeto>();
			llstObjeto.add(lstObjeto);
			lstObjeto.add(new Objeto(view, colspan, rowspan, llstObjeto.size(), lstObjeto.size()+1));
		}
		
		if(lstObjeto.size()>0){
			for(Objeto ob:lstObjeto){
				res = res + ob.colspan;
			}
		}
		res = res - lstObjeto.get(lstObjeto.size()-1).colspan;
		
		if(val + colspan + res == this.columnas)
			this.newColumns = lstObjeto.size();
		else 
			this.newColumns = this.columnas;
	}
	
	public GridComponent2 component(){
		//this.removeAllViews();
		buildTable();		
		//reset();		
		return this;
	}
	
	/*
	private void reset() {
		lstObjeto.clear();
		llstObjeto.clear();
		lstShow.clear();
		//lsttr.clear();
		//lsttl.clear();
		//lstPosition.clear();
		//lstRowShow.clear();		
		//this.newColumns = -1;
	    //this.val = 0;
	}
	*/
	
	private LinearLayout addLayoutObject(View view, int colspan, boolean marguin){
		TableRow.LayoutParams lparams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		if(marguin) lparams.setMargins(this.cellspacingl, this.cellspacingt, this.cellspacingr, this.cellspacingb);
		lparams.span = colspan;
		
		LinearLayout ly = new LinearLayout(context);
		ly.setOrientation(LinearLayout.VERTICAL);
		//ly.setBackgroundColor(this.cellspacingl == 0?getResources().getColor(R.color.WhiteSmoke):Color.WHITE);
		ly.setBackgroundColor(getResources().getColor(R.color.WhiteSmoke));
		ly.setGravity(Gravity.CENTER); //con esto centramos todos los txt automaticamente
	
		ly.setLayoutParams(lparams);
		
		int vheight, vwidth;
		ViewGroup.LayoutParams vly = view.getLayoutParams();
	
		if(vly!=null && vly.height>1) vheight=vly.height;
		else vheight=TableRow.LayoutParams.MATCH_PARENT;
		
		if(vly!=null && vly.width>1) vwidth=vly.width;
		else vwidth=TableRow.LayoutParams.WRAP_CONTENT;
		
		view.setLayoutParams(new TableRow.LayoutParams(vwidth, vheight));
		ly.addView(view);
		
		return ly;
	}
	
	private TableRow createTR(int index, int posicion, int flag){
		tr = new TableRow(context);
		tr.setBackgroundColor(getResources().getColor(this.colorFondo));
		
		if(posicion == -1 || lsttr.size()<posicion) lsttr.add(tr);
		else lsttr.add(posicion, tr);
		if(flag == 1) lstRowShow.add(tr);
		
		if(index != -1) {
			lstShow.add(tr);
		}
		return tr;
	}
	
	private TableLayout createTL(){
		tly = new TableLayout(context);
		lsttl.add(tly);
		return tly;
	}
	
	private TableRow getTR(int index){
		if(lsttr.size()!=0 && lsttr.size()>index)
			return lsttr.get(index);
		return createTR(index, -1, 1);
	}
	
	private TableLayout getTL(int index){
		if(lsttl.size()!=0 && lsttl.size()>index) 
			return lsttl.get(index);	
		return createTL();
	}
	
	@Deprecated
	public void buildTable(){
		int inc = 0;
		int yy = -1;
		int cont = -1;
		int col = 0;
		
		for(List<Objeto> lp:llstObjeto){
			cont++;
//			Log.e("lista: ", "lista: "+lp.size());
			for(Objeto obj : lp){
				if(obj.equals(lp.get(lp.size()-1))) obj.lastObj = true;
				if(obj.rowspan>1 && !obj.lastObj){
					yy++;
					if(yy >= 1){
						for(int h=0;h<spaPosition.size();h++){
							if(spaPosition.keyAt(h) == (cont+yy)-1 && !obj.equals(lp.get(lp.size()-1)))
									spaPosition.put(h, spaPosition.valueAt(h)-obj.rowspan+1);
						}
					}
					spaPosition.put(yy,obj.rowspan);
				}
			}
		}
		
		if(autoAjuste) {
			lstObjeto = new ArrayList<GridComponent2.Objeto>();
			llstObjeto.add(lstObjeto);
			for(int r=0;r<this.columnas;r++){
				View tv = new View(context);
				tv.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				lstObjeto.add(new Objeto(tv, 1, 1, llstObjeto.size(), lstObjeto.size()+1));
			}
			getWidthMax();
		}
		
//		for(int z=0;z<spaPosition.size();z++){
//			Log.e("spaPosition", "spaPosition: clave: "+spaPosition.keyAt(z)+" valor: "+spaPosition.valueAt(z));
//		}
		
		if(llstObjeto.size()>0){
			for(int x=0;x<llstObjeto.size();x++){
				int conta = -1;
				if(x==llstObjeto.size()-1 && autoAjuste) this.cellspacingb = this.cellspacingt = 0;
				yy=x+inc;
				for(Objeto obj : llstObjeto.get(x)){
					conta++;
					if(llstObjeto.get(x).get(llstObjeto.get(x).size()-1).rowspan>1) col=1;
					if(obj.lastObj && obj.rowspan>1){
						if(obj.row-1 == x)
							getTR(yy).addView(addLayoutObject(obj.view, obj.colspan, true));
						continue;
					}
					getTR(x + inc).addView(addLayoutObject(obj.view, obj.colspan, true));
					if(obj.rowspan>1 && !obj.lastObj){
						lstPosition.add(obj.rowspan);
						if(spaPosition.get(lstPosition.size()-1) == 1) continue;
						getTR(x + inc).addView(addLayoutObject(getTL(inc), this.columnas-(conta+1)-col, false));
						if(x>0) conta = lsttr.size();
						for(int z=0;z<spaPosition.get(lstPosition.size()-1);z++)
							getTL(inc).addView(createTR(-1, conta+z+1, 1));
				
						inc = inc+1;
					}
					col=0;
				}
			}
		}
//		Log.e("lsttr.size()", "lsttr.size(): "+lsttr.size());
		
		tly = createTL();
		for(TableRow tblr:lstShow)
			tly.addView(tblr);
		
		if(hScroll != null)	setScroll();
		else this.addView(tly);
	}
	
	private void setScroll(){
		int ancho = (anchoGrid <= FragmentForm.SIZE_DISPLAY.x-20? TableRow.LayoutParams.WRAP_CONTENT:FragmentForm.SIZE_DISPLAY.x-20);
		TableRow.LayoutParams tlp = new TableRow.LayoutParams(ancho, TableRow.LayoutParams.MATCH_PARENT);
		this.setLayoutParams(tlp);
		
		this.removeAllViews();
		hScroll.removeAllViews();
		if(ancho == TableRow.LayoutParams.WRAP_CONTENT){
			this.addView(tly);
		} else {
			hScroll.addView(tly);
			this.addView(hScroll);
		}
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		if(hScroll != null) setScroll();
		super.onConfigurationChanged(newConfig);
	}
	
	/**
	 * Returns an Image object that can then be painted on the screen. 
	 * The url argument must specify an absolute {@link URL}. The name
	 * argument is a specifier that is relative to the url argument. 
	 * <p>
	 * This method always returns immediately, whether or not the 
	 * image exists. When this applet attempts to draw the image on
	 * the screen, the data will be loaded. The graphics primitives 
	 * that draw the image will incrementally paint on the screen. 
	 *
	 * @param  url  an absolute URL giving the base location of the image
	 * @param  name the location of the image, relative to the url argument
	 * @return      the image at the specified URL
	 * @see         Image
	 */
	private void getWidthMax(){
		int ancho=0, alto=0, cont=0, fila = -1, sumador = 0;
		int cantidad = 1, posicion=0, result = -1, anchoColspan=0, pr = 0;
		SparseArray<Objeto> spw = new SparseArray<Objeto>();
		SparseIntArray spa = new SparseIntArray();
		SparseIntArray sum = new SparseIntArray();
		SparseArray<Objeto> spt = new SparseArray<Objeto>();
		boolean bresult = true, bcolSpan = true;
		
		while(cantidad <= this.columnas){
			List<Objeto> lst = new ArrayList<Objeto>();
			ancho=0; fila=0;bcolSpan = true;sumador = 0;
			if(llstObjeto.size()>0){
				spw.clear();
				for(int x=0;x<llstObjeto.size();x++){
					alto = 0;
					for(int y=0;y<spw.size();y++){
						Objeto obj = (Objeto)spw.valueAt(y);
						if(obj.rowspan + obj.row-1 == x+1 && !obj.lastObj){
							fila = obj.rowspan + obj.row - 1; 
							posicion = obj.column; 
						}
					}
					cont = (x <= fila - 1)?posicion:0;
					for(Objeto obj : llstObjeto.get(x)){
						cont++;
						if(obj.rowspan>1) {spw.put(spw.size(), obj);posicion = cont; fila = obj.rowspan + obj.row;}
						ViewGroup.LayoutParams lp = obj.view.getLayoutParams();
						//para el alto
						if(cantidad == this.columnas){
							if(lp.height > alto) alto = lp.height;
							if(obj.rowspan > 1 && !obj.lastObj) alto = 0;
						}
						
						if(bcolSpan){
							for(int g=0;g<spt.size();g++){
								Objeto obj2 = (Objeto)spt.valueAt(g);
								if(cont == obj2.column && x == obj2.row - 1){
									 cont = cont + obj2.colspan -1; 
									 bcolSpan = false;
								}
							}
						}
						//para el ancho
						if(cont == cantidad){
							if(obj.colspan <= 1) {
								lst.add(obj);
								if(lp.width > ancho)
									ancho = lp.width;
							} else {
								if(x != llstObjeto.size()-1){
									boolean verWidth = true;
									for(int g=0;g<spt.size();g++){
										Objeto obj3 = (Objeto)spt.valueAt(g);
										if(obj3.column == obj.column && obj3.row == obj.row){
											verWidth = false;
											break;
										}
									}
									if(verWidth){
//										Log.e("epe", "epe: "+(x+cont+sumador));
										spt.put(/*x*/ x+cont+sumador, obj);
									}
								}
							}
						}
					}
					sumador = sumador + llstObjeto.get(x).size();
	
					if(cantidad == this.columnas){
//						Log.e("alto", "alto: "+x+": valor: "+alto);
						spa.put(x, alto);
						//para acumular el alto de aquellos que tienen rowspan
						for(int r=0;r<spw.size();r++){
							Objeto obj3 = (Objeto)spw.valueAt(r);
							if(x < obj3.rowspan + obj3.row - 1 && x >= obj3.row-1)
								sum.put(r, sum.get(r) + alto);
						}
//						for(int y = 0; y<sum.size(); y++)
//							Log.e("sum", "sum clave: "+sum.keyAt(y)+" valor: "+sum.valueAt(y));	
					}
				}
			}
			
//			for(Objeto v : lst){
//				Log.e("valor", "obs: "+(v.view instanceof LabelComponent ? ((LabelComponent)v.view).getText(): v.view.getTag()));
//			}
			setHeightWidth(lst, 1, ancho);
//			Log.e("ancho", "ancho : "+ancho);
			anchoGrid = anchoGrid + ancho;
//			Log.e("----", "--------------------------------");
			
			cantidad++;
		}
//		Log.e("anchoGrid", "anchoGrid : "+anchoGrid);
//		Log.e("spt.size()", "spt.size() : "+spt.size());
//		for(int w=spt.size()-1;w>=0;w--){
//			Log.e("spt", "spt.keyAt(w) : "+spt.keyAt(w)+ " spt.valueAt(w): "+spt.valueAt(w));
//		}
		for(int z=0;z<llstObjeto.size();z++){
			if(z==llstObjeto.size()-1){
				for(int w=spt.size()-1;w>=0;w--){
					bresult = true; anchoColspan=0; pr = 0;
					for(int x=0;x<llstObjeto.size();x++){
						if(x != spt.valueAt(w).row && x != spt.valueAt(w).row - 1) continue;
						cont = 0; sumador = 0;
						for(Objeto obj : llstObjeto.get(x)){
							cont++;
							if(x == spt.valueAt(w).row - 1) {
								if(cont < spt.valueAt(w).column){
									ViewGroup.LayoutParams lpm = obj.view.getLayoutParams();
									pr = pr + lpm.width; 
									continue;
								} else
									break;
							}
							
							if(sumador == 0){
								for(int r=0;r<spw.size();r++){
									Objeto obj3 = (Objeto)spw.valueAt(r);
									if((obj3.rowspan + obj3.row >= spt.valueAt(w).row) &&
											obj3.column < spt.valueAt(w).column && obj3.row <= spt.valueAt(w).row){
										sumador++;
									}
								}
							}
								
//							Log.e("sumador", "sumador: "+sumador);
							if(cont + sumador < spt.valueAt(w).colspan + spt.valueAt(w).column && 
									cont + sumador >= spt.valueAt(w).column){
								ViewGroup.LayoutParams lp = obj.view.getLayoutParams();
								bresult = false;
								anchoColspan = anchoColspan + lp.width;
							}
						}
						if(!bresult) break;
					}
	
//					Log.e("anchoColspan: "+spt.keyAt(w), "anchoColspan: "+anchoColspan);
//					Log.e("pr", "pr: "+pr);
					if(anchoColspan == 0) 
						anchoColspan = anchoGrid - pr + (llstObjeto.get(spt.valueAt(w).row-1).size()-1)*(this.cellspacingl*4);
					if(anchoColspan > 0){
						List<Objeto> lst = new ArrayList<Objeto>();
						lst.add(spt.valueAt(w));
						setHeightWidth(lst, 1, anchoColspan + (llstObjeto.get(spt.valueAt(w).row).size()-1)*(this.cellspacingl*2));
					}
				}
			}
			
			boolean frowSpan= false;
			for(int s=0;s<spa.size();s++){
				if(spa.keyAt(s) == z){
					setHeightWidth(llstObjeto.get(z), 2, spa.get(z));
					//para establecer el alto de los rowspan + su margen correspondiente
					for(int rr=0;rr<spw.size();rr++){
						if((z == ((Objeto)spw.valueAt(rr)).row-1)){
//							Log.e("spw", "Entras solo dos veces y no mas: "+z);
							frowSpan = true; break;
						}
					}
					if(frowSpan){	
						for(int r=0;r<spw.size();r++){
							List<Objeto> lst1 = new ArrayList<Objeto>();
							for(int p=0;p<llstObjeto.get(z).size();p++){
								if(p == ((Objeto)spw.valueAt(r)).column - 1 &&
									z== ((Objeto)spw.valueAt(r)).row-1) lst1.add(llstObjeto.get(z).get(p));
							}
							setHeightWidth(lst1, 2, sum.valueAt(r) + (((Objeto)spw.valueAt(r)).rowspan - 1)*4);
						}
					
					}
					break;
				}
			}
		}
		
//		for(int y = 0; y<spw.size(); y++)
//			Log.e("spw", "spw: clave: "+spw.keyAt(y)+" valor: "+((Objeto)spw.valueAt(y)).rowspan);
//		return ancho;
	}
	
	private void setHeightWidth(List<Objeto> lst, int opcion, int value){
		for(Objeto v : lst){
			ViewGroup.LayoutParams lp = v.view.getLayoutParams();
			switch (opcion) {
				case 1:	lp.width = value; break;
				case 2:	lp.height = value; break;
				default: break;
			}
			v.view.setLayoutParams(lp);
		}
	}
	
	private class Objeto {
		private View view;
		private int colspan;
		private int rowspan;
		private int row;
		private int column;
		public boolean lastObj;
		public Objeto(View view, int colspan, int rowspan, int row, int column){
			this.view = view;
			this.colspan = colspan;
			this.rowspan = rowspan;
			this.row = row;
			this.column = column;
			this.lastObj = false;
		}
	}

	public View getFila(int posicion){
		if(lstRowShow.size()>posicion)
			return lstRowShow.get(posicion);
		return null;
	}
	
	public void hideColumn(int posicion){
		hideColumn(posicion, View.GONE);
	}
	
	//Ramon
	public void hideRow(int posicion, boolean hide){
		View view = getFila(posicion);
		if(view!=null)
			view.setVisibility( hide ? View.GONE : View.VISIBLE );
	}
	
	public void hideColumn(int posicion, int hideShow){
		if(lstRowShow.size()>0){
			for(TableRow tr : lstRowShow){
				if(posicion <= tr.getChildCount()){
					((LinearLayout)tr.getChildAt(posicion)).setVisibility(hideShow);
					continue;
				}
			}
		}
	}
	
	public GridComponent2 colorFondo(int fondo){this.colorFondo = fondo; return this;}
}
