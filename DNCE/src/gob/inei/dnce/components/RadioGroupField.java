package gob.inei.dnce.components;

import gob.inei.dnce.util.Util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * 
 * @author ajulcamoro
 *
 */
public class RadioGroupField extends RadioGroup implements Cloneable {

	private static String TAG = "RadioGroupField";
	private Context context;
	public static enum ORIENTATION{VERTICAL, HORIZONTAL};
	private ORIENTATION orientacion;
	protected int[] opciones;
	protected MyRadioButtonListener rbListener = null;
	protected List<Componente> buttons;
	private String callBack; 
	
	public RadioGroupField(Context context) {
		//super(context);
		//this.context = context;

		//RAMON
		this(context,new int[]{});
	}
	
	public RadioGroupField(Context context, int... opciones) {
		this(context, null, opciones);
	}
	
	public RadioGroupField(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}
	
	public RadioGroupField(Context context, AttributeSet attrs, int... opciones) {
		super(context, attrs);
		this.context = context;
		this.setOrientation(HORIZONTAL);
		//this.setGravity(17);
		crearOptions(opciones);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
	
	public void setOrientation(ORIENTATION sentido) {
		this.orientacion = sentido;
		switch (sentido) {
		case HORIZONTAL:
			this.setOrientation(LinearLayout.HORIZONTAL);
			break;
		case VERTICAL:
			this.setOrientation(LinearLayout.VERTICAL);
			break;
		}		
	}
	
	private void crearOptions(int[] opciones) {
		if (opciones == null) {
			return;
		}
		this.opciones = opciones;
		this.buttons = new ArrayList<Componente>();
//		rbListener = new MyRadioButtonListener();
		for (int i = 0; i < this.opciones.length; i++) {
			RadioButton rb = new RadioButton(context);
//			rb.setOnCheckedChangeListener(rbListener);
			rb.setText(getResources().getString(this.opciones[i]));
			rb.setTag(i+1);
			buttons.add(new Componente(rb));
			this.addView(rb);
		}
		setTagIndexSelected(-1);
//		this.setOnCheckedChangeListener(new RadioGroupFieldListener());
	}
	
	public void agregarEspecifique(int position, GridComponent txtOTRO) {
		if (rbListener == null) {
			rbListener = new MyRadioButtonListener();
			for (Componente c : buttons) {
				c.radioButton.setOnCheckedChangeListener(rbListener);
			}
		}
		for (View v : txtOTRO.getComponentes()) {
			Util.lockView(context, v);
		}
		RadioButton rb = new RadioButton(context);
		rb.setOnCheckedChangeListener(rbListener);
		rb.setText(getResources().getString(this.opciones[position]));
		rb.setTag(position+1);
		this.removeViewAt(position);
		this.buttons.set(position, new Componente(rb,true,txtOTRO.getComponentes()));
		LinearLayout.LayoutParams lp = new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, txtOTRO.getAltoTabla());
		lp.gravity = Gravity.CENTER_VERTICAL|Gravity.LEFT;
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		lp.rightMargin = 10;
		lp.topMargin = 2;
		lp.bottomMargin = 2;
		ll.addView(rb, lp);
		ll.addView(txtOTRO, lp);		
		this.addView(ll, position);
	}

	@Override
	public void setEnabled(boolean enabled) {
		for (int i = 0; i<getChildCount(); i++) {
		    getChildAt(i).setEnabled(enabled);      
		}
		super.setEnabled(enabled);
	}

	public Object getTagSelected(){
//		if(getCheckedRadioButtonId()==-1) return null;
		Object tag = null; 
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		for (int i = 0; i < buttons.size(); i++) {
			if (buttons.get(i).radioButton.isChecked()) {
				tag = buttons.get(i).radioButton.getTag();
				break;
			}
		}
		return tag;
	}
	
	public Object getTagSelected(String _null){
		Object tag = _null; 
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		for (int i = 0; i < buttons.size(); i++) {
			if (buttons.get(i).radioButton.isChecked()) {
				tag = buttons.get(i).radioButton.getTag();
				break;
			}
		}
		return tag;
	}
	
	public boolean isRadioButtonSelected() {
		if (buttons == null) {
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		boolean flag = false;
		for (Componente c : buttons) {
			if (c.radioButton.isChecked()) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	public void setTagSelected(Object tag) {
		if (tag == null) {
			limpiarCheck();
			return;
		}
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
				/* No cumple con la funcion equals */
				// if(rb.getTag().equals(tag)){
			}
//			int tot = getChildCount();
//			for (int i = 0; i < tot; i++) {
//				if (getChildAt(i) instanceof RadioButton) {
//					RadioButton rb = (RadioButton) getChildAt(i);
//					if (rb.getTag().toString().equals(tag.toString())) {
//						((RadioButton) getChildAt(i)).setChecked(true);
//						break;
//					}
//				} else if (getChildAt(i) instanceof LinearLayout) {
//					LinearLayout ll = (LinearLayout) getChildAt(i);
//					if (ll.getChildAt(0) instanceof RadioButton) {
//						RadioButton rb = (RadioButton) ll.getChildAt(0);
//						if (rb.getTag().toString().equals(tag.toString())) {
//							((RadioButton) ll.getChildAt(0)).setChecked(true);
//							break;
//						}
//					}
//				}
//				/* No cumple con la funcion equals */
//				// if(rb.getTag().equals(tag)){
//			}
		}
		int tagIndex = -1;
		for (int i = 0; i < buttons.size(); i++) {
			if (buttons.get(i).radioButton.getTag().toString().equals(tag.toString())) {
				tagIndex = i;
				buttons.get(i).radioButton.setChecked(true);
				if (buttons.get(i).tieneDetalle) {
					for (View v : buttons.get(i).views) {
						Util.lockView(context, false, v);
					}
				}
				executeCallBack();
				break;
			}
		}
		limpiarComponentes(tagIndex);
	}
	
	public void setTagIndexSelected(int tagIndex){
		limpiarCheck();
		if(tagIndex == -1){
			return;
		}
		if (!buttons.get(tagIndex).radioButton.isChecked()) {
			buttons.get(tagIndex).radioButton.setChecked(true);
		}		
		if (buttons.get(tagIndex).tieneDetalle) {
			Util.lockView(context, false, buttons.get(tagIndex).views.toArray(new View[buttons.get(tagIndex).views.size()]));
			buttons.get(tagIndex).views.get(0).requestFocus();
		}
		limpiarComponentes(tagIndex);
		executeCallBack();
	}
	
	public void changeOthersValues(int tagIndex){		
		if (buttons.get(tagIndex).tieneDetalle) {
			Util.lockView(context, false, buttons.get(tagIndex).views.toArray(new View[buttons.get(tagIndex).views.size()]));
			buttons.get(tagIndex).views.get(0).requestFocus();
		}
		limpiarComponentes(tagIndex);
		executeCallBack();
	}
	
	private void limpiarComponentes(int tagIndexSelected) {
		for (int i = 0; i < buttons.size(); i++) {
			if (i == tagIndexSelected) {
				continue;
			}
			if (buttons.get(i).tieneDetalle) {
				Util.cleanAndLockView(context, buttons.get(i).views.toArray(new View[buttons.get(i).views.size()]));
			}
		}
	}
		
	private void limpiarCheck() {
//		int tot = getChildCount();
//		for (int i = 0; i < tot; i++) {
//			if (getChildAt(i) instanceof RadioButton) {
//				((RadioButton) getChildAt(i)).setChecked(false);
//			} else if (getChildAt(i) instanceof LinearLayout) {
//				LinearLayout ll = (LinearLayout) getChildAt(i);
//				if (ll.getChildAt(0) instanceof RadioButton) {
//					((RadioButton) ll.getChildAt(0)).setChecked(false);
//					break;
//				}
//			}
//		}
		if (buttons == null) {
			return;
		}
		for (Componente b : buttons) {
			b.radioButton.setChecked(false);
		}
	}

	protected void setSize(int alto, int ancho) {
		LayoutParams lp = new LayoutParams(ancho, alto);
		if (this.orientacion == ORIENTATION.HORIZONTAL) {
			this.setGravity(Gravity.CENTER);
		} else if (this.orientacion == ORIENTATION.VERTICAL) {
			this.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
			lp.leftMargin = 7;
		}
		this.setLayoutParams(lp);
	}
	
	public void setCallBack(String callBackMethodName) {
		this.callBack = callBackMethodName;
	}
	
	public String getCallBack() {
		return this.callBack;
	}
	
	private void executeCallBack() {
		Method method;
        if(callBack==null){
            return;
        }
        try {
			method = getClass().getDeclaredMethod(callBack);
			method.invoke(this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setReadOnly(boolean readOnly) {
		this.setClickable(!readOnly); 
		this.setFocusable(!readOnly); 
		this.setFocusableInTouchMode(!readOnly);
		for (int i = 0; i < this.getChildCount(); i++) {
			this.getChildAt(i).setClickable(!readOnly);
			this.getChildAt(i).setFocusable(!readOnly); 
			this.getChildAt(i).setFocusableInTouchMode(!readOnly);
		}
	}
	
	public void setReadOnly() {
		this.setReadOnly(true); 
	}
	
	public RadioGroupField size(int alto, int ancho) { this.setSize(alto,ancho); return this;}	
	public RadioGroupField orientation(ORIENTATION sentido) { this.setOrientation(sentido); return this; }
    public RadioGroupField callBack(String methodCallBackName){ this.setCallBack(methodCallBackName); return this;}
    public RadioGroupField readOnly() { this.setReadOnly(); return this;}
	
	private class MyRadioButtonListener implements android.widget.CompoundButton.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				try {
					int index = Integer.parseInt(((RadioButton) buttonView)
							.getTag().toString());
					//TODO arreglar cuando tag empiece en 0
					RadioGroupField.this.setTagIndexSelected(index-1);
				} catch (NumberFormatException e) {
					RadioGroupField.this.setTagIndexSelected(-1);
				}
			}
		}
	}
	
	private class Componente {
		RadioButton radioButton;
		boolean tieneDetalle;
		List<View> views;
		
		public Componente(RadioButton radioButton, boolean tieneDetalle, List<View> views) {
			super();
			this.radioButton = radioButton;
			this.tieneDetalle = tieneDetalle;
			this.views = views;
		}
		
		public Componente(RadioButton radioButton) {
			this(radioButton, false, null);
		}
		
	}
}