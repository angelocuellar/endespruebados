package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;

public class IntegerField extends NumberField{
	
	private List<Long> excepValues;

	public IntegerField(Context context) {
		this(context, true);
	}
	public IntegerField(Context context, boolean hasTextWatcher) {
		super(context, hasTextWatcher);
		this.setInputType(getInputType(TextBoxField.INPUT_TYPE.ENTEROS));
		this.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		this.setPadding(4, 0, 7, 0);
		callbacks = new ArrayList<String>();
	}		

	public IntegerField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Class<Long> getValueClass() {
        return Long.class;
    }
	
	/*Fluent API*/
	public IntegerField maxLength(int length) {this.setMaxLength(length); return this;}
//	public IntegerField size(int alto, int ancho) { super.setSize(alto, ancho); return this;}
	public IntegerField size(float alto, float ancho) { super.setSizeFloat(alto, ancho); return this;}
	public IntegerField completarCaracteres(int espacios, String caracter) { this.setCompletarCaracteres(true, espacios); this.setCaracterCompletar(caracter); this.setDireccionCompletar(DIRECCION_COMPLETAR.DERECHA); return this;}
	public IntegerField completarCaracteres(int espacios, String caracter, DIRECCION_COMPLETAR direccion) { this.setCompletarCaracteres(true, espacios); this.setCaracterCompletar(caracter); this.setDireccionCompletar(direccion); return this;}
	public IntegerField readOnly() { this.setReadOnly(); return this;}
	public IntegerField negrita() { this.setTypeface(null, Typeface.BOLD); return this;}
	public IntegerField visible() { this.setVisible(); return this;}
	public IntegerField textAutomatico() { this.setTextAutomatico(); return this;}
	public IntegerField hint(int id) { this.setHintText(id); return this;}
	public IntegerField centrar() { this.posicionar(Gravity.CENTER); return this;}
	public IntegerField alinearIzquierda() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.LEFT); return this;}
	public IntegerField alinearDerecha() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.RIGHT); return this;}
	public IntegerField callback(String callback) { this.setCallback(callback); return this; }
	public IntegerField callbackOnFocus(String callback) { this.setCallbackOnFocus(callback); return this; }
	public IntegerField callbackOnFocusGained(String callback) { this.setCallbackOnFocusGained(callback); return this; }
	public IntegerField reescribir(int numVeces) { this.setReescribir(numVeces); return this;}
	public IntegerField rango(final Integer minimo, final Integer maximo, final LinkedList otros, final Integer omision) { this.setRango(minimo, maximo, otros, omision); return this;}
	public IntegerField outKeyOpen() {this.setOutKeyOpen(); return this;}

	@Override
	public void verificarRangos() {
//		Log.e(this.getClass().getSimpleName(), "verificarRangos");
		this.setRangoOk(false);
		if (this.getValue() == null) {
			this.setRangoOk(true);
			return;
		}
		Long val = Long.valueOf(this.getValue().toString());
		Long min = Long.valueOf(this.getMinValue().toString());
		Long max = Long.valueOf(this.getMaxValue().toString());
		setMensajeRango("");
		if (getOmision() != null) {
			Long omision = Long.valueOf(getOmision().toString());
			if (val.intValue() == omision.intValue()) {
				this.setRangoOk(true);	
				return;
			}
		}
		if (val < min || val > max) {
			if (getExcepValues() == null && getOmision()==null) {
				this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
					+ min + "\" hasta \"" + max
					+ "\" y no existe valor por omisi\u00f3n\"");
				return ;
			} else {
				if (getExcepValues() != null && getOmision()==null){
					if (excepValues.indexOf(val) == -1) {
						String exceptuados = "";
						for (Object o : getExcepValues()) {
							Integer i = Integer.valueOf(o.toString());
							exceptuados += i.toString() + ", ";
						}						
						this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
								+ min + "\" hasta \"" + max + "\" y los valores exceptuados son  \""
								+ exceptuados.substring(0, exceptuados.length() - 1) + "\"");
						return;
					}
				}
				else {
					this.setMensajeRango("El valor ingresado "+val.toString()+" esta fuera del rango. Los valores permitidos van desde \""
							+ min + "\" hasta \"" + max + "\" y el valor por omisi\u00f3n es \""
							+ getOmision().toString());
					return;
				}
				
			}			
		}
		this.setRangoOk(true);		
	}
	
	@Override
	public List<? extends Object> getExcepValues() {
		return excepValues;
	}
	
	@Override
	public void setExcepValues(List<? extends Object> excepVals) {
		this.excepValues = (List<Long>) excepVals;
	}
	
	public void setRango(final Integer minimo, final Integer maximo, final LinkedList otros, final Integer omision) {
		setMinValue(minimo);
		setMaxValue(maximo);
		setOmision(omision);
		setExcepValues(otros);
		setCallback("verificarRangos");
	}
}
