package gob.inei.dnce.components;

import gob.inei.dnce.R;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

public class ListViewComponent extends ListView {
	private TableComponent tc;
	private int posicion = -1;
	private int posicionVisible;
	private Animation animListView;
	public static boolean HAS_ANIMATION;
	public ListViewComponent(Context context) {
		super(context);
		this.setFocusable(true);
		this.setFocusableInTouchMode(true);
		this.setItemsCanFocus(true);
	}
	public ListViewComponent(Context context, TableComponent tc) {
		super(context);
		animListView = AnimationUtils.loadAnimation(getContext(), R.anim.anim1);
		this.tc = tc;
	}
	public ListViewComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public ListViewComponent(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	public int getPosicion() {
		return posicion;
	}
	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
	public int getPosicionVisible() {
		return posicionVisible;
	}
	public void setPosicionVisible(int posicionVisible) {
		this.posicionVisible = posicionVisible;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		posicion = pointToPosition((int)ev.getX(), (int)ev.getY());
		posicionVisible = getFirstVisiblePosition();
		if(HAS_ANIMATION && ev.getAction()==MotionEvent.ACTION_UP && getChildAt(posicion - getFirstVisiblePosition())!=null){
			getChildAt(posicion - getFirstVisiblePosition()).startAnimation(animListView);
		}
		return super.onTouchEvent(ev);
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		tc.setScroll(tc.hScroll);
		super.onConfigurationChanged(newConfig);
	}
}
