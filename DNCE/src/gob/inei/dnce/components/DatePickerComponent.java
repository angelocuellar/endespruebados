package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

public class DatePickerComponent extends DatePickerDialog {
	private int cDay;
	private int cMonth;
	private int cYear;
	private DateTimeField obj;
	private View objFocus;
	private int rdayfrom;
	private int rdayto;
	private int rmonthfrom;
	private int rmonthto;
	private int ryearfrom;
	private int ryearto;
	private Date rdateFrom;
	private Date rdateTo;

	public DatePickerComponent(Context context) {
		this(context,null);
	}
	public DatePickerComponent(Context context, DateTimeField view) {
		this(context, 0, view);
	}
	public DatePickerComponent(Context context, int theme, DateTimeField view) {
		this(context,theme,view,Calendar.getInstance().get(Calendar.YEAR), 
				Calendar.getInstance().get(Calendar.MONTH), 
				Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
	}
	public DatePickerComponent(Context context, int theme, DateTimeField view,
			int year, int monthOfYear, int dayOfMonth) {
		super(context, theme, null, year, monthOfYear, dayOfMonth);
		obj = view;
		cDay = dayOfMonth;
		cMonth = monthOfYear;
		cYear = year;
		rdayfrom = rdayto = rmonthfrom = rmonthto = ryearfrom = ryearto = -1;
	}
	
	@Override
	public void onClick (DialogInterface dialog, int which){
		if(which == DialogInterface.BUTTON_POSITIVE){
			if(obj != null) obj.setValue(Util.getFecha(cYear, cMonth+1, cDay));	
			if(objFocus != null) objFocus.requestFocus();
		} else {
			obj.setValue(null);
		}
		ejecutarCallbacks();
	}
	
	private void ejecutarCallbacks() {
		if (obj.getDialogParent() == null && obj.getFormParent() == null) {
			return;
		}
		if (obj.getCallbacks() == null) {
			return;
		}
		Method method;
		for (String callback : obj.getCallbacks()) {
			try {
            	if (obj.getFormParent() != null) {
            		method = obj.getFormParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
            		if (method != null) {
    					method.invoke(obj.getFormParent(), obj);
    				}
				} else {
					method = obj.getDialogParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
					if (method != null) {
						method.invoke(obj.getDialogParent(), obj);
					}
				}
        	} catch (NoSuchMethodException nsme) {
	            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
	            try {
	            	if (obj.getFormParent() != null) {
	            		method = obj.getFormParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
	            		if (method != null) {
	    					method.invoke(obj.getFormParent());
	    				} else {
	    					continue;
	    				}
					} else {
						method = obj.getDialogParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
						if (method != null) {
							method.invoke(obj.getDialogParent());
						} else {
	    					continue;
	    				}
					}
	            } catch (NoSuchMethodException nsme2) {
	            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
	            } catch (SecurityException se) {
		        	Log.e("FragmentForm", se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        }
	        } catch (SecurityException se) {
	        	Log.e("FragmentForm", se.toString(), se);
	        } catch (IllegalArgumentException iae) {
	        	Log.e("FragmentForm", iae.toString(), iae);
	        } catch (IllegalAccessException iae) {
	        	Log.e("FragmentForm", iae.toString(), iae);
	        } catch (InvocationTargetException ite) {
	        	Log.e("FragmentForm", ite.toString(), ite);
	        } catch (Exception ite) {
	        	Log.e("FragmentForm", ite.toString(), ite);
	        }
		}			
	}
	
	@Override
	public void onDateChanged (DatePicker view, int year, int month, int day) {
			
//		if(rdayfrom != -1 && rmonthfrom != -1 && ryearfrom != -1){
//			String daystr = (day<10) ? "0"+String.valueOf(day) : String.valueOf(day);
//			String monthstr = (month+1<10) ? "0"+String.valueOf(month+1) : String.valueOf(month+1);
//			String yearstr = (year<10) ? "0"+String.valueOf(year) : String.valueOf(year);
//			
//			Date fechamarcada = Util.getFecha(yearstr, monthstr, daystr);
//						
//	//		Calendar newDateChosen = Calendar.getInstance();
//	//		newDateChosen.set(year, month+1, day);
//	//		Date fechamarcada = newDateChosen.getTime();
//					
//			Calendar newDatemin = Calendar.getInstance();
//			newDatemin.set(ryearfrom, rmonthfrom-1, rdayfrom);
//			Date min = newDatemin.getTime();
//	
//			Log.d("min", min.toString());
//	        if (fechamarcada.compareTo(min)<0) {
//	        	view.init(ryearfrom, rmonthfrom-1, rdayfrom, this);
//	        	year = ryearfrom;
//	        	month = rmonthfrom-1;
//	        	day = rdayfrom;
//	        }
//	        
//			Calendar newDatemax = Calendar.getInstance();
//			newDatemax.set(ryearto, rmonthto-1, rdayto);
//			Date max = newDatemax.getTime();
//			
//	//        if (calendar.after(newDatemax)) {
//			Log.d("max", max.toString());
//	        if (fechamarcada.compareTo(max)>0) {
//	        	view.init(ryearto, rmonthto-1, rdayto, this);
//	        	year = ryearto;
//	        	month = rmonthto-1;
//	        	day = rdayto;
//	        }
//		}
		
		getDatePicker().init(getDatePicker().getYear(), getDatePicker().getMonth(), getDatePicker().getDayOfMonth(),new OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker arg0, int arg1, int arg2, int arg3) {
            	Log.e("bbbb", "chekca: "+arg3+ "."+ (arg2+1) + "."+arg1);
            }
            } );

		if(rdateFrom!=null && !validarDate(day, month+1, year)){
			day = cDay;
			month = cMonth;
			year = cYear;
		} else {
			day = validar(rdayfrom, rdayto, day);
			month = validar(rmonthfrom, rmonthto, month+1) - 1;
			year = validar(ryearfrom, ryearto, year);
			cDay = day;
			cMonth = month;
			cYear = year;
		}

		super.onDateChanged(view, year, month, day);
		updateTitle();
	}
	
	@Override
	public void show() {
		if(rdateFrom!=null && !validarDate(cDay, cMonth+1, cYear)){
			if(rdateTo!=null){
				cDay = Integer.valueOf(Util.getFechaFormateada(rdateTo, "dd"));
				cMonth = Integer.valueOf(Util.getFechaFormateada(rdateTo, "MM")) - 1;
				cYear = Integer.valueOf(Util.getFechaFormateada(rdateTo, "yyyy"));
			}
		}
		updateDate(validar(ryearfrom, ryearto, obj.getValue()!=null?obj.getAnio():cYear), 
				validar(rmonthfrom, rmonthto, obj.getValue()!=null?obj.getMes()-1:cMonth),
				validar(rdayfrom, rdayto, obj.getValue()!=null?obj.getDia():cDay));
		super.show();
	}

	@Override 
	public void dismiss(){
		obj.setUpdated(false);
		super.dismiss();
	}

	public int getcDay() {
		return cDay;
	}

	public int getcMonth() {
		return cMonth;
	}

	public int getcYear() {
		return cYear;
	}

	public void setRangoDay(int dayfrom, int dayto){
		rdayfrom = dayfrom;
		rdayto = dayto;
	}
	
	public void setRangoMonth(int monthfrom, int monthto){
		rmonthfrom = monthfrom;
		rmonthto = monthto;
	}
	
	public void setRangoYear(int yearfrom, int yearto){
		ryearfrom = yearfrom;
		ryearto = yearto;
	}
	
	public void setButtons(String ok, String cancel) {
		this.setButton(DialogInterface.BUTTON_POSITIVE, ok, this);
		this.setButton(DialogInterface.BUTTON_NEGATIVE, cancel, this);
	}
	
	private void updateTitle() {
		Date fecha = Util.getFecha(cYear, cMonth+1, cDay);
		SimpleDateFormat sdf = new SimpleDateFormat();
		switch (obj.tipoShow) {
			case DAY: sdf.applyLocalizedPattern("MMMM 'del' yyyy"); break;
			case MONTH: sdf.applyLocalizedPattern("dd 'del' yyyy"); break;
			case YEAR: sdf.applyLocalizedPattern("EEEE',' dd 'de' MMMM"); break;
			case DAY_MONTH: sdf.applyLocalizedPattern("'A\u00f1o:' yyyy"); break;
			case DAY_YEAR: sdf.applyLocalizedPattern("'Mes:' MMMM"); break;
			case MONTH_YEAR: sdf.applyLocalizedPattern("'D\u00eda:' dd"); break;
			default: sdf.applyLocalizedPattern("EEEE',' dd 'de' MMMM 'del' yyyy"); break;
		}
		this.setTitle(sdf.format(fecha));
	}
	
	private int validar(int rFrom, int rTo, int hm){
		if(rFrom != -1){
			if(hm < rFrom || hm > rTo)
				hm = hm < rFrom ? rFrom : rTo;
		}
		return hm;
	}
	
	private boolean validarDate(int day, int month, int year){
//		if(rdateFrom==null) return true;
//		Date current = Util.getFecha(year, month, day, "ddMMyyyy");
		Date current = Util.getFechaFormateada(Util.getFechaFormateada(year, month, day, "ddMMyyyy"), "ddMMyyyy");
		if(Util.compareDate(current, rdateFrom)>=0 && Util.compareDate(current, rdateTo)<=0) 
			return true;
		return false;
	}
	
	public void setRangoDate(String datefrom, String dateto){
//		String[] date1 = datefrom.split("/");
//		String[] date2 = dateto.split("/");
//		int[] dateInt1 = new int[date1.length];
//		int[] dateInt2 = new int[date2.length];
//		for (int i = 0; i < dateInt1.length; i++) {
//			dateInt1[i] = Integer.parseInt(date1[i]);
//		}
//		for (int i = 0; i < dateInt2.length; i++) {
//			dateInt2[i] = Integer.parseInt(date2[i]);
//		}
//		rdayfrom = dateInt1[0];
//		rmonthfrom = dateInt1[1];
//		ryearfrom = dateInt1[2];
////		Date fecha1 = Util.getFecha(ryearfrom, rmonthfrom+1, rdayfrom);
//		rdayto = dateInt2[0];
//		rmonthto = dateInt2[1];
//		ryearto = dateInt2[2];
////		Date fecha2 = Util.getFecha(ryearto, rmonthto+1, rdayto);
		
		rdateFrom = Util.getFechaFormateada(datefrom.replace("/", ""), "ddMMyyyy");
		rdateTo = Util.getFechaFormateada(dateto.replace("/", ""), "ddMMyyyy");;
	}
	
	public void showObject(DateTimeField.SHOW_HIDE object, boolean show){
		DatePicker datePicker = this.getDatePicker();
		int showHide = show?View.VISIBLE:View.GONE;
		switch (object) {
			case CALENDAR: datePicker.setCalendarViewShown(show); return;
			case DAY: getObjectShowHide(datePicker, "mDaySpinner").setVisibility(showHide); break;
			case MONTH: getObjectShowHide(datePicker, "mMonthSpinner").setVisibility(showHide); break;
			case YEAR: getObjectShowHide(datePicker, "mYearSpinner").setVisibility(showHide); break;
			case DAY_MONTH: getObjectShowHide(datePicker, "mDaySpinner").setVisibility(showHide); 
			   getObjectShowHide(datePicker, "mMonthSpinner").setVisibility(showHide); break;
			case DAY_YEAR: getObjectShowHide(datePicker, "mDaySpinner").setVisibility(showHide); 
			   getObjectShowHide(datePicker, "mYearSpinner").setVisibility(showHide); break;
			case MONTH_YEAR: getObjectShowHide(datePicker, "mMonthSpinner").setVisibility(showHide); 
			   getObjectShowHide(datePicker, "mYearSpinner").setVisibility(showHide); break;
			default: break;
		}
		datePicker.setCalendarViewShown(show);
	}
	
	public View getObjectShowHide(DatePicker datePicker, String campo){
		try {
	        Field f[] = datePicker.getClass().getDeclaredFields();
	        for (Field field : f) {
	            if (field.getName().equals(campo)) {
	                field.setAccessible(true);
	                Object yearPicker = new Object();
	                yearPicker = field.get(datePicker);
	                return ((View) yearPicker);
	            }
	        }
	    } 
	    catch (SecurityException e) {
	        Log.d("Error", e.getMessage());
	    } 
	    catch (IllegalArgumentException e) {
	        Log.d("Error", e.getMessage());
	    } 
	    catch (IllegalAccessException e) {
	        Log.d("Error", e.getMessage());
	    } 
	    
		return null;
	}
	
	public void setFocusOnDissmis(View focusView){
		this.objFocus = focusView;
	}
	
	public boolean isInRange(String avalue){
		if(rdateFrom==null || rdateTo==null) return true;
		Date current = Util.getFechaFormateada(avalue.replace("/", ""), "ddMMyyyy");
		if(!(Util.compareDate(current, rdateFrom)>=0 && Util.compareDate(current, rdateTo)<=0)) {
			ToastMessage.msgBox(obj.getContext(), "Fecha fuera de rango; Los valores permitidos "
					+ "van del \""+Util.getFechaFormateada(rdateFrom, "dd/MM/yyyy")+"\" al \""
					+Util.getFechaFormateada(rdateTo, "dd/MM/yyyy")+"\".",
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		}
		return true;
	}
}
