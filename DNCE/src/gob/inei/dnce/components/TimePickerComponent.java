package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

public class TimePickerComponent extends TimePickerDialog {
	private int cHours;
	private int cMinute;
	private DateTimeField obj;
	private View objFocus;
	private int rHourFrom;
	private int rHourTo;
	private int rMinuteFrom;
	private int rMinuteTo;

	public TimePickerComponent(Context context) {
		this(context,null);
	}
	public TimePickerComponent(Context context, DateTimeField view) {
		this(context, 0, view);
	}
	public TimePickerComponent(Context context, int theme, DateTimeField view) {
		this(context,theme,view, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), 
				Calendar.getInstance().get(Calendar.MINUTE), 
				android.text.format.DateFormat.is24HourFormat(context));
	}
	public TimePickerComponent(Context context, int theme, DateTimeField view,
			int hourOfDay, int minute, boolean is24HourView) {
		super(context, theme, null, hourOfDay, minute, is24HourView);
		obj = view;
		cHours = hourOfDay;
		cMinute = minute;
		rHourFrom = rHourTo = rMinuteFrom = rMinuteTo = -1;
	}
	
	@Override
	public void onTimeChanged (TimePicker view, int hourOfDay, int minute) {
		hourOfDay = validar(rHourFrom, rHourTo, hourOfDay);
		minute = validar(rMinuteFrom, rMinuteTo, minute);
		cHours = hourOfDay;
		cMinute = minute;
		updateTime(hourOfDay, minute);
		updateTitle();
	}
	
	@Override
	public void onClick (DialogInterface dialog, int which){
		if(which == DialogInterface.BUTTON_POSITIVE){
			if(obj != null) obj.setValue(Util.getHora(cHours, cMinute, 0));
			if(objFocus!=null) objFocus.requestFocus();
		} else {
			this.obj.setValue(null);
		}
		ejecutarCallbacks();
	}
	
	private void ejecutarCallbacks() {
		if (obj.getDialogParent() == null && obj.getFormParent() == null) {
			return;
		}
		if (obj.getCallbacks() == null) {
			return;
		}
		Method method;
		for (String callback : obj.getCallbacks()) {
			try {
            	if (obj.getFormParent() != null) {
            		method = obj.getFormParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
            		if (method != null) {
    					method.invoke(obj.getFormParent(), obj);
    				}
				} else {
					method = obj.getDialogParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
					if (method != null) {
						method.invoke(obj.getDialogParent(), obj);
					}
				}
        	} catch (NoSuchMethodException nsme) {
	            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
	            try {
	            	if (obj.getFormParent() != null) {
	            		method = obj.getFormParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
	            		if (method != null) {
	    					method.invoke(obj.getFormParent());
	    				} else {
	    					continue;
	    				}
					} else {
						method = obj.getDialogParent().getClass().getDeclaredMethod(callback, FieldComponent.class);
						if (method != null) {
							method.invoke(obj.getDialogParent());
						} else {
	    					continue;
	    				}
					}
	            } catch (NoSuchMethodException nsme2) {
	            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
	            } catch (SecurityException se) {
		        	Log.e("FragmentForm", se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        }
	        } catch (SecurityException se) {
	        	Log.e("FragmentForm", se.toString(), se);
	        } catch (IllegalArgumentException iae) {
	        	Log.e("FragmentForm", iae.toString(), iae);
	        } catch (IllegalAccessException iae) {
	        	Log.e("FragmentForm", iae.toString(), iae);
	        } catch (InvocationTargetException ite) {
	        	Log.e("FragmentForm", ite.toString(), ite);
	        } catch (Exception ite) {
	        	Log.e("FragmentForm", ite.toString(), ite);
	        }
		}			
	}
	
	private void updateTitle() {
		Date hora = Util.getHora(cHours, cMinute, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
		this.setTitle("Hora: ".concat(sdf.format(hora)));
	}
	
	private int validar(int rFrom, int rTo, int hm){
		if(rFrom != -1){
			if(hm < rFrom || hm > rTo)
				hm = hm < rFrom ? rFrom : rTo;
		}
		return hm;
	}
	
	@Override
	public void show() {
		updateTime(validar(rHourFrom, rHourTo, obj.getValue()!=null?obj.getHora():cHours), 
				validar(rMinuteFrom, rMinuteTo, obj.getValue()!=null?obj.getMinuto():cMinute));
		super.show();
	}
	
	@Override 
	public void dismiss(){
		obj.setUpdated(false);
		super.dismiss();
	}
	
	public int getcHour() {
		return cHours;
	}

	public int getcMinute() {
		return cMinute;
	}

	public int getcSegundo() {
		return 0;
	}
	
	public void setFocusOnDissmis(View focusView){
		this.objFocus = focusView;
	}
	
	public void setButtons(String ok, String cancel) {
		this.setButton(DialogInterface.BUTTON_POSITIVE, ok, this);
		this.setButton(DialogInterface.BUTTON_NEGATIVE, cancel, this);
	}
	
	public void setRangoHour(int hourFrom, int hourTo){
		rHourFrom = hourFrom;
		rHourTo = hourTo;
	}
	
	public void setRangoMinute(int minuteFrom, int minuteTo){
		rMinuteFrom = minuteFrom;
		rMinuteTo = minuteTo;
	}
}
