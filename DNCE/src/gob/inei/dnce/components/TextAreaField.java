package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.util.Util;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;

public class TextAreaField extends TextBoxField {
	private Rect mRect;
	private Paint mPaint;
	private int padding = 7;
	public final static int SOLO_TEXTO = Util.MyTextWatcher.SOLO_TEXTO;
	public final static int SOLO_TEXTO_NUMERO = Util.MyTextWatcher.SOLO_TEXTO_NUMERO;
	public final static int TEXTO_NUMERO = Util.MyTextWatcher.ALFANUMERICO;
	public final static int DECIMAL = Util.MyTextWatcher.DECIMAL;
	public final static int TELEFONO = Util.MyTextWatcher.TELEFONO;

	public TextAreaField(Context context, AttributeSet attrs) {
		super(context, attrs);
		mRect = new Rect();
		mPaint = new Paint();			
		mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(getResources().getColor(R.color.cajaweb));
	}
	
	public TextAreaField(Context context) {
		super(context);
		mRect = new Rect();
		mPaint = new Paint();			
		mPaint.setStyle(Paint.Style.STROKE);
        this.setInputType(getInputType(INPUT_TYPE.TEXTO_MULTILINEA));
		this.setGravity(Gravity.LEFT | Gravity.TOP);
        this.setTextAppearance(context, android.R.attr.textAppearanceMedium);
        this.setMaxLines(Integer.MAX_VALUE);
		this.setHorizontallyScrolling(false);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.bottomMargin = params.topMargin = params.leftMargin = params.rightMargin = padding;
        this.setLayoutParams(params);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		int height = getHeight();
		int lHeight = getLineHeight();
		int count = height / lHeight;
		if (getLineCount() > count) {
			count = getLineCount();
		}
		Rect r = mRect;
		Paint paint = mPaint;
		int baseline = getLineBounds(0, r);
		for (int i = 0; i < count; i++) {
			canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
			baseline += getLineHeight();
		}
		super.onDraw(canvas);
	}
	
	public void setTipoTexto(int tipo) { 
		if (textWatcher != null) {
			this.removeTextChangedListener(textWatcher);
			textWatcher = null;
		}
		textWatcher = new Util.MyTextWatcher(this, tipo);
		this.setTextWatcher(textWatcher);
		if (tipo == TEXTO_NUMERO) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TEXTO_NUMEROS));
		} else if(tipo == DECIMAL) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.DECIMALES));
		} else if(tipo == TELEFONO) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TELEFONO));
		}
	}
	
	/*Fluent API*/
	public TextAreaField maxLength(int length) {this.setMaxLength(length); return this;}
//	public TextAreaField size(int alto, int ancho) { super.setSize(alto, ancho); return this;}
	public TextAreaField size(float alto, float ancho) { super.setSizeFloat(alto, ancho); return this;}
	public TextAreaField soloTexto() { this.setTipoTexto(SOLO_TEXTO); return this;}
	public TextAreaField soloTextoNumero() { this.setTipoTexto(SOLO_TEXTO_NUMERO); return this;}
	public TextAreaField alfanumerico() { this.setTipoTexto(TEXTO_NUMERO); return this;}

	@Override
	public void setValue(Object aValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getValueClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void callback() {
		// TODO Auto-generated method stub
		
	}
}
