package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.interfaces.Respondible;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;

public class DialogComponent {
	
	private String mensaje;
	private String titulo;
	private AlertDialog.Builder builder;
	private Respondible llamante;
	private Map<String, Object> extras;
	private View view;
	public static enum TIPO_DIALOGO{NEUTRAL,YES_NO};
	public static enum TIPO_ICONO{ALERT,INFO};
	
	public DialogComponent(Context context, Respondible caller, 
			TIPO_DIALOGO type, String title, String message) {
		this(context, caller, type, title, message, TIPO_ICONO.ALERT);
	}
	
	public DialogComponent(Context context, Respondible caller, 
			TIPO_DIALOGO type, String title, String message, TIPO_ICONO icono) {
		this.llamante = caller;
		this.mensaje = message;
		this.titulo = title;
		ContextThemeWrapper ctw = new ContextThemeWrapper(context,
				R.style.MyTheme);
		builder = new AlertDialog.Builder(ctw);
		builder.setCancelable(false);
		builder.setIcon(getIcono(icono));
		builder.setTitle(this.titulo);
		builder.setMessage(this.mensaje);
		switch (type) {
		case NEUTRAL:
			setNeutralDialog();
			break;
		case YES_NO:
			setAcceptOption();
			setCancelOption();
			break;
		}
		extras = new HashMap<String, Object>();
	}	
	
	public void put(String key, Object value) {
		this.extras.put(key, value);
	}
	
	public Object get(String key) {		
		return get(key, null);
	}
		
	public Object get(String key, Object value) {
		Object v = this.extras.get(key);
		if (v == null) {
			v = value;
		}
		return v;
	}
	
	private void setNeutralDialog() {
		builder.setNeutralButton("Aceptar",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {					
					dialog.dismiss();
					llamante.onAccept();
				}
			});
	}
	
	private void setAcceptOption() {
		builder.setPositiveButton("Aceptar",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					llamante.onAccept();
				}
			});
	}
		
	private void setCancelOption() {
		builder.setNegativeButton("Cancelar",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					llamante.onCancel();
				}
			});
	}
	
	public void showDialog() {
		if (view!= null) {
			builder.setView(view);
		}
		AlertDialog dialog = builder.create();		
		dialog.show();
	}
	
	public View getView() {
		return this.view;
	}

	public void setView(View view) {
		this.view = view;
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
		builder.setMessage(this.mensaje);
	}
	
	private int getIcono(TIPO_ICONO icono) {
		switch (icono) {
		case ALERT: return android.R.drawable.ic_dialog_alert;
		case INFO: return android.R.drawable.ic_dialog_info;
		default: return android.R.drawable.ic_dialog_alert;
		}
	}
}
