package gob.inei.dnce.components;

import gob.inei.dnce.annotations.FieldCalificacion;
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.util.Memento;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class Entity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@FieldEntity(ignoreField=true)
	private static String TAG = "Entity";
	@FieldEntity(ignoreField=true, saveField=false)
	public static String PATRON = "p$[a-z0-9_]*;";
	@FieldEntity(ignoreField=true, saveField=false)
	public static String PATRON_SECCION = "p$[a-z0-9_]*;";
	@FieldEntity(ignoreField=true, saveField=false)
	public static PatronVariable PATRON_VARIABLE = new PatronVariable("p$[a-z0-9_]*;", -1, -1, 3);
	
	@FieldEntity(ignoreField=true)
	private static Comparator<Field> fieldComparator = new Comparator<Field>() {	
		public int compare(Field f1, Field f2) {
			return f1.getName().compareTo(f2.getName());
		}
	};
	
	public Integer id = null;
    public Integer usucre = null; 
    public String feccre = null; 
    public Integer usureg = null; 
    public String fecreg = null;
    public String fecenv = null;  
	
	public Entity() {}
	
	/**
	 * Llena los campos de la entidad con la informaci\xf3n contenida en el cursor
	 * 
	 * @param cursor Cursor conteniendo la informacion que se proporcionara a la entidad
	 * @param start Numero de la pregunta inicial
	 * @param end Numero de la pregunta final
	 * @return
	 */
	
	public Object fillEntity(Cursor cursor, int start, int end){
		return fillEntity(cursor, start, end, null);
	}
	
	public Object fillEntity(Cursor cursor, int start, int end, String[] other){
		if(cursor == null) return null;
		if(cursor.isBeforeFirst()) cursor.moveToNext();
		List<String> camposList = getFieldMatches(start, end);
		if (other != null) {
			for (int i = 0; i < other.length; i++) {
				camposList.add(other[i]);
			}
		}		
		return fillEntity(cursor, camposList.toArray(new String[camposList.size()]));
	}
	
	public Object fillEntity(Cursor cursor, SeccionCapitulo... secciones){
		if(cursor == null) return null;
		if(cursor.isBeforeFirst()) cursor.moveToNext();
		List<String> camposList = new ArrayList<String>();
		for (int i = 0; i < secciones.length; i++) {
			SeccionCapitulo seccion = secciones[i];
			camposList.addAll(getFieldMatches(seccion.seccion, seccion.inicio, seccion.fin));
			if (seccion.campos != null) {
				for (int j = 0; i < seccion.campos.length; i++) {
					camposList.add(seccion.campos[j]);
				}
			}
		}
		return fillEntity(cursor, camposList.toArray(new String[camposList.size()]));
	}
	
	public Entity fillEntity(Cursor cursor, String[] campos) {
		if(cursor == null) return null;
		if(cursor.isBeforeFirst()) cursor.moveToNext();		
		Entity entity = this;		
		for(int i = 0; i < campos.length; i++){
			String campo = campos[i];
			try {
				//Log.d(TAG, "campo = " + campo);
				if (cursor.getColumnIndex(campo.toUpperCase())==-1) continue;
				
				Field field = entity.getAllFields(campo.toLowerCase());
				int col = cursor.getColumnIndex(campo.toUpperCase());
				if (field.getType() == String.class) {
					entity.getAllFields(campo.toLowerCase()).set(entity,cursor.getString(col));
				}
				else if(field.getType() == Integer.class){
					entity.getAllFields(campo.toLowerCase()).set(entity, cursor.getString(col)==null?null:cursor.getInt(col));
				}
				else if(field.getType() == Float.class){
					entity.getAllFields(campo.toLowerCase()).set(entity, cursor.getString(col)==null?null:cursor.getFloat(col));
				}
				else if(field.getType() == BigDecimal.class){
					//entity.getAllFields(campo.toLowerCase()).set(entity, cursor.getString(col)==null?null:new BigDecimal(cursor.getString(col)));
					entity.getAllFields(campo.toLowerCase()).set(entity, cursor.getString(col)==null?null:new BigDecimal(cursor.getDouble(col)));
				}
				else if(field.getType() == byte[].class){					
					entity.getAllFields(campo.toLowerCase()).set(entity, cursor.getBlob(col)==null?null:cursor.getBlob(col));
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, e.getMessage());
			} catch (NullPointerException e) {
				Log.d(TAG, campo + " es nulo");
			}
			
		}
		return entity;
	}	
	
	/**
	 * Llena el content con los valores de las propiedades de la entidad
	 * @param content ContentValue a llenar
	 * @param start Numero de la pregunta inicial
	 * @param end Numero de pregunta final
	 * @return
	 */
	public ContentValues getContentValues(ContentValues content, int start, int end){
		return getContentValues(content, start, end, null);
	}
	
	public ContentValues getContentValues(ContentValues content, int start, int end, String[] other){
		//content.put("ID_N", inf_Cap100.id_n);
		Entity entity = this;
		for(String campo : getFieldMatches(start, end)){
			try {
				Object valueField;
				Field field = entity.getAllFields(campo.toLowerCase());
				if(field.getType() == byte[].class) {			
					valueField = field.get(entity);
					if (valueField != null) {
						byte[] blob = (byte[]) field.get(entity);
						content.put(campo.toUpperCase(), blob);
					} else {
						content.putNull(campo.toUpperCase());
					}
				} else {
					valueField = field.get(entity);
					content.put(campo.toUpperCase(), valueField == null?null:valueField.toString());
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, e.getMessage());
			}
		}
		
		if(other != null){
			for(String campo : other){
				try {
					Object valueField;
					
					Field field = entity.getAllFields(campo.toLowerCase());
					if(field.getType() == byte[].class) {			
						valueField = field.get(entity);
						if (valueField != null) {
							byte[] blob = (byte[]) field.get(entity);
							content.put(campo.toUpperCase(), blob);
						} else {
							content.putNull(campo.toUpperCase());
						}
					} else {
						valueField = field.get(entity);
						content.put(campo.toUpperCase(), valueField == null?null:valueField.toString());
					}
				} catch (IllegalArgumentException e) {
					Log.d(TAG, e.getMessage());
				} catch (IllegalAccessException e) {
					Log.d(TAG, e.getMessage());
				}
			}
		}
		
		return content;
	}
	
	/**
	 * Llena el content con los valores de las propiedades de la entidad
	 * @param content ContentValue a llenar
	 * @param start Numero de la pregunta inicial
	 * @param end Numero de pregunta final
	 * @return
	 */
	public ContentValues getContentValues(ContentValues content, SeccionCapitulo... secciones){
		Entity entity = this;
		for (int i = 0; i < secciones.length; i++) {
			SeccionCapitulo seccion = secciones[i];
			for(String campo : getFieldMatches(seccion.getSeccion(), seccion.getInicio(), seccion.getFin())){
				try {
					Object valueField;
					Field field = entity.getAllFields(campo.toLowerCase());
					if(field.getType() == byte[].class) {			
						valueField = field.get(entity);
						if (valueField != null) {
							byte[] blob = (byte[]) field.get(entity);
							content.put(campo.toUpperCase(), blob);
						} else {
							content.putNull(campo.toUpperCase());
						}
					} else {
						valueField = field.get(entity);
						content.put(campo.toUpperCase(), valueField == null?null:valueField.toString());
					}
				} catch (IllegalArgumentException e) {
					Log.d(TAG, e.getMessage());
				} catch (IllegalAccessException e) {
					Log.d(TAG, e.getMessage());
				}
			}
			if(seccion.getCampos() != null){
				for(String campo : seccion.getCampos()){
					try {
						Object valueField;
						Field field = entity.getAllFields(campo.toLowerCase());
						if(field.getType() == byte[].class) {			
							valueField = field.get(entity);
							if (valueField != null) {
								byte[] blob = (byte[]) field.get(entity);
								content.put(campo.toUpperCase(), blob);
							} else {
								content.putNull(campo.toUpperCase());
							}
						} else {
							valueField = field.get(entity);
							content.put(campo.toUpperCase(), valueField == null?null:valueField.toString());
						}
					} catch (IllegalArgumentException e) {
						Log.d(TAG, e.getMessage());
					} catch (IllegalAccessException e) {
						Log.d(TAG, e.getMessage());
					}
				}
			}
		}		
		return content;
	}
	 
	/**
	 * Llena el content con los valores de las propiedades de la entidad sin distinguir mayusculas y minusculas,  usarlo para cuando la tabla y el� bean no cumplan el estandar
	 * @param content ContentValue a llenar
	 * @param other nombre de los atributos del bean
	 * @return
	 */
	public ContentValues getContentValues2(ContentValues content, String[] other){
		//content.put("ID_N", inf_Cap100.id_n);
		Entity entity = this;
		
		if(other != null){
			for(String campo : other){
				try {
					Object valueField = entity.getAllFields(campo).get(entity);
					content.put(campo, valueField == null?null:valueField.toString());
				} catch (IllegalArgumentException e) {
					Log.d(TAG, e.getMessage());
				} catch (IllegalAccessException e) {
					Log.d(TAG, e.getMessage());
				}
			}
		}
		
		return content;
	}
	
	/**
	 * Obtiene una lista con los nombres de la preguntas desde <code>start<code> hasta <code>end<code>
	 * @param start Numero de la pregunta inicial 
	 * @param end Numero de la pregunta final
	 * @return
	 */
//	public List<String> getFieldMatches(int start, int end){
//		List<String> allMatches = new ArrayList<String>();
//		String campos = getAllFieldsString();
//		for(int i=start; i<=end; i++){
////			Matcher m = Pattern.compile("[a-z]+"+i+"[a-z0-9_]*;").matcher(getAllFieldsString());
//			String tmp = PATRON.replace("$", Util.completarCadena(String.valueOf(i), "0", 3, COMPLETAR.IZQUIERDA));
//			Matcher m = Pattern.compile(tmp).matcher(campos);
//			while (m.find()) {
//				allMatches.add(m.group().replace(";", ""));
//			}
//		}
//		
//		return allMatches;
//	}
	public List<String> getFieldMatches(int start, int end){				
		return getFieldMatches(-1, start, end);
	}
	
	/**
	 * Obtiene una lista con los nombres de la preguntas desde <code>start<code> hasta <code>end<code>
	 * @param start Numero de la pregunta inicial 
	 * @param end Numero de la pregunta final
	 * @return
	 */
	public List<String> getFieldMatches(int seccion, int start, int end){
		return getFieldMatches(seccion, start, end, -1, -1);
	}
	public List<String> getFieldMatches(int seccion, int start, int end, int subPregI, int subPregF){
//		List<String> allMatches = new ArrayList<String>();
//		String allFieldsString = getAllFieldsString();
//		for(int i=start; i<=end; i++){
////			Matcher m = Pattern.compile("[a-z]+"+i+"[a-z0-9_]*;").matcher(getAllFieldsString());
//			String tmp = PATRON_SECCION.replace("$", Util.completarCadena(String.valueOf(i), "0", 3, COMPLETAR.IZQUIERDA));
//			tmp = tmp.replace("%", Util.completarCadena(String.valueOf(seccion), "0", 2, COMPLETAR.IZQUIERDA));
//			for(int j=subPregI; j<=subPregF; j++){
//				String tmps = tmp.replace("#", j==-1?"":"_"+String.valueOf(j));
//				Matcher m = Pattern.compile(tmps).matcher(allFieldsString);
//				while (m.find()) {
//					allMatches.add(m.group().replace(";", ""));
//				}
//			}
//		}		
//		return allMatches;
		return getFieldMatches(seccion, -1, start, end, -1, -1);
	}
	
//	public List<String> getFieldMatches(int seccion, int subSeccion, int start, int end, int subPregI, int subPregF){
//		List<String> allMatches = new ArrayList<String>();
//		String allFieldsString = getAllFieldsString();
//		for(int i=start; i<=end; i++){
////			Matcher m = Pattern.compile("[a-z]+"+i+"[a-z0-9_]*;").matcher(getAllFieldsString());
//			String tmp = PATRON_SECCION.replace("$", Util.completarCadena(String.valueOf(i), "0", 3, COMPLETAR.IZQUIERDA));
////			tmp = tmp.replace("&", subSeccion==-1?"":String.valueOf(subSeccion));
//			tmp = tmp.replace("&", subSeccion==-1?"":Util.completarCadena(String.valueOf(subSeccion), "0", 3, COMPLETAR.IZQUIERDA));
//			tmp = tmp.replace("%", Util.completarCadena(String.valueOf(seccion), "0", 2, COMPLETAR.IZQUIERDA));
//			for(int j=subPregI; j<=subPregF; j++){
//				String tmps = tmp.replace("#", j==-1?"":"_"+String.valueOf(j));
//				Matcher m = Pattern.compile(tmps).matcher(allFieldsString);
//				while (m.find()) {
//					allMatches.add(m.group().replace(";", ""));
//				}
//			}
//		}		
//		return allMatches;
//	}
	
	public List<String> getFieldMatches(int seccion, int subSeccion, int start, int end, int subPregI, int subPregF){
		List<String> allMatches = new ArrayList<String>();
		String allFieldsString = getAllFieldsString();
//		Log.e(TAG, allFieldsString);
		for(int i=start; i<=end; i++){
			String tmp = PATRON_VARIABLE.patron.replace("$", Util.completarCadena(String.valueOf(i), "0", 
					PATRON_VARIABLE.longitudPregunta, COMPLETAR.IZQUIERDA));
			tmp = tmp.replace("&", subSeccion==-1?"":Util.completarCadena(String.valueOf(subSeccion), "0", 
					PATRON_VARIABLE.longitudSubSeccion, COMPLETAR.IZQUIERDA));
			tmp = tmp.replace("%", seccion==-1?"":Util.completarCadena(String.valueOf(seccion), "0", 
					PATRON_VARIABLE.longitudSeccion, COMPLETAR.IZQUIERDA));
			if (subPregI == -1) {
				tmp = tmp.replace("#", "");
//				Log.e(TAG, "Patron [" + tmp + "]");
				Matcher m = Pattern.compile(tmp).matcher(allFieldsString);
				while (m.find()) {
					allMatches.add(m.group().replace(";", ""));
				}
			} else {
				for(int j=subPregI; j<=subPregF; j++){
					String tmps = tmp.replace("#", j==-1?"":"_"+String.valueOf(j));
					Matcher m = Pattern.compile(tmps).matcher(allFieldsString);
					while (m.find()) {
						allMatches.add(m.group().replace(";", ""));
					}
				}
			}
		}		
//		Log.e(TAG, ""+Arrays.toString(allMatches.toArray(new String[allMatches.size()])));
		return allMatches;
	}
	
	/**
	 * Construye una cadena conteniendo los nombres de los campos, de la entidad, separados por punto y coma(";")
	 * @return
	 */
	protected String getAllFieldsString(){
		Field[] campos = getAllFields();
		String nameFields="";
		for(Field campo : campos){
			FieldEntity fe = campo.getAnnotation(FieldEntity.class);
			if (fe != null) {
				if (fe.ignoreField()) {
					continue;
				}
			}
			nameFields += campo.getName() + ";";
		}
		return nameFields.toUpperCase();
	}
	
	/**
	 * Devuelve los campos de la entidad, separados por punto y coma(";")
	 * @return
	 */
	protected Field[] getAllFields(){
		Field[] campos = this.getClass().getDeclaredFields();
		Field[] camposPadre = getFatherFields();
		Field[] totalCampos = new Field[campos.length+camposPadre.length];
		for (int i = 0; i < camposPadre.length; i++) {
			totalCampos[i] = camposPadre[i];
		}
		for (int i = 0; i < campos.length; i++) {
			totalCampos[camposPadre.length+i] = campos[i];
		}
		return totalCampos;
	}
		
	protected Field getAllFields(Field buscado){
		Field[] campos = getAllFields();
		
		Arrays.sort(campos, fieldComparator);
		int indice = Arrays.binarySearch(campos, buscado, fieldComparator);
		if (indice < 0) {
			return null;
		} else {
			return campos[indice];
		}
	}
	
	protected Field getAllFields(String buscado){
		Field[] campos = getAllFields();		
		Arrays.sort(campos, fieldComparator);
		for (int i = 0; i < campos.length; i++) {
			if (campos[i].getName().equals(buscado)) {
				return campos[i];
			}
		}
		return null;
	}
		
	/**
	 * Obtiene una lista con los campos de la entidad desde <code>start<code> hasta <code>end<code>
	 * @param start Numero de la pregunta inicial 
	 * @param end Numero de la pregunta final
	 * @return
	 */
	public List<Field> getFields(int start, int end) {
		List<Field> allMatches = new ArrayList<Field>();
		Field[] fields = getAllFields();
		Pattern pat;
		Matcher mat;
		for (int i = 0; i < fields.length; i++) {
			pat = Pattern.compile("p" + i + "[a-z0-9_]*;");
			mat = pat.matcher(fields[i].getName());
			if (mat.matches()) {
				allMatches.add(fields[i]);
			}
			pat = null;
			mat = null;
		}
		return allMatches;
	}
	
	/**
	 * Obtiene una lista con los campos de la entidad desde <code>start<code> hasta <code>end<code>
	 * @param start Numero de la pregunta inicial 
	 * @param end Numero de la pregunta final
	 * @return
	 */
	public List<Field> getFields(String... campos) {
		List<Field> allMatches = new ArrayList<Field>();
		Field[] fields = getAllFields();
		for (int i = 0; i < campos.length; i++) {
			for (int j = 0; i < fields.length; j++) {
				if (fields[j].getName().equals(campos[i])) {
					allMatches.add(fields[j]);					
					break;
				}				
			}
		}
		
		return allMatches;
	}
	
	private Field[] getFatherFields() {
		Field[] fields = this.getClass().getSuperclass().getDeclaredFields();
		return fields;
	}
	
	/**
	 * Construye una array conteniendo los nombres de los campos de la entidad
	 * @return
	 */
	public String[] getFieldsNames(){
		Field[] fields = getAllFields();
		List<String> campos = new ArrayList<String>();
		for (int i = 0; i < fields.length; i++) {
			if ("serialVersionUID".equals(fields[i].getName())) {
				continue;
			}
			FieldEntity annotation = fields[i].getAnnotation(FieldEntity.class);  
			if (annotation != null) {
				if (annotation.ignoreField()) {
					continue;
				} 	
			}
			campos.add(fields[i].getName());
		}
		return campos.toArray(new String[campos.size()]);
	}
	
	/**
	 * Construye una array conteniendo los nombres de los campos de la entidad
	 * @return
	 */
	public Field[] getFieldsEntities(){
		Field[] fields = getAllFields();
		List<Field> campos = new ArrayList<Field>();
		for (int i = 0; i < fields.length; i++) {
			if ("serialVersionUID".equals(fields[i].getName())) {
				continue;
			}
			FieldEntity annotation = fields[i].getAnnotation(FieldEntity.class);  
			if (annotation != null) {
				if (annotation.ignoreField()) {
					continue;
				} 	
			}
			campos.add(fields[i]);
		}
		return campos.toArray(new Field[campos.size()]);
	}
	
	/**
	 * Construye una array conteniendo los nombres de los campos de la entidad
	 * @return
	 */
	public String[] getFieldsSaveNames(){
		Field[] fields = getAllFields();
		List<String> campos = new ArrayList<String>();
		for (int i = 0; i < fields.length; i++) {
			if ("serialVersionUID".equals(fields[i].getName())) {
				continue;
			}
			FieldEntity annotation = fields[i].getAnnotation(FieldEntity.class);  
			if (annotation != null) {
				if (annotation.ignoreField()) {
					continue;
				} 
				if (!annotation.saveField()) {
					continue;
				} 	
			}
			campos.add(fields[i].getName());
		}
		return campos.toArray(new String[campos.size()]);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public boolean equalsToPractice(Object obj) {
		return this.equals(obj);
	}
	
	public static class PatronVariable {
		private String patron;
		private int longitudSeccion;
		private int longitudSubSeccion;
		private int longitudPregunta;
		
		/**
	       * Constructor de clase.
	       * @param  patron cadena que representa el patron a seguir para encontrar 
	       * el nombre de la variable
	       * @param  longitudSeccion la longitud de la seccion para completarla con 
	       * ceros a la izquierda, si -1 se obviara dicha seccion
	       * @param  longitudSubSeccion la longitud de la subseccion para completarla con 
	       * ceros a la izquierda, si -1 se obviara dicha subseccion
	       * @param  longitudPregunta la longitud de la pregunta para completarla con 
	       * ceros a la izquierda
	       */
		public PatronVariable(String patron, int longitudSeccion,
				int longitudSubSeccion, int longitudPregunta) {
			super();
			this.patron = patron;
			this.longitudSeccion = longitudSeccion;
			this.longitudSubSeccion = longitudSubSeccion;
			this.longitudPregunta = longitudPregunta;
		}

		public String getPatron() {
			return patron;
		}

		public int getLongitudSeccion() {
			return longitudSeccion;
		}

		public int getLongitudSubSeccion() {
			return longitudSubSeccion;
		}

		public int getLongitudPregunta() {
			return longitudPregunta;
		}
		
	}
				
	public static class SeccionCapitulo {
		private int seccion;
		private int subseccion;
		private int inicio;
		private int fin;
		private int subPregI;
		private int subPregF;
		private String[] campos;
		public SeccionCapitulo() {
			
		}	
		public SeccionCapitulo(int seccion, int inicio, int fin, String... campos) {
			this(seccion, -1, inicio, fin, campos);
		}
		public SeccionCapitulo(int seccion, int subseccion, int inicio, int fin, String... campos) {
			this(seccion, subseccion, inicio, fin, -1, -1, campos);
		}
		public SeccionCapitulo(int seccion, int inicio, int fin, int subPregI, int subPregF, String... campos) {
			this(seccion, -1, inicio, fin, subPregI, subPregF, campos);
		}
		public SeccionCapitulo(int seccion, int subseccion, int inicio, int fin, int subPregI, int subPregF, String... campos) {
			super();
			this.seccion = seccion;
			this.subseccion = subseccion;
			this.inicio = inicio;
			this.fin = fin;
			this.subPregI = subPregI;
			this.subPregF = subPregF;
			this.campos = campos;
		}
		public int getSeccion() {
			return seccion;
		}
		public int getSubseccion() {
			return subseccion;
		}
		public int getInicio() {
			return inicio;
		}
		public int getFin() {
			return fin;
		}
		public int getSubPregI() {
			return subPregI;
		}
		public int getSubPregF() {
			return subPregF;
		}
		public String[] getCampos() {
			return campos;
		}
		@Override
		public String toString() {
			return "SeccionCapitulo [seccion=" + seccion + ", subseccion="
					+ subseccion + ", inicio=" + inicio + ", fin=" + fin
					+ ", subPregI=" + subPregI + ", subPregF=" + subPregF + "]";
		}
		
	}	
	
//	public Memento<Entity> saveToMemento() {
//		return null;
//	}
//	
//	public void restoreFromMemento(Memento<? extends Entity> memento) {
//		
//	}
	
	public Memento<Entity> saveToMemento(Class<? extends Entity> clase) {
		Field[] campos = getFieldsEntities();
		Entity e = null;
		try {
			e = clase.newInstance();		
			for (int i = 0; i < campos.length; i++) {
				campos[i].set(e, campos[i].get(this));
			}
		} catch (IllegalArgumentException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InstantiationException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		}
		return new Memento<Entity>(e);
	}
	
	public Memento<Entity> saveToMemento(Class<? extends Entity> clase, Integer valor) {
		Field[] campos = getFieldsEntities();
		Entity e = null;
		try {
			e = clase.getDeclaredConstructor(Integer.class).newInstance(valor);
			for (int i = 0; i < campos.length; i++) {
				campos[i].set(e, campos[i].get(this));
			}
		} catch (IllegalArgumentException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InstantiationException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (NoSuchMethodException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InvocationTargetException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		}
		return new Memento<Entity>(e);
	}
	
	public Memento<Entity> saveToMemento(Class<? extends Entity> clase, String valor) {
		Field[] campos = getFieldsEntities();
		Entity e = null;
		try {
			Constructor[] cos =clase.getConstructors();
			Constructor<? extends Entity> constructor = null;
			for (int i = 0; i < cos.length; i++) {
				if (cos[i].getParameterTypes() != null && cos[i].getParameterTypes().length > 0) {
					if (cos[i].getParameterTypes()[0] == String.class) {
						constructor = cos[i];
						break;
					}
				}
			}
			e = constructor.newInstance(valor);
			for (int i = 0; i < campos.length; i++) {
				campos[i].set(e, campos[i].get(this));
			}
		} catch (IllegalArgumentException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InstantiationException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InvocationTargetException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		}
		return new Memento<Entity>(e);
	}
	
	public Memento<Entity> saveToMemento(Class<? extends Entity> clase, String valor1, String valor2) {
		Field[] campos = getFieldsEntities();
		Entity e = null;
		try {
			Constructor[] cos =clase.getConstructors();
			Constructor<? extends Entity> constructor = null;
			for (int i = 0; i < cos.length; i++) {
				if (cos[i].getParameterTypes() != null && cos[i].getParameterTypes().length > 1) {
					if (cos[i].getParameterTypes()[0] == String.class 
							&& cos[i].getParameterTypes()[1] == String.class) {
						constructor = cos[i];
						break;
					}
				}
			}
			e = constructor.newInstance(valor1, valor2);
			for (int i = 0; i < campos.length; i++) {
				campos[i].set(e, campos[i].get(this));
			}
		} catch (IllegalArgumentException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InstantiationException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (InvocationTargetException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		}
		return new Memento<Entity>(e);
	}
	
	public void restoreFromMemento(Memento<? extends Entity> memento) {
		Field[] campos = getFieldsEntities();
		try {
			for (int i = 0; i < campos.length; i++) {
				campos[i].set(this, campos[i].get(memento.getMemento()));
			}
		} catch (IllegalArgumentException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			Log.e(getClass().getSimpleName(), e1.getMessage(), e1);
		}
	}
	
	/**
	 * Construye una array conteniendo los nombres de los campos de la a usar para calificar las practicas
	 * @return
	 */
	public Field[] getPracticeFields(){
		Field[] fields = this.getClass().getDeclaredFields();
		List<Field> campos = new ArrayList<Field>();
		for (int i = 0; i < fields.length; i++) {
			if ("serialVersionUID".equals(fields[i].getName())) {
				continue;
			}
			FieldCalificacion annotation = fields[i].getAnnotation(FieldCalificacion.class);  
			if (annotation != null) {
				if (annotation.ignoreField()) {
					continue;
				} 	
			}
			campos.add(fields[i]);
		}
		return campos.toArray(new Field[campos.size()]);
	}
	
	public String getTitle() {
		return "";
	}
	
	public String getPks() {
		return "";
	}
	
	public SeccionCapitulo[] getSecCap(List<String> fields){
		return getSecCap(fields, true);
	}
	public SeccionCapitulo[] getSecCap(List<String> fields, boolean check){
		return getSecCap(fields, check, false);
	}
	public SeccionCapitulo[] getSecCap(List<String> fields, boolean check, boolean valNull){
		FieldsEntity fe = getListFieldsEntity(this, fields, check, valNull);
		return new SeccionCapitulo[] { new SeccionCapitulo(0, -1,
				-1, fe.lstfields.toArray(new String[fe.lstfields.size()])) };
	}
	
	public class FieldsEntity {
		public String fields;
		public List<String> lstfields;
	}
	
	public FieldsEntity getListFieldsEntity(Entity entity, List<String> fields, boolean ui){
		return getListFieldsEntity(entity, fields, ui, false);
	}
	public FieldsEntity getListFieldsEntity(Entity entity, List<String> fields, boolean ui, boolean valNull){
		List<String> data = new ArrayList<String>();
		String strFields = entity.getFields(fields, null, ui, valNull);
	
		for(String p:strFields.split(",")){
			Log.e("ppppp", "ppppp: "+p);
			data.add(p.trim());
		} 
		
		FieldsEntity fe = new FieldsEntity(); 
		fe.fields = strFields.trim();
		fe.lstfields = data;
    	
		return fe;
	}
	
	public String getFields(List<String> fields, String alias, boolean ui) {
		return getFields(fields, alias, ui, false);
	}
	public String getFields(List<String> fields, String alias, boolean ui, boolean valNull) {
		String nameFields="";
		if(alias == null) alias = "";
		else if(alias != null) alias = alias+".";
		for(String field:fields){
			if(ui){
				//field = field.substring(sbstr);
				field = field.replaceFirst("[a-z]*", "");
				try {
					if(this.getClass().getField(field.toLowerCase())!=null);
				} catch (NoSuchFieldException e) {
					continue;
				}
			} else {
				if(field.equals("serialVersionUID")) continue;
			}
			if(valNull){
				try {
					Field f = this.getClass().getField(field.toLowerCase());
					f.set(this, null);
				} catch (Exception e) {
					continue;
				} 
			}
			nameFields += alias+field.toUpperCase() + ", ";
		}
//		nameFields += alias+"ID_N" + ", ";
//		nameFields += alias+"USUCRE" + ", ";
//		nameFields += alias+"FECCRE" + ", ";
		String pks = getPks().equals("")?"":", "+getPks();
		return nameFields.substring(0, nameFields.length()-2)+pks+" ";
	}
	
	//Ramon
	@SuppressLint("DefaultLocale")
	public void setValor(String campo, String valor) throws IllegalArgumentException, IllegalAccessException {
		if(campo!=null) {
			Entity entity = this;
			Field field = entity.getAllFields(campo.toLowerCase());
			if(field!=null)
				field.set(entity,valor);
		}
	}
	
	public String pksToString() {
		return id.toString();
	}
}