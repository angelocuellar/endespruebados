package gob.inei.dnce.components;

import gob.inei.dnce.R;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GridComponent extends GridLayout {

	private static final int CELLSPACING = 2;
	private List<CellComponent> componentes;
	private int filas;
	private int columnas;
	private int gravity;
	private Context context;
	private int[] aFila;
	private int[] aColumna;
	private int cellspacing = 2;
	private int colorFondo;
	
	public GridComponent(Context context, int columnas) {
		this(context, Gravity.CENTER, columnas, CELLSPACING);
	}
	
	public GridComponent(Context context, int gravity, int columnas) {
		this(context, gravity, columnas, CELLSPACING);
	}
	
	public GridComponent(Context context, int gravity, int columnas, int cellspacing) {
		super(context);
		this.context = context;
		this.gravity = gravity;
		componentes = new ArrayList<GridComponent.CellComponent>();
		this.columnas = columnas;
		this.colorFondo = R.color.marco;
		setColorFondo(this.colorFondo);
	    this.setColumnCount(this.columnas);
	    LayoutParams lp = new LayoutParams();
	    lp.setGravity(Gravity.CENTER);
	    this.setLayoutParams(lp);
	    this.cellspacing = cellspacing;
	}
	
	public void setColorFondo(int colorFondo) {
		this.setBackgroundColor(getResources().getColor(colorFondo));
	}
	
	public void addComponent(View view) {
		this.addComponent(view, 1);
	}

	public void addComponent(View view, int colunmSpan) {
		this.addComponent(view, colunmSpan, 1);
	}
	
	public void addComponent(View view, int columnSpan, int rowSpan) {
		if(view == null) return;
		this.componentes.add(new CellComponent(view, columnSpan, rowSpan));
	}		
	
	@Deprecated
	public void buildTable() {
		filas = 0;
		for (CellComponent c : componentes) {
			filas += c.columnSpan;
		}
		aFila = new int[filas];
		aColumna = new int[columnas];
		for (int i = 0; i < filas; i++) {
			aFila[i] = 0;
		}
		for (int i = 0; i < columnas; i++) {
			aColumna[i] = 0;
		}
		int cont = 0;
		int f = 0;
		for (CellComponent c : componentes) {
			int h =0;
			if (c.component instanceof GridComponent){
				h=((GridComponent)c.component).getAltoTabla();
			} else {
				h=c.component.getLayoutParams().height;
			}
			verificarAlto(f,h);
			if (c.columnSpan > 1) {
				
			} else {
				int w = 0;
				if (c.component instanceof GridComponent) {
					w = ((GridComponent) c.component).getAnchaTabla();
				} else {
					w = c.component.getLayoutParams().width;
				}
				verificarAncho(cont,w);
			}			
			cont = cont + c.columnSpan;
			if (cont%columnas==0) {
				f += 1;
				cont = 0;
			}
		}		
		int anchoTabla = getAnchaTabla();
		cont = 0;
		f = 0;
		for (CellComponent c : componentes) {
			Spec row1 = GridLayout.spec(f);
			Spec colspan2 = GridLayout.spec(cont, c.columnSpan);
			GridLayout.LayoutParams first = new GridLayout.LayoutParams(row1, colspan2);
			first.rowSpec = GridLayout.spec(f);
			first.height = aFila[f];
			if (c.columnSpan > 1) {
				for (int i = cont; i < cont+c.columnSpan; i++) {
					first.width += aColumna[i];
				} 
				first.width += (this.cellspacing==0?1:this.cellspacing)*c.columnSpan;
			} else {
				first.width = aColumna[cont];
			}
			first.rightMargin = this.cellspacing;
			first.topMargin = this.cellspacing;
			first.leftMargin = this.cellspacing;
			first.bottomMargin = this.cellspacing;
			first.setGravity(Gravity.CENTER);
			if (c.component instanceof EditText || c.component instanceof DecimalField
					|| c.component instanceof TextField || c.component instanceof IntegerField
					|| c.component instanceof ListView 
					|| (c.component instanceof LinearLayout && !(c.component instanceof RadioGroupField))
					|| (c.component instanceof LinearLayout && !(c.component instanceof RadioGroupOtherField))) {
				this.addView(c.component, first);
			} else {
				if (c.component instanceof LabelComponent || c.component instanceof GridComponent) {
					android.widget.LinearLayout.LayoutParams lp = new android.widget.LinearLayout.LayoutParams(first.width,first.height);
					lp.gravity = Gravity.CENTER;
					c.component.setLayoutParams(lp);
				}
				LinearLayout ll = createSection(LinearLayout.VERTICAL);
				ll.setGravity(Gravity.CENTER);
				ll.addView(c.component);
				this.addView(ll, first);
			}
			cont = cont + c.columnSpan;
			if (cont%columnas==0) {
				f += 1;
				cont = 0;
			}
		}
		android.widget.GridLayout.LayoutParams param = new android.widget.GridLayout.LayoutParams();
		param.height = android.widget.GridLayout.LayoutParams.WRAP_CONTENT;
		param.width = anchoTabla;
		param.setGravity(this.gravity);
		param.leftMargin = 20;
		param.rightMargin = 20;
		this.setLayoutParams(param);
	}
	
	private int getAnchaTabla() {
		int ancho=0;
		for(int i=0;i<this.columnas;i++){
			ancho+=aColumna[i];
		}
		ancho += ((this.cellspacing)*this.columnas*2);	
		return ancho;
	}
	
	public int getAltoTabla() {
		int alto=0;
		for(int i=0;i<this.columnas;i++){
			alto+=aFila[i];
		}
		alto+= (this.cellspacing)*this.filas*2;
		return alto;
	}

	private void verificarAlto(int f, int height) {
		if (aFila[f] < height) {
			aFila[f] = height;
		}
	}
	
	private void verificarAncho(int f, int width) {
		if (aColumna[f] < width) {
			aColumna[f] = width;
		}
	}

	private class CellComponent {
		View component;
		int columnSpan;
		int rowSpan;
		
		public CellComponent(View component, int columnSpan, int rowSpan) {
			this.component = component;
			this.columnSpan = columnSpan;
			this.rowSpan = rowSpan;
		}
	}
	
	protected LinearLayout createSection(int orientacion) {
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout ll = new LinearLayout(this.context);
		ll.setBackgroundColor(getResources().getColor(R.color.WhiteSmoke));
		ll.setOrientation(orientacion);
		lp.gravity = Gravity.CENTER;
		ll.setLayoutParams(lp);
		ll.setGravity(Gravity.CENTER);
		return ll;
	}
	
	public List<View> getComponentes() {
		List<View> views = new ArrayList<View>();
		for (CellComponent c : componentes) {
			views.add(c.component);
		}
		return views;
	}
		
	public GridComponent component(){
		buildTable();
		return this;
	}
	
	public GridComponent colorFondo(int fondo){setColorFondo(this.colorFondo = fondo); return this;}
}
