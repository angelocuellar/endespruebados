package gob.inei.dnce.components.ui;

//import gob.inei.divies2014.model.CPV0301_DET_01;
//import gob.inei.cunamas2015.fragment.c10200.C1CAP200Fragment_001;
//import gob.inei.cunamas2015.fragment.c10200.dialog.C1CAP200Fragment_001_001;
//import gob.inei.cunamas2015.model.C1CAP0200;
import gob.inei.dnce.R;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.ImageViewField;
import gob.inei.dnce.components.LabelComponent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class ImagenDialog  extends DialogFragmentComponent {
	
	private ImageViewField miImageView;
	private int estiloBoton; 
	private GaleriaDialog caller;
	public ButtonComponent btnCancelar;
	Bitmap  imagen;
	LinearLayout q0, q1,q2;	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		return rootView;
	}	
	
	public static ImagenDialog newInstance(GaleriaDialog pagina, Bitmap bitmap, int estiloBoton) {
		ImagenDialog f = new ImagenDialog();
		f.caller = pagina;		
		f.estiloBoton = estiloBoton;
		f.setParent(pagina.getParent());
		f.imagen=bitmap;
		return f;
	}

//	public ImagenDialog estilo(int estilo) {
//		this.estilo = estilo;
//		return this;
//	}

	public ImagenDialog estiloBoton(int estilo) {
		this.estiloBoton = estilo;
		return this;
	}
	
	@Override
	protected View createUI() {
		buildFields();	
		q2 = createQuestionSection(miImageView);
		LinearLayout botones = createButtonSection( btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q2);
		form.addView(botones);
		return contenedor;
	}
	

	@Override
	protected void buildFields()  {
		miImageView = new ImageViewField(getParent().getActivity()).zoom();
		miImageView.setImageBitmap(imagen);
		if (estiloBoton != 0) {
			btnCancelar = new ButtonComponent(getParent().getActivity(),
					estiloBoton).text(R.string.btnCerrar).size(200, 60);
		} else {
			btnCancelar = new ButtonComponent(getParent().getActivity())
					.text(R.string.btnCerrar).size(200, 60);
		}
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
}

