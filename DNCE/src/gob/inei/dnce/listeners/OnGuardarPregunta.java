package gob.inei.dnce.listeners;

import gob.inei.dnce.interfaces.Item;
import gob.inei.dnce.interfaces.Pregunta;

import java.util.List;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface OnGuardarPregunta {
	List<? extends Item> onAntesGuardar() throws Exception;//retorna los items antes de guardar
	void onDespuesGuardar() throws Exception;
	
	void onGuardar(Pregunta pregunta, boolean limpiar) throws Exception;
	//String onValidar(String valor, Pregunta pregunta);
	
	void onEliminarOtroDinamico(Pregunta pregunta) throws Exception;
}
