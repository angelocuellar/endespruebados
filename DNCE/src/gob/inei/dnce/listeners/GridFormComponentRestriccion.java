package gob.inei.dnce.listeners;

import gob.inei.dnce.components.GridFormComponent.Valor;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface GridFormComponentRestriccion
{
	String validar(String codigo, Valor valor) throws Exception;
}
