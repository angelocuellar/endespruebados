package gob.inei.dnce.listeners;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface GridFormComponentListener {
	void onCambioValor(String codigo, boolean si) throws Exception;
	void onTodosNo() throws Exception;
}
