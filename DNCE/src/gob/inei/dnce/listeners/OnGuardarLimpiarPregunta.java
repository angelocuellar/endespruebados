package gob.inei.dnce.listeners;

import gob.inei.dnce.interfaces.Pregunta;

/**
 * 
 * @author Rdelacruz
 *
 */
public abstract class OnGuardarLimpiarPregunta 
implements OnGuardarPregunta
{
	public abstract void onGuardar(Pregunta pregunta) throws Exception;
	public abstract void onLimpiar(Pregunta pregunta) throws Exception;
	
	@Override
	public final void onGuardar(Pregunta pregunta, boolean limpiar) throws Exception {
		
		if(pregunta.isRelevante()) {
			onGuardar(pregunta);			
		}
		
		if(limpiar)
			onLimpiar(pregunta);
	}
	
	@Override
	public void onEliminarOtroDinamico(Pregunta pregunta) throws Exception {
		
	}
}
