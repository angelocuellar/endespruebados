package gob.inei.dnce.listeners;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface OnSetValue {
	String getValue(String codigo) throws Exception;
	String getEspecifique(String codigo) throws Exception;
}
