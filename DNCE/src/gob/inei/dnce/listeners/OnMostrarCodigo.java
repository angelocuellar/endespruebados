package gob.inei.dnce.listeners;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface OnMostrarCodigo {
	String getCodigo(String codigo);
}
