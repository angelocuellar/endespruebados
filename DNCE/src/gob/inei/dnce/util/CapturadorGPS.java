package gob.inei.dnce.util;

import gob.inei.dnce.components.ToastMessage;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

public class CapturadorGPS extends Service implements LocationListener {

	private final Context mContext;
	private final String TAG = CapturadorGPS.this.getClass().getSimpleName(); 
	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	// flag for GPS status
	boolean canGetLocation = false;

	Location location; // location
	double latitude; // latitude
	double longitude; // longitude
	double altitude; // longitude

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000l;//;0;// 0 minutes // 1000 * 60 * 1; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;
	private int nroSatelites = 0;
	
	private static CapturadorGPS INSTANCE;

	public CapturadorGPS(Context context) {
		this.mContext = context;
		location = null;
		getLocation();
	}
	
	public CapturadorGPS satelites(int nroSatelites) {
		this.nroSatelites = nroSatelites;
		return this;
	}
	
//	public static CapturadorGPS getInstance(Context context) {
//		if (INSTANCE == null) {
//			INSTANCE = new CapturadorGPS(context);
//		}
//		return INSTANCE;
//	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) mContext
					.getSystemService(LOCATION_SERVICE);
			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				this.canGetLocation = false;
				location = null;
			} else {
				this.canGetLocation = true;
				// First get location from Network Provider
				if (isNetworkEnabled) {
					locationManager.removeUpdates(this);
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//					Log.e(getClass().getSimpleName(), "Network GPS");
					if (locationManager != null) {
//						location = locationManager
//								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						} else {
							this.canGetLocation = false;
						}
					}
				} 
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) { 
					if (location == null) {
						locationManager.removeUpdates(this);
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//						Log.e(getClass().getSimpleName(), "GPS Enabled");
						if (locationManager != null) {
//							location = locationManager
//									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								int satelitesHabilitados = location.getExtras().getInt("satellites", 0);								
								Log.e(TAG, "Satelites ubicados: " + satelitesHabilitados);
								if (satelitesHabilitados < this.nroSatelites) {
									latitude = 0;
									longitude = 0;
									ToastMessage.msgBox(this.mContext, "El n�mero de sat�lites es "+ satelitesHabilitados +" y se solicit� " + this.nroSatelites + ".", 
											ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
								} else {
									latitude = location.getLatitude();
									longitude = location.getLongitude();
								}
							} else {
								this.canGetLocation = false;							
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 * */
	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(CapturadorGPS.this);
		}
	}

	/**
	 * Function to get latitude
	 * */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		} else {
			latitude = 0;
		}

		// return latitude
		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		} else {
			longitude = 0;
		}

		// return longitude
		return longitude;
	}

	/**
	 * Function to get altitude
	 * */
	public double getAltitude() {
		if (location != null) {
			altitude = location.getAltitude();
		} else {
			altitude = 0;
		}
		// return altitude
		return altitude;
	}

	/**
	 * Function to get Bundle
	 * */
	public Bundle getBundle() {
		if (location != null) {
//			for (String key : location.getExtras().keySet()) {
//				Log.e(TAG, "Bundle: "+ key + "["+Util.getText(location.getExtras().get(key), "")+"]");
//			}
			return location.getExtras();
		} else {
			return null;
		}
	}
	
	/**
	 * Function to get altitude
	 * */
	public double getBearing() {
		if (location != null) {
			return location.getBearing();
		} else {
			return 0;
		}
	}
	
	public double getAccuracy() {
		if (location != null) {
			return location.getAccuracy();
		} else {
			return 0;
		}
	}

	/**
	 * Function to check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}
	
	public void turnGPSOn() {
		Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		intent.putExtra("enabled", true);
		this.mContext.getApplicationContext().sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.mContext.getApplicationContext().getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) {
			// if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.mContext.getApplicationContext().sendBroadcast(poke);
		}		
	}
	
	public void turnGPSOff() {
		String provider = Settings.Secure.getString(this.mContext.getApplicationContext().getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) { // if gps is enabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.mContext.getApplicationContext().sendBroadcast(poke);
		}
	}

	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * lauch Settings Options
	 * */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setCancelable(false);
		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			this.location = location;
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		this.location = null;
	}

	@Override
	public void onProviderEnabled(String provider) {
		this.location = null;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
}
