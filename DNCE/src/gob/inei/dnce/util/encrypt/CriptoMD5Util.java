package gob.inei.dnce.util.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Rdelacruz
 */
public class CriptoMD5Util {
    
    //http://stackoverflow.com/questions/11105718/tripledes-algorithm-from-net-to-java
   public static final byte[] hash(String texto, boolean textoUTF8) 
           throws NoSuchAlgorithmException, UnsupportedEncodingException { 
        final MessageDigest md = MessageDigest.getInstance("MD5");
        
        final byte[] messageDigest;
        
        if(textoUTF8)
            messageDigest = md.digest(texto.getBytes("UTF-8"));
        else
            messageDigest = md.digest(texto.getBytes());
        
        //String md5 = EncodingUtils.getString(messageDigest, "UTF-8");        
        //Log.i("Function MD5", md5);
        //Log.i("Function MD5 Length","Length: "+ md5.length());
        return messageDigest;
    }
}
