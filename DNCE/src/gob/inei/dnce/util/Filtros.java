package gob.inei.dnce.util;

import gob.inei.dnce.components.ToastMessage;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Color;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class Filtros implements InputFilter {
	public static enum TIPO {ALFA, ALFA_U, ALFA_L, ALFAN, ALFAN_U, ALFAN_L, NUMBER, DECIMAL, EMAIL, WEB, ONLYOTHERS};
	public static int ERRORCOLOR = Color.RED;
	private EditText txtFiltro;
	private TIPO tipo;
	private int length;
	private int decimal;
	private long from;
	private long to;
	private int omision;
	private int startMsg;
	private int fromMsg;
	private int fLExact;
	private int tLExact;
	private int colorText;
	private String text="";
	private char[] others;
	private String chrDec="";
	public static HashMap<View, String> hmFiltro;
	private String mensaje;
	private boolean omisionText;
	private static final String NTNS = "NO TIENE/NO SABE";
	
	static {
		hmFiltro = new HashMap<View, String>();
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, char[] others){
		setFiltro(editText, tipo, -1, others);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, char[] others, long from, long to){
		setFiltro(editText, tipo, -1, -1, others, from, to);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, char[] others){
		setFiltro(editText, tipo, length, -1, others, -1, -1);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, int decimales, char[] others, long from, long to){
		setFiltro(editText, tipo, length, decimales, others, from, to, -1);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, int decimales, char[] others, long from, long to, int omision){
		setFiltro(editText, tipo, length, decimales, others, from, to, omision, -1);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, int decimales, char[] others, long from, long to, int omision, int startMsgDig){
		setFiltro(editText, tipo, length, decimales, others, from, to, omision, startMsgDig, -1);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, int decimales, char[] others, long from, long to, int omision, int startMsgDig, int fromMsgNum){
		setFiltro(editText, tipo, length, decimales, others, from, to, omision, startMsgDig, fromMsgNum, -1, -1);
	}
	
	public static void setFiltro(EditText editText, TIPO tipo, int length, int decimales, char[] others, long from, long to, int omision, int startMsgDig, int fromMsgNum, int fLExact, int tLExact){
		Filtros filter = new Filtros();
		filter.txtFiltro = editText;
		filter.colorText = editText.getCurrentTextColor();
		filter.tipo = tipo;
		filter.length = length;
		filter.decimal = decimales;
		filter.from = from;
		filter.to = to;
		filter.omision = omision;
		filter.startMsg = startMsgDig;
		filter.fromMsg = fromMsgNum;
		filter.fLExact = fLExact;
		filter.tLExact = tLExact;
		filter.others = others;
		filter.mensaje = filter.setMessage(from, to, omision);
		filter.setType();
		editText.setFilters(new InputFilter[]{filter});
	}

	private void setType() {
		if(this.others == null) return;
		String oChars = String.copyValueOf(others);
		switch (tipo) {
		case NUMBER: txtFiltro.setKeyListener(DigitsKeyListener.getInstance("0123456789"+oChars));
			break;
		default:
			break;
		}
	}

	public String setMessage(long from, long to, int omision){
		if(from == -1 && to == -1) return "";
		return "Valor Ingresado esta fuera del rango. Los valores permitidos van desde \""
				+ from +"\" hasta \""+to+"\" "+ (omision == -1 ? "y no existe valor por omision.":
					"y existe valor por omision \" "+omision+"\".");
	}
	
	private void removeView(){
		if(hmFiltro.size()>0){
			if(hmFiltro.containsKey(txtFiltro)){
				hmFiltro.remove(txtFiltro);
				txtFiltro.setTextColor(this.colorText);
			}
		}
	}
	
	@Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//		Log.e("cheha", "source: "+source+" - start: "+start+" - end: "+end);
//		Log.e("cheha", "dest: "+dest+" - dstart: "+dstart+" - dend: "+dend+" - dest.length(): "+dest.length()+"- length:"+length);
		
		
		if(length!=-1 && end>start && dest.length()>=length) return "";
		text = getTextFitro(source, start, end, dest, dstart, dend);
		
//		if(tipo == TIPO.EMAIL) return checkFilterEmail();
//		if(tipo == TIPO.NUMBER) {
//			if(!text.equals("")) return checkFilterNumber(source, start, from==-1?"":String.valueOf(from), 
//					to==-1?"":String.valueOf(to), end-start>0);
//			removeView();
//		}
		
		switch (tipo) {
			case ALFAN_U: case ALFAN_L: case ALFAN: if(!text.equals("")) return checkFilterAlfanumerico(source, start, from==-1?"":String.valueOf(from), 
				to==-1?"":String.valueOf(to),tipo.ordinal(), end-start>0);
						removeView(); break;
			case EMAIL: return checkFilterEmail(source, start);
			case WEB: return checkFilterWeb();
			case NUMBER: if(!text.equals("")) return checkFilterNumber(source, start, from==-1?"":String.valueOf(from), 
					to==-1?"":String.valueOf(to), end-start>0);
						removeView(); break;
			case DECIMAL: if(!text.equals("")) return checkFilterDecimal(source, start, length==-1?"":String.valueOf(length), 
					decimal==-1?"":String.valueOf(decimal), from==-1?"":String.valueOf(from), 
							to==-1?"":String.valueOf(to));
						removeView(); break;
			case ONLYOTHERS: if(!text.equals("")) return checkFilterOnlyOthers(source, start, from==-1?"":String.valueOf(from), 
					to==-1?"":String.valueOf(to), end-start>0);
						removeView(); break;
			default: break;
		}
				
        for (int i = start; i < end; i++) {
        	switch (tipo) {
        		case ALFA_U: case ALFA_L: case ALFA: return checkFilterAlfa(source, i, tipo.ordinal());
//				case ALFAN_U: case ALFAN_L: case ALFAN: return checkFilterAlfanumerico(source, i, from==-1?"":String.valueOf(from), 
//						to==-1?"":String.valueOf(to),tipo.ordinal());
//				case NUMBER : return checkFilterNumber(source, i, from==-1?"":String.valueOf(from), 
//						to==-1?"":String.valueOf(to));
//				case DECIMAL: return checkFilterDecimal(source, i, length==-1?"":String.valueOf(length), 
//						decimal==-1?"":String.valueOf(decimal), from==-1?"":String.valueOf(from), 
//								to==-1?"":String.valueOf(to));
//				case ONLYOTHERS: return checkFilterOnlyOthers(source, start);
				
				default: break;
			}
        }
        return null;
    }

	private String getTextFitro(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		text = "";
		if(end > start) {
			if(dest.length()>0)
				text = dest.toString().substring(0, dend)+source+dest.toString().substring(dend, dest.length());
			else
				text = source.toString();
		} else if(dend > dstart && dest.length()>0) {
			text = dest.toString().substring(0, dstart)+dest.toString().substring(dend, dest.length());
		}
//		Log.e("text", "text: "+text);
		return text;
	}
	
	private CharSequence checkFilterAlfa(CharSequence source, int c, int opcion) { 
		return checkFilterAlfanumerico(source, c, "", "", opcion, false);
	}
	private CharSequence checkFilterAlfanumerico(CharSequence source, int c, String strFrom, String strTo, int opcion, boolean flag) { 
		if (flag) {
			char chr = source.charAt(c);
			boolean bolAlfa = opcion > 2 ? Character.isLetterOrDigit(chr) : Character.isLetter(chr);
			boolean check = (bolAlfa || Character.isSpaceChar(chr) || !(checkFilterOnlyOthers(source, c)!=null));
			
			switch (opcion) {
				case 0: case 3: if(!check) return "";break;
				case 1: case 4:	if (!(check	&& !Character.isLowerCase(chr))) return "";break;
				case 2: case 5: if (!(check	&& !Character.isUpperCase(chr))) return "";break;
				default: return null;
			}
		}
		double from = strFrom.equals("") ? -1 : Integer.parseInt(strFrom);
		double to = strTo.equals("") ? -1 : Integer.parseInt(strTo);
		validateRange(from, to);
		return null;
	}
	
	private CharSequence checkFilterNumber(CharSequence source, int c, String strFrom, String strTo, boolean flag) {
//		Log.e("vvvvv", "vvvvvv: "+omisionText);
		if(omisionText && !Util.esVacio(txtFiltro)) { txtFiltro.setText(""); return null; }
		if(omisionText = text.equals(NTNS)) {  removeView(); return null; }
		if (flag && !(Character.isDigit(source.charAt(c)) || !(checkFilterOnlyOthers(source, c)!=null)))
            return "";
//		int textColor = colorText;
//		if(!text.matches("(^[1-9]{1}$|^[1-4]{1}[0-9]{1}$|^50$)"))
		long from = strFrom.equals("") ? -1 : Long.parseLong(strFrom);
		long to = strTo.equals("") ? -1 : Long.parseLong(strTo);
		
		validateRange(from, to);
		
//		if(from != -1 && to != -1) {
//			try{
//				int number = text == null ? -1 : Integer.parseInt(text);
//				if(!((number >= from && number <= to) || number == this.omision)){
//					textColor = Color.RED;
//		//			txtFiltro.setError("Valor fuera de rango valor permitido de "+from+" a "+to, null);
//		//			String mensaje = "Valor Ingresado esta fuera del rango. Los valores permitidos van desde \""
//		//					+ from+"\" hasta \""+to+"\" y no existe valor por omision.";
//					if(startMsg == -1 || text.length() == startMsg){
//						ToastMessage.msgBox(txtFiltro.getContext(), mensaje,
//								ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//					}
//					hmFiltro.put(txtFiltro, mensaje);
//		//            return "";
//				} else {
//		//			txtFiltro.setError(null);
//	//				if(hmFiltro.size()>0){
//	//					if(hmFiltro.containsKey(txtFiltro)){
//	//						hmFiltro.remove(txtFiltro);
//	//					}
//	//				}
//					removeView();
//				}
//				txtFiltro.setTextColor(textColor);
//			} catch(Exception e){
//				removeView();
//				txtFiltro.setTextColor(textColor);
//			}
//		}
		return null;
	}
	
	private CharSequence checkFilterDecimal(CharSequence source, int c, String strLength, String strDecimal, String strFrom, String strTo) {
		if(text.indexOf(".")==length-1) return "";
		chrDec = chrDec + (checkFilterOnlyOthers(source, c)==null && !chrDec.contains(source)?"\\"+source:"");
		if(!text.matches("^[0-9"+chrDec+"]{1," + strLength + "}+((\\.[0-9"+chrDec+"]{0," + strDecimal + "})?)"))
            return "";
		double from = strFrom.equals("") ? -1 : Integer.parseInt(strFrom);
		double to = strTo.equals("") ? -1 : Integer.parseInt(strTo);
		validateRange(from, to);
        return null;
	}
	
	private CharSequence checkFilterEmail(CharSequence source, int c) {
		if(text.equals(NTNS)) { removeView(); return null; }
		if(!text.matches("^[_A-Za-z0-9-\\+\\.]{1,}(@{1})?([_A-Za-z0-9-\\+\\.]{1,})?"))
            return "";
		int textColor = colorText;
		if(!text.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
			textColor = ERRORCOLOR; //Color.RED;
		} 
		txtFiltro.setTextColor(textColor);
        return null;
	}
	private CharSequence checkFilterWeb() {
		if(!text.matches("^[_A-Za-z0-9-\\+\\.]{1,}?([_A-Za-z0-9-\\+\\.]{1,})?"))
            return "";
		int textColor = colorText;
		if(!text.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)"+"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
			textColor = ERRORCOLOR; //Color.RED;
		} 
		txtFiltro.setTextColor(textColor);
        return null;
	}
	
	private CharSequence checkFilterOnlyOthers(CharSequence source, int c) {
		return checkFilterOnlyOthers(source, c, "", "", true);
	}
	
	private CharSequence checkFilterOnlyOthers(CharSequence source, int c, String strFrom, String strTo, boolean flag) {
		if(flag && (others==null || !new String(others).contains(String.valueOf(source.charAt(c)))))
			return "";
//		int textColor = colorText;
//		int from = strFrom.equals("") ? -1 : Integer.parseInt(strFrom);
//		int to = strTo.equals("") ? -1 : Integer.parseInt(strTo);
		long from = strFrom.equals("") ? -1 : Long.parseLong(strFrom);
		long to = strTo.equals("") ? -1 : Long.parseLong(strTo);
		
		validateRange(from, to);
		return null;
	}
	
	public static Map.Entry<View, String> getErrorFiltro(){
		Log.e("hmFiltro", "hmFiltro.size(): "+hmFiltro.size());
		if(hmFiltro.size()>0){
			for(Map.Entry<View, String> t : hmFiltro.entrySet()){
				return t;
			}
		} 
		return null;
	}
	
	public static void clear(){
		hmFiltro.clear();
	}
	
	private void validateRange(double from, double to){
		int textColor = colorText;
		if(from != -1 && to != -1) {
			try{
				long number = text == null ? -1 : Long.parseLong(text);
				if(!((number >= from && number <= to) || number == this.omision)){
					textColor = ERRORCOLOR; //Color.RED;
		//			txtFiltro.setError("Valor fuera de rango valor permitido de "+from+" a "+to, null);
		//			String mensaje = "Valor Ingresado esta fuera del rango. Los valores permitidos van desde \""
		//					+ from+"\" hasta \""+to+"\" y no existe valor por omision.";
//					Log.e("antessssss", "text.length(): "+text.length()+" startMsg: "+startMsg);
//					Log.e("antessssss", "number: "+number+" fromMsg: "+fromMsg);
					if(startMsg == -1 || (text.length() == startMsg && number >= fromMsg)){
						ToastMessage.msgBox(txtFiltro.getContext(), mensaje,
								ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					}
					hmFiltro.put(txtFiltro, mensaje);
		//            return "";
				} else {
					if(this.fLExact!=-1 && this.tLExact!=-1 && 
							!(text.length() >= this.fLExact && text.length() <= this.tLExact)){
						textColor = ERRORCOLOR;//Color.RED;
						String msg = this.fLExact!=this.tLExact?("El campo debe contener exactamente de "
								+this.fLExact+ " a "+this.tLExact+" digitos."):("El campo debe contener "
										+ "exactamente "+this.fLExact+" digitos.");
						hmFiltro.put(txtFiltro, msg);
//						Log.e("antessssss", "hmFiltro.size(): "+hmFiltro.size());
					} else
						removeView();
				}
				txtFiltro.setTextColor(textColor);
			} catch(Exception e){
				removeView();
				txtFiltro.setTextColor(textColor);
			}
		}
	}
	
	public void setLength(int length) {
		this.length = length;
	}

	public void setTipo(TIPO tipo) {
		this.tipo = tipo;
	}
}
