package gob.inei.dnce.util;

import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.interfaces.Pregunta;

import android.view.View;
import android.widget.RadioButton;

/**
 * 
 * @author Rdelacruz
 *
 */
public class RespuestaRadioGroup 
extends RespuestaImpl
//implements RadioGroup.OnCheckedChangeListener
{
	private final Integer MISSING = 9;

	private final RadioGroupOtherField radioGroup;
	private Integer valorRegistrado;
	private boolean siRequerido = false;
	
	private Listener listener;
	
	private final TIPO tipo;
	private final boolean tipoSI_NO;
	
	public static enum TIPO {
		NORMAL, OTRO_FIJO, OTRO_DINAMICO;
	}
	
	
	
	public interface Listener {
		void onCambioValor(boolean si) throws Exception;
	}
	
	public RespuestaRadioGroup(RadioGroupOtherField radioGroup, TIPO tipo) {
		this.radioGroup = radioGroup;
		this.tipo = tipo;
		this.tipoSI_NO = radioGroup.getNumeroOpciones() == 2 ;
	}
	
	public Integer getValorIntegerReal() {
		return (Integer) radioGroup.getValue();
	}
	
	public Integer getValorInteger() {
		Integer v = getValorIntegerReal();
		Pregunta p = getPregunta();
		if(p.isRelevante()) {
			if(v==null && p.isOmitible())
				v = MISSING;
		}
		else {
			v = null;
		}
		return v;
	}
	
	@Override
	public boolean isMissing() {
		return MISSING.equals(getValorInteger());
	}
	
	@Override
	public String getValor() {
		Integer v = getValorInteger();
		return v==null ? null: v+"";
	}
	
	@Override
	public void setValor(String valor) throws Exception {
		Integer v = valor==null ? null : Integer.parseInt(valor);
		radioGroup.setValue(v);
		
		onCambioValor(valor);
	}
	
	public void setValor(Integer valor) throws Exception {		
		radioGroup.setValue(valor);
		
		String v = valor==null ? null : String.valueOf(valor);
		onCambioValor(v);
	}
	
	@Override
	public void onCambioValor(String valor) throws Exception {
		
		TextField tf = getPregunta().getTxtEspecifiqueEtiqueta();
		if(tf!=null) {
			if(valor!=null && valor.equals("2")) {//NO
				//if(tipo==TIPO.OTRO_FIJO) {
				tf.setValue("");
				Util.lockView(tf.getContext(), true, tf);
				//}				
			}
			else {//SI, VACIO
				Util.lockView(tf.getContext(), false, tf);
			}
		}
		
		if(listener!=null)
			listener.onCambioValor(valor!=null && valor.equals("1"));
	}
	
	
	
	public RadioGroupOtherField getRadioGroup() {
		return radioGroup;
	}

	public Integer getValorAnterior() {
		return valorRegistrado;
	}

	public void setValorAnterior(Integer valorAnterior) {
		this.valorRegistrado = valorAnterior;
	}

	@Override
	public View getView() {
		return radioGroup;
	}

	public Integer getValorRegistrado() {
		return valorRegistrado;
	}

	public void registrarValor() {
		valorRegistrado = getValorIntegerReal();
	}

	@Override
	public String validar(String valor) {
		Integer v = valor==null? null : Integer.parseInt(valor);
		ItemImpl p = getPregunta();
		String item = p.toString();
		
		boolean especifiqueEstaVacio = false;
		if(p.isEspecifique()) {
			String especifique = p.getEspecifique(); 
			if(especifique.trim().isEmpty()) {
				especifiqueEstaVacio = true;
			}
		}
		
		if(p.isOtro()) {
			if(especifiqueEstaVacio && v==null)
				return "";//NO CAMBIAR EL "". GridComponent.guardar() depende de ello 
		}
		
		if(p.isRequerido()) {
			
			if(p.isOtro()) {
				siRequerido = true;
			}
						
			if(v==null) {
				return "Falta indicar '"+item+"'";
			}
			else {				
				if(p.isOtro()) {
					if(especifiqueEstaVacio)
						return "Debe especificar '"+item+"'";
				}
				
				//if(p.isEspecifique() && v==2) {
				//	p.getTxtEspecifiqueEtiqueta().setText("");
				//}
			}
		}
				
		if(siRequerido && (v==null || v!=1)) {
//			if(!p.isEliminarOtroOnNo())
				return "Falta indicar SI en '"+item+"'";
		}	
		
		TextField tf = p.getTxtEspecifiqueSi();
		if(tf!=null) {
			//String val = (String) tf.getValue();
			String val = tf.getText().toString();
			if(val==null || val.trim().isEmpty()) {
				return "Falta especificar el texto del item '"+item+"'";
//				mostrarMensaje(mensaje);
//				tf.requestFocus();
//				guardar = false;
			}
		}
		
		return null;
	}

	public boolean isSiRequerido() {
		return siRequerido;
	}

	public void setSiRequerido(boolean siRequerido) {
		this.siRequerido = siRequerido;
	}

	@Override
	public boolean debeLimpiar() {		
		/*
		//Para acelerar el guardado ignoramos la limpieza de items que ya fueron limpiados antes
		if(mapRespuestasAnteriores!=null) {
			String valor = getValor();
			Item respuestaAnterior = mapRespuestasAnteriores.get(getPregunta().getCodigo());
			String valorAnterior = respuestaAnterior!=null ? respuestaAnterior.getValor() : null;
			if(valor!=null && valor.equals("2") && valorAnterior!=null && valorAnterior.equals("2"))
				return false;
			if(valor!=null && valor.equals("9") && valorAnterior!=null && valorAnterior.equals("9"))
				return false;
		}
		*/
		
		Integer v = getValorInteger();
		return v!=null && (v==2 || v==MISSING || v==3);//2. No / 3. No corresponde 
	}

	
	
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public void desactivarNo() {
		for (int i = 0; i < radioGroup.getChildCount(); i++) {
			View v = radioGroup.getChildAt(i);
			if(v instanceof RadioButton) {
				RadioButton rb = (RadioButton)v;
				if(rb.getTag().equals(1)) {
					rb.setText("");
				}
				else if(rb.getTag().equals(2)) {
					rb.setVisibility(View.GONE);
				}
			}
		}
	}

	public TIPO getTipo() {
		return tipo;
	}

	public boolean isTipoSI_NO() {
		return tipoSI_NO;
	}

	
}
