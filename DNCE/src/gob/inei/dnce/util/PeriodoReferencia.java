package gob.inei.dnce.util;

import gob.inei.dnce.util.Util.COMPLETAR;

import java.util.Calendar;
import java.util.Date;

public class PeriodoReferencia {
	private Date fecha;
	private String dosAnios;
	private String doceMeses;
	private String doceMesesProximos;
	private String unMes;
	private String quinceDias;
	private String dosSemanas;
	private String tresMeses;
	private String seisMeses;
	private String tresAnios;
	private String cuatroSemanas;
	private String sieteDias;
	private String semanaAnterior;
	private String anioPasado;

	private final int DIAS_ENERO = 31;
	private final int DIAS_FEBRERO = 28;
	private final int DIAS_FEBRERO_BISIESTO = 29;
	private final int DIAS_MARZO = 31;
	private final int DIAS_ABRIL = 30;
	private final int DIAS_MAYO = 31;
	private final int DIAS_JUNIO = 30;
	private final int DIAS_JULIO = 31;
	private final int DIAS_AGOSTO = 31;
	private final int DIAS_SEPTIEMBRE = 30;
	private final int DIAS_OCTUBRE = 31;
	private final int DIAS_NOVIEMBRE = 30;
	private final int DIAS_DICIEMBRE = 31;
	private String diaAnterior;

	public PeriodoReferencia(Date fecha) {
		this.fecha = fecha;
	}
	
	public String getDosAnios() {
		dosAnios = "";
		if (fecha == null) {
			return dosAnios;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = mes1 == 1 ? c1.get(Calendar.YEAR) - 1 : c1
				.get(Calendar.YEAR);
		anio1 -= 1;
		int mes2 = mes1 == 1 ? 12 : mes1 - 1;
		int anio2 = mes1 == 1 ? anio1 : anio1 - 1;
		dosAnios = "de " + anio2 + " a " + anio1;
		return dosAnios;
	}
	
	public String get24Meses() {
		dosAnios = "";
		if (fecha == null) {
			return dosAnios;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = mes1 == 1 ? c1.get(Calendar.YEAR) - 1 : c1
				.get(Calendar.YEAR);
		int mes2 = mes1 == 1 ? 12 : mes1 - 1;
		int anio2 = mes1 == 1 ? anio1 - 1 : anio1 - 2;
		dosAnios = "de " + Util.getNombreMes(mes1).substring(0, 3) + "/"
				+ anio2 + " a " + Util.getNombreMes(mes2).substring(0, 3) + "/"
				+ anio1;
		return dosAnios;
	}

	public String getAnioPasado() {
		anioPasado = "";
		if (fecha == null) {
			return anioPasado;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int mes2 = mes1 == 1 ? 12 : mes1 - 1;
		int anio2 = anio1 - 1;
		anioPasado = "de " + Util.getNombreMes(mes1).toLowerCase() + " a "
				+ Util.getNombreMes(mes2).toLowerCase() + " de " + anio1;
		return anioPasado;
	}

	public String getDoceMeses() {
		doceMeses = "";
		if (fecha == null) {
			return doceMeses;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = mes1 == 1 ? c1.get(Calendar.YEAR) - 1 : c1
				.get(Calendar.YEAR);
		int mes2 = mes1 == 1 ? 12 : mes1 - 1;
		int anio2 = mes1 == 1 ? anio1 : anio1 - 1;
		doceMeses = "de " + Util.getNombreMes(mes1).substring(0, 3) + "/"
				+ anio2 + " a " + Util.getNombreMes(mes2).substring(0, 3) + "/"
				+ anio1;
		return doceMeses;
	}

	public String getProximosDoceMeses() {
		doceMesesProximos = "";
		if (fecha == null) {
			return doceMesesProximos;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int mes2 = mes1 == 1 ? 12 : mes1 + 1;
		int anio2 = anio1 + 1;
		doceMesesProximos = "de " + Util.getNombreMes(mes2).substring(0, 3)
				+ "/" + anio1 + " a " + Util.getNombreMes(mes1).substring(0, 3)
				+ "/" + anio2;
		return doceMesesProximos;
	}

	public String getUnMes() {
		unMes = "";
		if (fecha == null) {
			return unMes;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int anio1 = c1.get(Calendar.YEAR);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int mes2 = mes1 == 1 ? 12 : mes1 - 1;
		int anio2 = mes1 == 1 ? anio1-1 : anio1;
		unMes = Util.getNombreMes(mes2) +" "+ anio2;
		return unMes;
	}

	//TODO revisar cuando haya cambio de a�o, esta fallando en ese caso
	public String getQuinceDias() { 
		quinceDias = "";
		if (fecha == null) {
			return quinceDias;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		
		if (dia1 == 1) {
			dia1 = obtenerDiaMesAnterior(anio1, mes1, dia1);
			mes1 = mes1 - 1;
			mes1 = mes1==1 ? 12 : mes1 - 1;
			anio1 = mes1==12 ? anio1-1 : anio1;
		} else {
			dia1 = dia1 - 1;
		}

		int dia2 = dia1 <= 14 ? obtenerDiaMesAnterior(anio1, mes1 - 1,
				14 - (dia1)) : dia1 - 14;
		int mes2 = dia1 <= 14 ? mes1 == 1 ? 12 : mes1 - 1 : mes1;
		int anio2 = mes2 == 12 ? anio1 - 1 : anio1;
		quinceDias = "de " + completarCadena(String.valueOf(dia2), "0", 2)
				+ "/" + Util.getNombreMes(mes2).substring(0, 3) + " a "
				+ completarCadena(String.valueOf((dia1)), "0", 2) + "/"
				+ Util.getNombreMes(mes1).substring(0, 3);
		return quinceDias;
	}

	public String getDosSemanas() {
		dosSemanas = "";
		if (fecha == null) {
			return dosSemanas;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		Integer diaSemana = Integer.valueOf(c1.get(Calendar.DAY_OF_WEEK));
		Integer minuto1 = Integer.valueOf(c1.get(Calendar.MINUTE));
		Integer hora1 = Integer.valueOf(c1.get(Calendar.HOUR_OF_DAY));
		Integer dia1 = Integer.valueOf(c1.get(Calendar.DATE));
		Integer mes1 = Integer.valueOf(c1.get(Calendar.MONTH) + 1);
		Integer anio1 = Integer.valueOf(c1.get(Calendar.YEAR));
		SemanaAnterior sa = establecerInicioSemanaAnterior(diaSemana, hora1,
				minuto1, dia1, mes1, anio1);
		int dia2 = sa.dia < 13 ? obtenerDiaMesAnterior(sa.anio, sa.mes - 1,
				13 - sa.dia) : sa.dia - 13;
		int mes2 = sa.dia - 1 <= 13 ? sa.mes == 1 ? 12 : sa.mes - 1 : sa.mes;
		int anio2 = mes2 == 12 ? sa.anio - 1 : sa.mes;
		dosSemanas = "de " + completarCadena(String.valueOf(dia2), "0", 2)
				+ "/" + Util.getNombreMes(mes2).substring(0, 3) + " a "
				+ completarCadena(String.valueOf(sa.dia), "0", 2) + "/"
				+ Util.getNombreMes(sa.mes).substring(0, 3);
		return dosSemanas;
	}

	public String getTresMeses() {
		tresMeses = "";
		if (fecha == null) {
			return tresMeses;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int mes2 = mes1 < 4 ? 12 - (4 - (mes1 == 3 ? 4 : (mes1 + 1)))
				: mes1 - 3;
		int anio2 = mes1 < 4 ? anio1 - 1 : anio1;
		mes1 = mes1 - 1 == 0 ? 12 : mes1 - 1;
		// tresMeses = "de " + completarCadena(String.valueOf(mes2), "0", 2) +
		// "/" + anio2 + // tres letras el nombres
		// " a " + completarCadena(String.valueOf(mes1), "0", 2) + "/" + anio1;
		tresMeses = "de " + Util.getNombreMes(mes2).substring(0, 3) + "/"
				+ anio2 + // tres letras el nombres
				" a " + Util.getNombreMes(mes1).substring(0, 3) + "/" + anio1;
		return tresMeses;
	}
	
	public String getSeisMeses() {
		seisMeses = "";
		if (fecha == null) {
			return seisMeses;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int mes2 = mes1 < 7 ? 12 - (7 - (mes1 == 6 ? 7 : (mes1 + 1)))
				: mes1 - 6;
		int anio2 = mes1 < 7 ? anio1 - 1 : anio1;
		mes1 = mes1 - 1 == 0 ? 12 : mes1 - 1;
		seisMeses = "de " + Util.getNombreMes(mes2).substring(0, 3) + "/"
				+ anio2 + // tres letras el nombres
				" a " + Util.getNombreMes(mes1).substring(0, 3) + "/" + anio1;
		return seisMeses;
	}

	public String getTresAnios() {
		tresAnios = "";
		if (fecha == null) {
			return tresAnios;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR)-1;
		int mes2 = mes1 < 4 ? 12 - (4 - (mes1 == 3 ? 4 : (mes1 + 1)))
				: mes1 - 3;
		int anio2 = anio1 - 2;
		mes1 = mes1 - 1 == 0 ? 12 : mes1 - 1;
		// tresMeses = "de " + completarCadena(String.valueOf(mes2), "0", 2) +
		// "/" + anio2 + // tres letras el nombres
		// " a " + completarCadena(String.valueOf(mes1), "0", 2) + "/" + anio1;
		tresAnios = "de " + anio2 + " a " + anio1;
		return tresAnios;
	}
	
	public String get36Meses() {
		tresAnios = "";
		if (fecha == null) {
			return tresAnios;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int mes2 = mes1 < 4 ? 12 - (4 - (mes1 == 3 ? 4 : (mes1 + 1)))
				: mes1;
		int anio2 = anio1 - 3;
		mes1 = mes1 - 1 == 0 ? 12 : mes1 - 1;
		tresAnios = "de " + Util.getNombreMes(mes2).substring(0, 3) + "/" + anio2 + " al " + Util.getNombreMes(mes1).substring(0, 3) + "/" + anio1;
		return tresAnios;
	}

	public String getSieteDias() {
		sieteDias = "";
		if (fecha == null) {
			return sieteDias;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int dia2 = dia1 - 1 <= 6 ? obtenerDiaMesAnterior(anio1, mes1 - 1,
				6 - (dia1 - 1)) : dia1 - 1 - 6;
		int mes2 = dia1 - 1 <= 7 ? mes1 == 1 ? 12 : mes1 - 1 : mes1;
		int anio2 = mes2 == 12 ? anio1 - 1 : anio1;
		sieteDias = "de " + completarCadena(String.valueOf(dia2), "0", 2) + "/"
				+ Util.getNombreMes(mes2).substring(0, 3) + " a "
				+ completarCadena(String.valueOf(dia1 - 1), "0", 2) + "/"
				+ Util.getNombreMes(mes1).substring(0, 3);
		return sieteDias;
	}

	public String getCuatroSemanas() {
		cuatroSemanas = "";
		if (fecha == null) {
			return cuatroSemanas;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int diaSemana = c1.get(Calendar.DAY_OF_WEEK);
		int minuto1 = c1.get(Calendar.MINUTE);
		int hora1 = c1.get(Calendar.HOUR_OF_DAY);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		SemanaAnterior sa = establecerInicioSemanaAnterior(diaSemana, hora1,
				minuto1, dia1, mes1, anio1);
		int dia2 = sa.dia <= 27 ? obtenerDiaMesAnterior(sa.anio, sa.mes - 1,
				27 - sa.dia) : sa.dia - 27;
		int mes2 = sa.dia - 1 <= 27 ? sa.mes == 1 ? 12 : sa.mes - 1 : sa.mes;
		int anio2 = mes2 == 12 ? sa.anio - 1 : sa.mes;
		cuatroSemanas = "del " + completarCadena(String.valueOf(dia2), "0", 2)
				+ "/" + Util.getNombreMes(mes2).substring(0, 3) + " al "
				+ completarCadena(String.valueOf(sa.dia), "0", 2) + "/"
				+ Util.getNombreMes(sa.mes).substring(0, 3);
		return cuatroSemanas;
	}

	public String getSemanaAnterior() {
		semanaAnterior = "";
		if (fecha == null) {
			return semanaAnterior;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int diaSemana = c1.get(Calendar.DAY_OF_WEEK);
		int minuto1 = c1.get(Calendar.MINUTE);
		int hora1 = c1.get(Calendar.HOUR_OF_DAY);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		SemanaAnterior sa = establecerInicioSemanaAnterior(diaSemana, hora1,
				minuto1, dia1, mes1, anio1);
		int dia2 = sa.dia <= 6 ? obtenerDiaMesAnterior(sa.anio, sa.mes - 1,
				6 - (sa.dia)) : sa.dia - 6;
		int mes2 = sa.dia - 1 <= 6 ? sa.mes-1 : (sa.mes == 1 ? 12 : (dia2 < sa.dia ? sa.mes : sa.mes-1));
		int anio2 = mes2 == 12 ? sa.anio - 1 : sa.mes;
		semanaAnterior = "del " + completarCadena(String.valueOf(dia2), "0", 2)
				+ "/" + Util.getNombreMes(mes2).substring(0, 3) + " al "
				+ completarCadena(String.valueOf(sa.dia), "0", 2) + "/"
				+ Util.getNombreMes(sa.mes).substring(0, 3);
		return semanaAnterior;
	}

	private String getDiaAnterior(int dayOfWeek) {
		diaAnterior = "";
		if (fecha == null) {
			return semanaAnterior;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int diaSemana = c1.get(Calendar.DAY_OF_WEEK);
		int minuto1 = c1.get(Calendar.MINUTE);
		int hora1 = c1.get(Calendar.HOUR_OF_DAY);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		int diferencia = 7;
		SemanaAnterior saActual = establecerInicioSemanaAnterior(diaSemana, hora1,
				minuto1, dia1, mes1, anio1);
		int diaSA = saActual.dia <= diferencia ? obtenerDiaMesAnterior(saActual.anio, saActual.mes - 1,
				diferencia - (saActual.dia)) : saActual.dia - diferencia;
		int mesSA = saActual.dia - 1 <= diferencia ? saActual.mes-1 : (saActual.mes == 1 ? 12 : (diaSA < saActual.dia ? saActual.mes : saActual.mes-1));
		int anioSA = mesSA == 12 ? saActual.anio - 1 : saActual.anio;
		SemanaAnterior sa = new SemanaAnterior(diaSA, mesSA, anioSA);
		int diff = 0;
		if (sa.dayOfWeek() == Calendar.SUNDAY) {
			diff = 1;
		}
		int diasEnMes = obtenerTotalDiasMes(sa.anio, sa.mes);
		int dia2 = 0;
		int mes2 = 0;
		String dia="";
		switch (dayOfWeek) {
		case Calendar.SUNDAY:
			dia2 = sa.dia + 1 - diff > diasEnMes ? 1 : sa.dia + 1 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Domingo";
			break;
		case Calendar.MONDAY:
			dia2 = sa.dia + 2 - diff > diasEnMes ? (sa.dia + 2 - diff - diasEnMes) : sa.dia + 2 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Lunes";
			break;
		case Calendar.THURSDAY:
			dia2 = sa.dia + 3 - diff > diasEnMes ? (sa.dia + 3 - diff - diasEnMes) : sa.dia + 3 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Martes";
			break;
		case Calendar.WEDNESDAY:
			dia2 = sa.dia + 4 - diff > diasEnMes ? (sa.dia + 4 - diff - diasEnMes)  : sa.dia + 4 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Miercoles";
			break;
		case Calendar.TUESDAY:
			dia2 = sa.dia + 5 - diff > diasEnMes ? (sa.dia + 5 - diff - diasEnMes)  : sa.dia + 5 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Jueves";
			break;
		case Calendar.FRIDAY:
			dia2 = sa.dia + 6 - diff > diasEnMes ? (sa.dia + 6 - diff - diasEnMes)  : sa.dia + 6 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="Viernes";
			break;
		case Calendar.SATURDAY:
			dia2 = sa.dia + 7 - diff > diasEnMes ? (sa.dia + 7 - diff - diasEnMes) : sa.dia + 7 - diff;
			mes2 = dia2 > sa.dia ? sa.mes : (sa.mes == 12 ? 1 : sa.mes+1);
			dia="S�bado";
			break;
		default:
			break;
		}
		diaAnterior = dia + " " + Util.completarCadena(String.valueOf(dia2), "0", 2, COMPLETAR.IZQUIERDA) + "/" + Util.completarCadena(String.valueOf(mes2), "0", 2, COMPLETAR.IZQUIERDA);
		return diaAnterior;
	}

	public String getDomingoAnterior() {
		return getDiaAnterior(Calendar.SUNDAY);		
	}

	public String getLunesAnterior() {
		return getDiaAnterior(Calendar.MONDAY);		
	}

	public String getMartesAnterior() {
		return getDiaAnterior(Calendar.THURSDAY);		
	}

	public String getMiercolesAnterior() {
		return getDiaAnterior(Calendar.WEDNESDAY);		
	}

	public String getJuevesAnterior() {
		return getDiaAnterior(Calendar.TUESDAY);		
	}

	public String getViernesAnterior() {
		return getDiaAnterior(Calendar.FRIDAY);		
	}

	public String getSabadoAnterior() {
		return getDiaAnterior(Calendar.SATURDAY);		
	}

	public String getSemanaAnteriorEmpleo() {
		semanaAnterior = "";
		if (fecha == null) {
			return semanaAnterior;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(this.fecha);
		int diaSemana = c1.get(Calendar.DAY_OF_WEEK);
		int minuto1 = c1.get(Calendar.MINUTE);
		int hora1 = c1.get(Calendar.HOUR_OF_DAY);
		int dia1 = c1.get(Calendar.DATE);
		int mes1 = c1.get(Calendar.MONTH) + 1;
		int anio1 = c1.get(Calendar.YEAR);
		SemanaAnterior sa = establecerInicioSemanaAnteriorEmpleo(diaSemana, hora1,
				minuto1, dia1, mes1, anio1);
		int dia2 = sa.dia <= 6 ? obtenerDiaMesAnterior(sa.anio, sa.mes - 1,
				6 - (sa.dia)) : sa.dia - 6;
		int mes2 = sa.dia - 1 <= 6 ? sa.mes-1 : (sa.mes == 1 ? 12 : (dia2 < sa.dia ? sa.mes : sa.mes-1));
		int anio2 = mes2 == 12 ? sa.anio - 1 : sa.mes;
		semanaAnterior = "del " + completarCadena(String.valueOf(dia2), "0", 2)
				+ "/" + Util.getNombreMes(mes2).substring(0, 3) + " al "
				+ completarCadena(String.valueOf(sa.dia), "0", 2) + "/"
				+ Util.getNombreMes(sa.mes).substring(0, 3);
		return semanaAnterior;
	}
	
	private int obtenerDiaMesAnterior(int anio, int mes, int diasRestar) {
		return obtenerTotalDiasMes(anio, mes) - diasRestar;
	}

	private boolean esAnioBisiesto(int anio) {
		if ((anio % 4) == 0) {
			if (anio % 400 == 0) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	private int obtenerTotalDiasMes(int anio, int mes) {
		int dias = 0;
		switch (mes) {
		case 1:
			dias = DIAS_ENERO;
			break;
		case 2:
			dias = esAnioBisiesto(anio) ? DIAS_FEBRERO_BISIESTO : DIAS_FEBRERO;
			break;
		case 3:
			dias = DIAS_MARZO;
			break;
		case 4:
			dias = DIAS_ABRIL;
			break;
		case 5:
			dias = DIAS_MAYO;
			break;
		case 6:
			dias = DIAS_JUNIO;
			break;
		case 7:
			dias = DIAS_JULIO;
			break;
		case 8:
			dias = DIAS_AGOSTO;
			break;
		case 9:
			dias = DIAS_SEPTIEMBRE;
			break;
		case 10:
			dias = DIAS_OCTUBRE;
			break;
		case 11:
			dias = DIAS_NOVIEMBRE;
			break;
		case 12:
			dias = DIAS_DICIEMBRE;
			break;
		default:
			break;
		}
		return dias;
	}

	private String completarCadena(String cadena, String caracter, int tamanio) {
		String tmp = "";
		for (int i = 0; i < tamanio - cadena.length(); i++) {
			tmp += caracter;
		}
		return (tmp + cadena);
	}

	private SemanaAnterior establecerInicioSemanaAnterior(Integer diaSemana,
			Integer hora1, Integer minuto1, Integer dia1, Integer mes1,
			Integer anio1) {
		if (diaSemana == Calendar.SATURDAY && ((hora1 > 12) || (hora1 == 12 && minuto1 > 0))) {
			if (hora1 > 12) {
				dia1 = Integer.valueOf(dia1 + 1);
			} else if (hora1 == 12 && minuto1 > 0) {
				dia1 = Integer.valueOf(dia1 + 1);
			}
		} else if (diaSemana != Calendar.SUNDAY) {
			int diferencia = Calendar.SATURDAY - diaSemana;
			if (dia1 + diferencia - 7 > 0) {
				dia1 = Integer.valueOf(dia1 + diferencia - 7);
			} else {
				if (mes1 - 1 == 0) {
					mes1 = Integer.valueOf(12);
					anio1 = Integer.valueOf(anio1 - 1);
				} else {
					mes1 = Integer.valueOf(mes1 - 1);
				}
				int totalDiasMes = obtenerTotalDiasMes(anio1, mes1);
				dia1 = Integer.valueOf(totalDiasMes - 7 + dia1 + diferencia);
			}
		}
		SemanaAnterior sa = new SemanaAnterior(dia1, mes1, anio1);
		return sa;
	}
	
	private SemanaAnterior establecerInicioSemanaAnteriorEmpleo(Integer diaSemana,
			Integer hora1, Integer minuto1, Integer dia1, Integer mes1,
			Integer anio1) {
		if (diaSemana == Calendar.SUNDAY && ((hora1 > 12) || (hora1 == 12 && minuto1 > 0))) {
			if (hora1 > 12) {
				dia1 = Integer.valueOf(dia1 + 1);
			} else if (hora1 == 12 && minuto1 > 0) {
				dia1 = Integer.valueOf(dia1 + 1);
			}
		} else if (diaSemana != Calendar.MONDAY) {
			int diferencia = Calendar.SUNDAY - diaSemana;
			if (dia1 + diferencia > 0) {
				dia1 = Integer.valueOf(dia1 + diferencia);
			} else {
				if (mes1 - 1 == 0) {
					mes1 = Integer.valueOf(12);
					anio1 = Integer.valueOf(anio1 - 1);
				} else {
					mes1 = Integer.valueOf(mes1 - 1);
				}
				int totalDiasMes = obtenerTotalDiasMes(anio1, mes1);
				dia1 = Integer.valueOf(totalDiasMes + dia1 + diferencia);
			}
		}
		SemanaAnterior sa = new SemanaAnterior(dia1, mes1, anio1);
		return sa;
	}

	private class SemanaAnterior {
		private Integer dia;
		private Integer mes;
		private Integer anio;

		public SemanaAnterior() {
			super();
		}

		public SemanaAnterior(Integer dia, Integer mes, Integer anio) {
			super();
			this.dia = dia;
			this.mes = mes;
			this.anio = anio;
		}

		@Override
		public String toString() {
			return "SemanaAnterior [dia=" + dia + ", mes=" + mes + ", anio="
					+ anio + "]";
		}

		public int dayOfWeek() {
			Calendar c1 = Calendar.getInstance(); 
			c1.setTime(Util.getFecha(anio, mes, dia));
			return c1.get(Calendar.DAY_OF_WEEK);
		}
	}
}
