package gob.inei.dnce.util;

import gob.inei.dnce.components.Entity;

public class Memento<T extends Entity> {

	private T memento;
	
	public Memento(T memento) {
		this.memento = memento;
	}

	public T getMemento() {
		return memento;
	}

}
