package gob.inei.dnce.util;

import gob.inei.dnce.components.Entity;

import java.util.HashMap;
import java.util.Map;

public class Caretaker<T extends Entity> {

	private Map<String, Memento<T>> estados;
	
	public Caretaker() {
		estados = new HashMap<String, Memento<T>>();
	}
	
	public void addMemento(String key, Memento<T> memento) {
		estados.put(key, memento);
	}
	
	public Memento<T> get(String key) {
		return estados.get(key);
	}

}
