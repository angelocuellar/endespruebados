package gob.inei.dnce.util;

public class Edad {
	private int anios;
	private int meses;
	
	public Edad(int anios, int meses) {
		this.anios = anios;
		this.meses = meses;
	}
	
	public int getAnios() {
		return anios;
	}
	
	public int getMeses() {
		return meses;
	}	
}
