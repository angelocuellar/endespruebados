package gob.inei.dnce.util;

import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.interfaces.EventKey;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class Calculo<T extends EditText> implements TextWatcher, EventKey {
	private FragmentForm _this;
	private DialogFragmentComponent _thisDialog;
	private T cajaTotal;
	private List<List<T>> SumadoresList;
	private List<Integer> cantCamposList;
	private List<String> lstCallbacks=new ArrayList<String>();
	private List<T> camposExtList=new ArrayList<T>();
	private List<BigDecimal> resultadosList=new ArrayList<BigDecimal>();
	private List<T> lstResult=new ArrayList<T>();
	private boolean flag=false, estResult=true, valResult=true, fLoad=false;
	private int presicionOperacion;
	private int presicionResultado;
	private int redondeo;
	private BigDecimal resultExt;
	public enum TIPO {SUMA, RESTA, PRODUCTO, DIVISION, PROMEDIO};
	public enum TIPERETURN {INTEGER, BIGDECIMAL};
	private TIPO tipoRes, tipoOp;
	private TIPERETURN tipoRetorno;
	private int length;
	private boolean block, rOnlytxtR;
	

	public Calculo(FragmentForm v, T cajaTotal, int presicion,int presicionResul, List<T>... lstSum) {
		this(v, cajaTotal, presicion, presicionResul, BigDecimal.ROUND_HALF_EVEN, TIPO.SUMA, lstSum);
	}
	
	public Calculo(FragmentForm v, T cajaTotal, int presicion,int presicionResul,int redondeo, List<T>... lstSum) {
		this(v, cajaTotal, presicion, presicionResul, redondeo, TIPO.SUMA, lstSum);
	}
	
	public Calculo(FragmentForm v, T cajaTotal, int presicion,int presicionResul, TIPO tipoOp, List<T>... lstSum) {
		this(v, cajaTotal, presicion, presicionResul, BigDecimal.ROUND_HALF_EVEN, tipoOp, lstSum);
	}

	public Calculo(FragmentForm v, T cajaTotal, int presicion,int presicionResul,int redondeo, TIPO tipoOp, List<T>... lstSum) {
		this._this=v;
		this.cajaTotal=cajaTotal;
		this.SumadoresList=Arrays.asList(lstSum);
		this.presicionOperacion=presicion;
		this.presicionResultado=presicionResul;
		this.redondeo=redondeo;
		this.tipoOp=tipoOp;
		this.block = false;
	}
	
	public void setCallback(String NameMethod){
		setCallback(NameMethod, TIPERETURN.INTEGER);
	}
	public void setCallback(String NameMethod, TIPERETURN tipeReturn){
		setCallback(null, NameMethod, tipeReturn);
	}
	public void setCallback(DialogFragmentComponent dlg, String NameMethod){
		setCallback(dlg, NameMethod, TIPERETURN.INTEGER);
	}
	public void setCallback(DialogFragmentComponent dlg, String NameMethod, TIPERETURN tipeReturn){
		this._thisDialog = dlg;
		this.tipoRetorno = tipeReturn;
		this.lstCallbacks.add(NameMethod);
	}
	
	public void setConfOperacion(){
		this.setConfOperacion(0);
	}
	
	public void setConfOperacion(boolean valResult){
		this.setConfOperacion(valResult, 0);
	}
	
	public void setConfOperacion(boolean valResult, boolean firstLoad){
		this.setConfOperacion(valResult, firstLoad, 0);
	}
	
	public void setConfOperacion(Integer... cantCampos){
		this.setConfOperacion(TIPO.SUMA, cantCampos);
	}
	
	public void setConfOperacion(TIPO tipoRes, Integer... cantCampos){
		this.setConfOperacion(tipoRes, true, cantCampos);
	}
	
	public void setConfOperacion(boolean valResult, Integer... cantCampos){
		this.setConfOperacion(TIPO.SUMA, valResult, cantCampos);
	}
	
	public void setConfOperacion(boolean valResult, boolean firstLoad, Integer... cantCampos){
		this.setConfOperacion(TIPO.SUMA, valResult, firstLoad, cantCampos);
	}
	
	public void setConfOperacion(TIPO tipoRes, boolean valResult, Integer... cantCampos){
		this.setConfOperacion(tipoRes, valResult, false, cantCampos);
	}
	
	public void setConfOperacion(TIPO tipoRes, boolean valResult, boolean firstLoad, Integer... cantCampos){
		this.cantCamposList=Arrays.asList(cantCampos);
		this.tipoRes=tipoRes;
		this.valResult=valResult;
		this.fLoad=firstLoad;
		this.addOrRemoveListener();
	}
	
	public void setConfExtra(List<T> campos, BigDecimal result){
		setConfExtra(campos, result, false);
	}
	
	public void setConfExtra(List<T> campos, BigDecimal result, boolean rOnlyTxtResult){
		this.camposExtList=campos;
		this.resultExt=result;
		this.rOnlytxtR = rOnlyTxtResult;
	}
	
	public void setBlokCalculo(boolean block){
		this.block = block;
	}
	
	private void unirCampos(List<T> Sumadores, Integer cantCampos){
		lstResult.clear();
		if(Sumadores.size()>0){
			if(cantCampos>0){
				EditText key=new EditText(_this.getActivity());
				EditText value=new EditText(_this.getActivity());
				if(cantCampos*2<=Sumadores.size()){
					for(int i=0;i<Sumadores.size();i++){
						EditText result=new EditText(_this.getActivity());
						if(i<cantCampos*2){
							if(i%2==0) key=Sumadores.get(i);
							if(i%2!=0) {
								value=Sumadores.get(i);
								result.setText(Util.getText(Util.getBigDecimal(key.getText().toString(), value.getText().toString(), presicionOperacion)));
								lstResult.add((T)result);
							}
						} else lstResult.add(Sumadores.get(i));
					}
				}
			} else lstResult.addAll(Sumadores);
		}
		
		for(EditText p:lstResult)	Log.e("p", "p: "+p.getText().toString());
	}
	
	private BigDecimal getResult(BigDecimal result, TIPO tipo, List<T> Sumas){
		if(Sumas.size()>0){
			BigDecimal resp=BigDecimal.ZERO;
			for(T obj:Sumas){
				resp=Util.getBigDecimal(obj.getText().toString());
				if(obj==Sumas.get(0)) {result=resp; continue;}
				result=perfomCalculo(result, tipo, presicionOperacion, BigDecimal.ROUND_HALF_EVEN, resp);
			}
		}
		return result;
	}
	
	private int getSumadoresObjetList(){
		int acum = 0;
		if(SumadoresList.size()>0){
			for(List<T> p:SumadoresList){
				acum = acum + p.size();
			}
		}
		return acum;
	}
	
	private BigDecimal perfomCalculo(BigDecimal result, TIPO tipo, int presicion, int redondeo, BigDecimal resp){
		switch (tipo) {
			case SUMA: result=Util.getSumaDecimal(presicion, redondeo, result, resp); break;
			case RESTA: result=Util.getSumaDecimal(presicion, redondeo, result, resp.multiply(Util.getBigDecimal("-1"))); break;
			case PRODUCTO: result=Util.getBigDecimal(result.multiply(resp), presicion, redondeo); break;
			case PROMEDIO: result=Util.getSumaDecimal(presicion, redondeo, result, resp); break;
			case DIVISION: 
				try { //para controlar una division entre cero.
					result=Util.getBigDecimal(new BigDecimal(result.doubleValue()/resp.doubleValue()), presicion, redondeo); break;
				} catch (NumberFormatException e) {
					this.estResult = false;
					result = BigDecimal.ZERO;
				}
			default: break;
		}
		return result;
	}
	
	private BigDecimal getResultOperacion(BigDecimal result,  List<T> Sumas, Integer cc){
		unirCampos(Sumas, cc);
		result=getResult(result, this.tipoOp, this.lstResult);
		return result;
	}
	
	private void addOrRemoveListener(){
		if(this.SumadoresList.size()>0){
			for(List<T> obj:this.SumadoresList){
				for(T ob:obj){
					ob.addTextChangedListener(this);
//					_this.bind(this, ob);
				}
			}
		}
	}
	
	private boolean verificaCampos(boolean flag){
		if(SumadoresList.size()>0){
			for(List<T> obj:this.SumadoresList){
				if(!verificarText(obj, flag)) return false;
			}
		}
		return true;
	}
	
	private boolean verificarText(List<T> lst, boolean flag){
		for(T ob:lst)
			if(ob.getText().toString().trim().equals("")==flag) return false;
		return true;
	}
	
	private boolean verificarExtra(){
		if(camposExtList.size()>0){
			if(verificarText(camposExtList, true) && 
					getResult(BigDecimal.ZERO, TIPO.SUMA, camposExtList).compareTo(resultExt)==0){
				flag=false;
				setBlock(true);
				return true;
			} else 
				setBlock(false);
		}
		if(rOnlytxtR) readOnly(cajaTotal);
		return false;
	}
	
	private void readOnly(T cajaT){
		cajaT.setCursorVisible(!flag);
		cajaT.setFocusable(!flag);
		cajaT.setFocusableInTouchMode(!flag);
	}
	
	private void setBlock(boolean block){
		List<T> lstOp=new ArrayList<T>();
		for(List<T> lst:this.SumadoresList){
			lstOp.addAll(lst);
			for(T ob:camposExtList) lstOp.remove(ob);
			if(block){
				_this.cleanAndLockView(lstOp.toArray(new View[lstOp.size()]));
				_this.cleanAndLockView(this.cajaTotal);
			} else {
				_this.lockView(false, lstOp.toArray(new View[lstOp.size()]));
				_this.lockView(false, this.cajaTotal);
			}
			lstOp.clear();
		}
	}
	
	private void setResultFinal(boolean estado, BigDecimal textResult){
		//if(!estado)	this.cajaTotal.setBackgroundColor(_this.getResources().getColor(R.color.red));
    	//else this.cajaTotal.setBackgroundColor(_this.getResources().getColor(R.color.blanco));
		
		if(!valResult && textResult == null) textResult = BigDecimal.ZERO;
		if(tipoOp == TIPO.PROMEDIO){
			try { //para controlar una division entre cero.
				textResult=textResult.divide(Util.getBigDecimal(String.valueOf(getSumadoresObjetList()), "0", presicionOperacion), BigDecimal.ROUND_HALF_UP);
			} catch (NumberFormatException e) {
			}
		}
		this.cajaTotal.setText(Util.getText(textResult));
		executeCallback(textResult);
	}
	
	private boolean isFocused(){
		if(SumadoresList.size()>0){
			for(List<T> obj:this.SumadoresList){
				for(T ob:obj)
					if(ob.isFocused()) return true;
			}
		}
		return false;
	}

	private void executeCallback(BigDecimal textResult) {
		if(!lstCallbacks.isEmpty() && textResult != null){
			for(String strMethod : lstCallbacks){
				Object object = _this;
				if(_thisDialog != null){
					object = _thisDialog;
				}
				for(Method m : object.getClass().getDeclaredMethods()){
//					Log.e("m.getName()", "m.getName(): "+m.getName());
					if(m.getName().equals(strMethod)){
						try {
							m.invoke(object, getTipeReturn(textResult));
							break;
						} catch (IllegalArgumentException e) {
							Log.e("IllegalArgumentException", "IllegalArgumentException: "+e.getMessage());
						} catch (IllegalAccessException e) {
							Log.e("IllegalAccessException", "IllegalAccessException: "+e.getMessage());
						} catch (InvocationTargetException e) {
							Log.e("InvocationTargetException", "InvocationTargetException: "+e.getMessage());
						}
					}
				}
			}
		}
	}
	
	private Object getTipeReturn(BigDecimal txtResult){
		if(this.tipoRetorno == TIPERETURN.INTEGER) return Integer.valueOf(txtResult.intValue());
		return txtResult;
	}

	@Override
	public void onTextChanged(CharSequence onTextChanged, int arg1, int arg2,int arg3) { }
	@Override
    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) {	
		length = arg0.toString().length();
	}
	@Override
    public void afterTextChanged(Editable arg0) {
		if(block) return;
//		if(length > arg0.toString().length()) flag = true;
		if(fLoad){
			flag = true;
//			fLoad = false;
		} else
			flag = isFocused();
		if(!flag) return;
		if(verificarExtra()) return;
		if(/*(!valResult && verificaCampos(true))||*/(valResult && verificaCampos(false))) {
			Log.e("Resultssss", "Result: ");
    		setResultFinal(true, null);
    		return;
    	}
//		if((valResult /*&& verificaCampos(true))||(valResult && verificaCampos(false)*/)) {
//			Log.e("Resultssss", "Result: ");
//			setResultFinal(true, null);
//			return;
//		}
		Log.e("Resultggggg", "Result: ");
		resultadosList.clear();
		BigDecimal result=BigDecimal.ZERO;
		int cont=0;
		for(List<T> lst:this.SumadoresList){
			int cl=this.cantCamposList.size();
			cl=cl>0&&cl>=cont?this.cantCamposList.get(cont++):-1;
			resultadosList.add(getResultOperacion(BigDecimal.ZERO,lst,cl));
		}
		
		for(BigDecimal resp:resultadosList){
			if(resp==resultadosList.get(0)) {
				result=Util.getBigDecimal(resp,presicionResultado,redondeo);
				continue;
			}
			result=perfomCalculo(result, tipoRes, presicionResultado, this.redondeo, resp);
		}

		Log.e("Result", "Result: "+result);
		if(result.signum()<0) estResult = false;
		setResultFinal(estResult, !estResult?null:result);
    	flag=false; 
    	estResult=true;
    }

	@Override
	public boolean onKey(View from, View to, View next, int keyCode,
			KeyEvent event, HashMap<String, LinkedList> values,
			Boolean cancel, Boolean allEquals) {
		if(event.getAction()==KeyEvent.ACTION_DOWN && keyCode!=KeyEvent.KEYCODE_ENTER)
			flag=true;
		return false;
	}
}