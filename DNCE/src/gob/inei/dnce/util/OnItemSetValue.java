package gob.inei.dnce.util;

import gob.inei.dnce.interfaces.Item;
import gob.inei.dnce.listeners.OnSetValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class OnItemSetValue 
implements OnSetValue
{
	
	private final Map<String,Item> mapItems;
	
	public OnItemSetValue(List<? extends Item> items) {
		mapItems = new HashMap<String, Item>();
		for(Item it : items) {
			mapItems.put(it.getCodigo(), it);
		}		
	}
	

	@Override
	public final String getValue(String codigo) throws Exception {
		Item it = mapItems.get(codigo);
		return it!=null ? it.getValor() : null;
	}

	
}
