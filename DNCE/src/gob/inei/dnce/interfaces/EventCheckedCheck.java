package gob.inei.dnce.interfaces;

import java.util.HashMap;
import java.util.LinkedList;

import android.view.View;
import android.widget.CompoundButton;

/**
 * 
 * @author ajulcamoro
 *
 */
public interface EventCheckedCheck {

	/**
	 * 
	 * Evento para CheckBox
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param isChecked
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onCheckedChanged(CompoundButton from, View to, View next, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
}
