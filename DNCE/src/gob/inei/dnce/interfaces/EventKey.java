package gob.inei.dnce.interfaces;

import java.util.HashMap;
import java.util.LinkedList;

import android.view.KeyEvent;
import android.view.View;

/**
 * 
 * @author ajulcamoro
 *
 */
public interface EventKey {
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param keyCode
	 * @param event
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onKey(View from, View to, View next, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
}
