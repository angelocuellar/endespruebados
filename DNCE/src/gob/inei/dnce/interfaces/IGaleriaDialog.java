package gob.inei.dnce.interfaces;

import gob.inei.dnce.components.FragmentForm;

import java.io.File;
import java.util.Map;

import android.graphics.Bitmap;

public interface IGaleriaDialog {	
	FragmentForm getForm();
	void pictureChosen(Bitmap bitmap, File filePath);
}
