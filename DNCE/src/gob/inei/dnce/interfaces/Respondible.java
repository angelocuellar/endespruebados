package gob.inei.dnce.interfaces;

public interface Respondible {
	void onCancel();
	void onAccept();
}
