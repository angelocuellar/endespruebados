package gob.inei.dnce.interfaces;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface Item {
	String getCodigo();
//	String getCodigoExterno();
	String getNombre();
	String getValor();
}
