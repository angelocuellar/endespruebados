package gob.inei.dnce.interfaces;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface RespondibleDialog 
extends Respondible
{
	void onCerrar();
}
