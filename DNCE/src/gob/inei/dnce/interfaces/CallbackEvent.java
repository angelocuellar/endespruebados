package gob.inei.dnce.interfaces;

import gob.inei.dnce.components.RadioGroupOtherField;

import java.util.HashMap;
import java.util.LinkedList;

import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

/**
 * 
 * Interface para el manejo de eventos en cola.
 * 
 * @author ajulcamoro
 *
 */
public interface CallbackEvent {
	
	/**
	 * 
	 * Evento para EditText, Spinner, CheckBox, RadioGroup
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param keyCode
	 * @param event
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
		
	/**
	 * 
	 * Evento para Spinner
	 * 
	 * @param arg0
	 * @param from
	 * @param to
	 * @param next
	 * @param arg2
	 * @param arg3
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
	/**
	 * 
	 * Evento para RadioGroup
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param checkedId
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
	/**
	 * 
	 * Evento para RadioGroup
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param checkedId
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
	/**
	 * 
	 * Evento para CheckBox
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param isChecked
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
	/**
	 * 
	 * @param v
	 * @param hasFocus
	 * @return
	 */
	void onFocusChange(View v, boolean hasFocus);
}