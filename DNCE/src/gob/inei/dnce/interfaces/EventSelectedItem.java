package gob.inei.dnce.interfaces;

import java.util.HashMap;
import java.util.LinkedList;

import android.view.View;
import android.widget.AdapterView;

public interface EventSelectedItem {
	/**
	 * 
	 * Evento para Spinner
	 * 
	 * @param arg0
	 * @param from
	 * @param to
	 * @param next
	 * @param arg2
	 * @param arg3
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, 
			HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
}
