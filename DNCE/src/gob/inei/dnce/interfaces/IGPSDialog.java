package gob.inei.dnce.interfaces;

import java.util.Map;

import android.support.v4.app.Fragment;

public interface IGPSDialog {
	final String LONGITUD = "LONGITUDE";
	final String LATITUD = "LATITUDE";
	final String ALTURA = "ALTITUDE";
	final String ACCURACY = "ACCURACY";
	final String SATELITES_NRO = "satellites";
	final String BEARING = "BEARING";
	
	Fragment getForm();
	void postShow(Map<String, String> properties);
}
