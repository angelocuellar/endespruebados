package gob.inei.dnce.interfaces;

import java.util.List;

import gob.inei.dnce.components.Entity;

public interface Searchable <T extends IDetailEntityComponent>{
	void postResultSearch(T result, String resp);
	List<T> getListData();
	String[] getFieldsListData();
	boolean getHasEsp(T selecc);
}
