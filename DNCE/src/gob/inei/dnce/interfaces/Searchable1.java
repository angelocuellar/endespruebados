package gob.inei.dnce.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface Searchable1 {
	public static enum NIVEL{NIVEL1, NIVEL2, NIVEL3};
	public static enum KEY{ESPEC, SHOW};
	<T extends IDetailEntityComponent>void postResultSearch(List<String> resps, T... result);
	<T extends IDetailEntityComponent>List<T> getListData(NIVEL nivel, T selecc);
	List<Object[]> getFieldsListData(NIVEL nivel);
	<T extends IDetailEntityComponent>boolean getHasEsp(T selecc, NIVEL nivel, KEY key);
	NIVEL getNiveles();
}
