package gob.inei.endes2023.model;

import gob.inei.dnce.components.FragmentForm;

public interface IExportacion {
	FragmentForm getFragmentForm();

}
